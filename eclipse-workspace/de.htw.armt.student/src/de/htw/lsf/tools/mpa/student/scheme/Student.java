package de.htw.lsf.tools.mpa.student.scheme;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;

@Entity
@Table(name = "STUDENT")
public class Student extends ModelObject {

	private static final long serialVersionUID = -2472869070231349521L;
	public static final String REGISTRATIONNUMBER_FIELD = "registrationNumber";
	public static final String LASTNAME_FIELD = "lastname";
	public static final String FIRSTNAME_FIELD = "firstname";
	public static final String NAMENSZUSATZ_FIELD = "namenszusatz";
	public static final String TITLE_FIELD = "title";
	public static final String MALE_FIELD = "male";
	public static final String EMAIL_FIELD = "email";
	
	private Long registrationNumber;
	private String lastname;
	private String firstname;
	private String namenszusatz; // middle initial, name affix
	private String title;
	private boolean male = true;
	private String email;
	private boolean manuallyAdded = false;

	public Student() {
		super();
	}

	public Student(Long registrationNumber, String lastname, String firstname,
			String namenszusatz, String title, boolean male, String email) {
		this(registrationNumber, lastname, firstname, namenszusatz, title, male,
				email, false);
	}

	public Student(Long registrationNumber, String lastname, String firstname,
			String namenszusatz, String title, boolean male, String email,
			boolean manuallyAdded) {
		super();
		this.registrationNumber = registrationNumber;
		this.lastname = lastname;
		this.firstname = firstname;
		this.namenszusatz = namenszusatz;
		this.title = title;
		this.male = male;
		this.email = email;
		this.manuallyAdded = manuallyAdded;
	}

	@Id
	@Column(name = "matrikelnummer")
	public Long getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(Long registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	@Column(name = "nachname")
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Column(name = "vorname")
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Column(name = "namenszusatz")
	public String getNamenszusatz() {
		return namenszusatz;
	}

	public void setNamenszusatz(String namenszusatz) {
		this.namenszusatz = namenszusatz;
	}

	@Column(name = "titel")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "maenlich")
	public boolean isMale() {
		return male;
	}

	public void setMale(boolean male) {
		this.male = male;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return lastname + ", " + firstname + " (" + registrationNumber + ")";
	}

	@Column(name = "manuellhinzugefuegt")
	public boolean isManuallyAdded() {
		return manuallyAdded;
	}

	public void setManuallyAdded(boolean manuallyAdded) {
		this.manuallyAdded = manuallyAdded;
	}

	@Transient
	@Override
	public Long getNumber() {
		return this.getRegistrationNumber();
	}

	@Override
	public void setNumber(Long number) {
		this.setRegistrationNumber(number);
	}
}
