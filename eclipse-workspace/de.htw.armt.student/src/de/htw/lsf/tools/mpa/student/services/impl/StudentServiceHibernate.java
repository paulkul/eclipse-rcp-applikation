/*
 * StudentService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.student.services.impl;

import javax.persistence.EntityManager;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.student.scheme.Student;
import de.htw.lsf.tools.mpa.student.scheme.dao.impl.StudentDAOHibernate;
import de.htw.lsf.tools.mpa.student.services.StudentService;

/**
 * @author V.Medvid
 */
public class StudentServiceHibernate extends AbstractServiceHibernate<Student>
		implements StudentService {

	private static StudentServiceHibernate instance = null;

	public StudentServiceHibernate() {
		super(Student.class);
	}

	@Override
	public AbstractDAO<Student> getDAO(EntityManager entityManager) {
		StudentDAOHibernate dao = StudentDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static StudentServiceHibernate getInstance() {
		if (instance == null) {
			instance = new StudentServiceHibernate();
		}
		return instance;
	}
}
