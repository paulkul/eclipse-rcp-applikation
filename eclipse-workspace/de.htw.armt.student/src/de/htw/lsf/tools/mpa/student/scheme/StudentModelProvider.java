package de.htw.lsf.tools.mpa.student.scheme;

import org.eclipse.core.databinding.observable.list.WritableList;

public enum StudentModelProvider {
	INSTANCE;

	private WritableList students;

	private StudentModelProvider() {
		students = WritableList.withElementType(Student.class);
	}

	public WritableList getStudents() {
		return students;
	}

}
