package de.htw.lsf.tools.mpa.bewertungen.gui.views;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.part.ViewPart;

import de.htw.lsf.tools.mpa.base.gui.actions.RefreshViewAction;
import de.htw.lsf.tools.mpa.base.gui.views.RefreshableView;
import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.gui.adapterfactories.LehrveranstaltungAdapterFactory;
import de.htw.lsf.tools.mpa.bewertungen.gui.handlers.SetLeistungsnachweisAction;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.services.impl.LehrveranstaltungServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.adapterfactory.LeistungsnachweisAdapterFactory;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

public class LehrveranstaltungView extends ViewPart implements RefreshableView {

	public static final String ID = "de.htw.lsf.tools.mpa.bewertungen.gui.views.LehrveranstaltungView";

	private IAdapterFactory adapterFactory = new LehrveranstaltungAdapterFactory();
	private TreeViewer treeViewer;
	private MenuManager menuManager;
	private Logger logger = Logger.getLogger(LehrveranstaltungView.class);

	@Override
	public void createPartControl(Composite parent) {
		getViewSite().getActionBars().getToolBarManager()
				.add(
						new RefreshViewAction(this.getSite()
								.getWorkbenchWindow(), this));

		treeViewer = new TreeViewer(parent, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		Platform.getAdapterManager().registerAdapters(new LeistungsnachweisAdapterFactory(),
				AbstractAssessment.class);
		Platform.getAdapterManager().registerAdapters(adapterFactory,
				Lehrveranstaltung.class);
		Platform.getAdapterManager().registerAdapters(adapterFactory,
				Lehrveranstaltung[].class);
		getSite().setSelectionProvider(treeViewer);
		treeViewer.setLabelProvider(new WorkbenchLabelProvider());
		treeViewer.setContentProvider(new BaseWorkbenchContentProvider());

		hookDoubleClickAction();
		hookContextMenu();
		refreshData();
	}

	private void hookContextMenu() {
		menuManager = new MenuManager();
		Menu menu = menuManager.createContextMenu(treeViewer.getControl());
		treeViewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuManager, treeViewer);
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				refreshContextMenu(event.getSelection());
			}
		});
	}

	private void hookDoubleClickAction() {
		treeViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				IHandlerService handlerService = (IHandlerService) getSite()
						.getService(IHandlerService.class);
				try {
					handlerService
							.executeCommand(
									"de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.views.LehrveranstaltungView.doubleClick",
									new Event());
				} catch (Exception ex) {
					logger
							.log(Level.WARN, Messages.LehrveranstaltungView_2,
									ex);
				}
			}
		});
	}

	private void refreshContextMenu(ISelection incoming) {
		IContributionItem[] items = menuManager.getItems();
		for (IContributionItem item : items) {
			if (item instanceof ISelectionChangedListener) {
				treeViewer
						.removeSelectionChangedListener((ISelectionChangedListener) item);
			}
		}
		menuManager.removeAll();
		if (incoming instanceof IStructuredSelection) {
			IStructuredSelection selection = (IStructuredSelection) incoming;
			if (selection.size() == 1) {
				Object obj = selection.getFirstElement();
				if (obj instanceof Lehrveranstaltung) {
					Lehrveranstaltung lv = (Lehrveranstaltung) obj;
					MenuManager subMenu = new MenuManager(
							Messages.LehrveranstaltungView_3);
					menuManager.add(subMenu);
					try {
						List<AssessmentGroup> list = LeistungsnachweisServiceHibernate
								.getInstance().getLeistungsnachweiseFuerModul(
										lv.getModul());
						for (AssessmentGroup lnmtln : list) {
							SetLeistungsnachweisAction sla = new SetLeistungsnachweisAction(
									this.getSite().getWorkbenchWindow(), this,
									lnmtln, lv);
							subMenu.add(sla);
						}
					} catch (Exception e) {
						logger.log(Level.WARN,
								Messages.LehrveranstaltungView_4, e);
					}
					menuManager.add(new Action() {
						@Override
						public void run() {
							IHandlerService handlerService = (IHandlerService) getSite()
									.getService(IHandlerService.class);
							try {
								handlerService
										.executeCommand(
												"de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.views.LehrveranstaltungView.computeFinalScore",
												new Event());
								refreshData();
							} catch (Exception ex) {
								logger.log(Level.WARN,
										Messages.LehrveranstaltungView_6, ex);
							}
						}

						@Override
						public String getText() {
							return Messages.LehrveranstaltungView_7;
						}

						@Override
						public ImageDescriptor getImageDescriptor() {
							return super.getImageDescriptor();
						}

						@Override
						public String getDescription() {
							return Messages.LehrveranstaltungView_8;
						}
					});
				} else
				// Teilnehmer per Mail informieren
				if (obj != null && obj instanceof Assessment) {
					menuManager.add(new Action() {

						@Override
						public void run() {
							IHandlerService handlerService = (IHandlerService) getSite()
									.getService(IHandlerService.class);
							try {
								handlerService
										.executeCommand(
												"de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.views.LehrveranstaltungView.CallMailService",
												new Event());
								refreshData();
							} catch (Exception ex) {
								logger.log(Level.WARN,
										Messages.LehrveranstaltungView_10, ex);
							}
						}

						@Override
						public String getText() {
							return Messages.LehrveranstaltungView_11;
						}

						@Override
						public ImageDescriptor getImageDescriptor() {
							return super.getImageDescriptor();
						}

						@Override
						public String getDescription() {
							return Messages.LehrveranstaltungView_12;
						}
					});

				}
			}
		}
		// treeViewer.addSelectionChangedListener(sla);
	}

	@Override
	public void refreshData() {
		try {
			List<Lehrveranstaltung> lehrveranstaltungen = LehrveranstaltungServiceHibernate
					.getInstance().selectAll();
			if (lehrveranstaltungen != null) {
				TreePath[] paths = treeViewer.getExpandedTreePaths();
				ISelection sel = treeViewer.getSelection();
				treeViewer.setInput(lehrveranstaltungen
						.toArray(new Lehrveranstaltung[lehrveranstaltungen
								.size()]));
				treeViewer.setExpandedTreePaths(paths);
				treeViewer.setSelection(sel);
			}
		} catch (Exception e) {
			logger.log(Level.WARN, Messages.LehrveranstaltungView_13, e);
		}
	}

	@Override
	public void setFocus() {
		treeViewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		Platform.getAdapterManager().unregisterAdapters(adapterFactory);
		super.dispose();
	}

}
