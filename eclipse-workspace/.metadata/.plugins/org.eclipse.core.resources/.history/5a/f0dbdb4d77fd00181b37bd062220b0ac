package de.htw.lsf.tools.mpa.bewertungen.gui.views;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Bewertung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungServiceFactory;
import de.htw.lsf.tools.mpa.bewertungen.services.impl.BewertungServiceHibernate;
import de.htw.lsf.tools.mpa.bewertungen.services.impl.TeilnehmerServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Term;
import de.htw.lsf.tools.mpa.student.gui.views.StudentSubView;

public class StudentBewertungenView extends StudentSubView {

	public static final String ID = "de.htw.lsf.tools.mpa.bewertungen.gui.views.StudentBewertungenView";

	private FormToolkit toolkit;
	private ScrolledForm form;
	private boolean firstSection;
	private Logger logger = Logger.getLogger(StudentBewertungenView.class);

	@Override
	public void createPartControl(final Composite parent) {
		this.setPartName(Messages.StudentBewertungenView_1);
		toolkit = new FormToolkit(parent.getDisplay());

		form = toolkit.createScrolledForm(parent);
		TableWrapLayout layout = new TableWrapLayout();
		form.getBody().setLayout(layout);
	}

	private void createSection(Teilnehmer teilnehmer) {
		int params = Section.TITLE_BAR | Section.TWISTIE;
		if (firstSection) {
			params |= Section.EXPANDED;
			firstSection = false;
		}
		Section section = toolkit.createSection(form.getBody(), params);
		TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
		td.colspan = 1;
		section.setLayoutData(td);
		section.addExpansionListener(new ExpansionAdapter() {
			@Override
			public void expansionStateChanged(ExpansionEvent e) {
				form.reflow(true);
			}
		});
		String txt = ""; //$NON-NLS-1$
		Lehrveranstaltung lehrveranstaltung = teilnehmer.getVeranstaltung();
		if (lehrveranstaltung != null) {
			Module m = lehrveranstaltung.getModul();
			if (m != null) {
				txt = m.getName();
			} else {
				txt = "-"; //$NON-NLS-1$
			}
			txt += " ("; //$NON-NLS-1$
			Term s = lehrveranstaltung.getSemester();
			if (s != null) {
				txt += s.getName();
			} else {
				txt += "-"; //$NON-NLS-1$
			}
			txt += ")"; //$NON-NLS-1$
		}
		section.setText(txt);
		Composite client = toolkit.createComposite(section, SWT.WRAP);
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		layout.marginWidth = 2;
		layout.marginHeight = 2;
		layout.marginBottom = 20;
		layout.horizontalSpacing = 10;
		client.setLayout(layout);
		section.setClient(client);
		createFormData(client, teilnehmer);
		form.reflow(true);
	}

	private void createFormData(Composite client, Teilnehmer teilnehmer) {
		Lehrveranstaltung lehrveranstaltung = teilnehmer.getVeranstaltung();
		if (lehrveranstaltung != null) {
			AssessmentGroup lng = lehrveranstaltung
					.getLeistungsnachweisGruppe();
			if (lng != null) {
				createFormDataForLNMTLN(client, lng, teilnehmer, 0);
			}
		}
	}

	private void createFormDataForLNMTLN(Composite client,
			AssessmentGroup lng, Teilnehmer teilnehmer,
			int level) {
		DecimalFormat df = new DecimalFormat("0.00"); //$NON-NLS-1$
		String txt = ""; //$NON-NLS-1$
		Text text = null;
		Font boldFont = JFaceResources.getFontRegistry().getBold(
				JFaceResources.DEFAULT_FONT);
		Font italicFont = JFaceResources.getFontRegistry().getItalic(
				JFaceResources.DEFAULT_FONT);
		Label lbl = toolkit.createLabel(client, lng.getName()
				+ " (" + lng.getAbbreviation() //$NON-NLS-1$
				+ "):"); //$NON-NLS-1$
		GridData gd = new GridData();
		gd.horizontalIndent = level * 20;
		lbl.setLayoutData(gd);
		lbl.setFont(boldFont);
		double value = 0;
//		try {
			value = BewertungServiceHibernate.getInstance().getBewertung(
					teilnehmer, lng);
//		} catch (Exception e) {
//			logger.log(Level.ERROR, Messages.StudentBewertungenView_11, e);
//		}
		txt = df.format(value);
		toolkit.createLabel(client, txt);
		value = 0;
		try {
			LehrveranstaltungServiceFactory factory = LehrveranstaltungServiceFactory
					.getInstance(LehrveranstaltungServiceFactory.HIBERNATE);
			value = factory.getLeistungsnachweisService().getMaxPunkte(lng);
		} catch (Exception e) {
			logger.log(Level.ERROR, Messages.StudentBewertungenView_12, e);
		}
		txt = " (max:  " + df.format(value) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
		toolkit.createLabel(client, txt);
		txt = Messages.StudentBewertungenView_15 + lng.getFormula();
		lbl = toolkit.createLabel(client, txt);
		lbl.setFont(italicFont);
		lbl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		for (AbstractAssessment lnw : lng.getAssessments()) {
			txt = ""; //$NON-NLS-1$
			if (lnw instanceof Assessment) {
				lbl = toolkit.createLabel(client, lnw.getName()
						+ " (" + lnw.getAbbreviation() //$NON-NLS-1$
						+ "):"); //$NON-NLS-1$
				gd = new GridData();
				gd.horizontalIndent = (level + 1) * 20;
				lbl.setLayoutData(gd);
				Bewertung b = null;
				try {
					b = BewertungServiceHibernate.getInstance().getBewertung(
							teilnehmer, (Assessment) lnw);
				} catch (Exception e) {
					logger.log(Level.ERROR, Messages.StudentBewertungenView_19,
							e);
				}
				if (b != null) {
					txt = df.format(b.getPunkte());
				}
				text = toolkit.createText(client, txt);
				text.setSize(100, 10);
				gd = new GridData();
				gd.widthHint = 30;
				text.setLayoutData(gd);
				value = ((Assessment) lnw).getMaxScore();
				txt = " (max:  " + df.format(value) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				toolkit.createLabel(client, txt);
				txt = ""; //$NON-NLS-1$
				toolkit.createLabel(client, txt);
			} else if (lnw instanceof AssessmentGroup) {
				createFormDataForLNMTLN(client,
						(AssessmentGroup) lnw, teilnehmer,
						level + 1);
			}
		}
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public void setStudent(Long matrikelnummer) {
		firstSection = true;
		for (Control child : form.getBody().getChildren()) {
			child.dispose();
		}
		List<Teilnehmer> list = new ArrayList<Teilnehmer>();
		try {
			list = TeilnehmerServiceHibernate.getInstance()
					.getTeilnehmerByMatrNr(matrikelnummer);
		} catch (Exception e) {
			logger.log(Level.ERROR, Messages.StudentBewertungenView_23, e);
		}
		for (Teilnehmer teilnehmer : list) {
			createSection(teilnehmer);
		}
	}

	@Override
	public void setFocus() {

	}

}
