package de.htw.armt.assessment.parts;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.UIEvents.Part;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.part.ViewPart;

import de.htw.lsf.tools.mpa.base.gui.actions.RefreshViewAction;
import de.htw.lsf.tools.mpa.base.gui.views.RefreshableView;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.adapterfactory.LeistungsnachweisAdapterFactory;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;

public class SamplePart1 extends ViewPart implements RefreshableView {

	private TreeViewer treeViewer;
	public WritableList input;

	private IAdapterFactory adapterFactory = new LeistungsnachweisAdapterFactory();

	ModulService ms = ModulServiceHibernate.getInstance();

	@PostConstruct
	public void createPartControl(Composite parent) {
		this.setPartName(Messages.LeistungsnachweisView_Titel);

		treeViewer = new TreeViewer(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		Platform.getAdapterManager().registerAdapters(adapterFactory, AbstractAssessment.class);
		Platform.getAdapterManager().registerAdapters(adapterFactory, Module.class);
		Platform.getAdapterManager().registerAdapters(adapterFactory, Module[].class);
		getSite().setSelectionProvider(treeViewer);
		treeViewer.setLabelProvider(new WorkbenchLabelProvider());
		treeViewer.setContentProvider(new BaseWorkbenchContentProvider());
		refreshData();
		treeViewer.expandToLevel(2);
	}

	@Override
	public void refreshData() {
		// save tree path and selection
		TreePath[] paths = treeViewer.getExpandedTreePaths();
		ISelection sel = treeViewer.getSelection();

		List<Module> moduls = ms.selectAll();
		treeViewer.setInput(moduls.toArray(new Module[moduls.size()]));

		// restore tree path and selection
		treeViewer.setExpandedTreePaths(paths);
		treeViewer.setSelection(sel);
	}

	@Override
	public void setFocus() {
		treeViewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		Platform.getAdapterManager().unregisterAdapters(adapterFactory);
		super.dispose();
	}

}
