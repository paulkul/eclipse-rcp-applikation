package de.htw.lsf.tools.mpa.bewertungen.gui.dialogs;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.gui.providers.TeilnehmerTreeContentProvider;
import de.htw.lsf.tools.mpa.bewertungen.gui.providers.TeilnehmerTreeLabelProvider;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.services.BewertungServiceFactory;
import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungService;
import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungServiceFactory;
import de.htw.lsf.tools.mpa.bewertungen.services.TeilnehmerService;
import de.htw.lsf.tools.mpa.bewertungen.services.impl.TeilnehmerServiceHibernate;
import de.htw.lsf.tools.mpa.student.scheme.Student;
import de.htw.lsf.tools.mpa.student.services.impl.StudentServiceHibernate;

public class EditLehrveranstaltungDialog extends Dialog {		
	
	private Lehrveranstaltung lv = null;
	private List<Student> toCheck = null;
	private List<Student> studenten = null;
	private List<Teilnehmer> teilnehmer = null;
	private TreeViewer leftTree, rightTree;
	private FormToolkit toolkit;
	private ScrolledForm form;
	private ISelection selection;
	private TeilnehmerService tnService = BewertungServiceFactory.getInstance(
			BewertungServiceFactory.HIBERNATE).getTeilnehmerService();
	private GetVerfuegbareStudenten getStudenten = new GetVerfuegbareStudenten();

	private LehrveranstaltungService lvService = LehrveranstaltungServiceFactory.getInstance(
			LehrveranstaltungServiceFactory.HIBERNATE).getLehrveranstaltungService();
	
	public EditLehrveranstaltungDialog(Shell parentShell, Long lv_id) {
		super(parentShell);
		lv = lvService.find(lv_id);	
	}
	
    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText(Messages.EditLehrveranstaltungDialog_0);
    }
    
    @Override
    protected Control createDialogArea(Composite parent) {
    	
    	toolkit = new FormToolkit(parent.getDisplay());
		toolkit.setBackground(parent.getBackground());
	    form = toolkit.createScrolledForm(parent);
	    TableWrapLayout layout = new TableWrapLayout();
	    layout.numColumns = 3;
	    form.getBody().setLayout(layout);
	    
	    //PARENT SECTION
	    Section parentSection = toolkit.createSection(form.getBody(), Section.NO_TITLE);
	    TableWrapData td = new TableWrapData(TableWrapData.FILL_GRAB);
	    td.colspan = 3;	      
	    parentSection.setLayoutData(td);	      	
	    Composite parentComp = toolkit.createComposite(parentSection);
	      
	    GridLayout glayout = new GridLayout();
	    glayout.numColumns = 3;					 
	    parentComp.setLayout(glayout);
	    GridData gd;
    	
      		
		Label lv_label = new Label(parentComp, SWT.SINGLE);
		lv_label.setText(lv.getBezeichnung());
		gd = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
		gd.horizontalSpan = 3;
		lv_label.setLayoutData(gd);

		
		// LEFT SECTION
		Section leftSection = toolkit.createSection(parentComp,Section.NO_TITLE);
    	gd = new GridData(GridData.FILL_BOTH);
    	gd.horizontalSpan = 1;
    	leftSection.setLayoutData(gd);	
    	Composite leftComp = toolkit.createComposite(leftSection);
    	glayout = new GridLayout();
    	glayout.numColumns = 1;					 
    	leftComp.setLayout(glayout);

    	
    	Label left = new Label(leftComp, 0);
    	left.setText(Messages.EditLehrveranstaltungDialog_1);
		gd = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
		gd.horizontalSpan = 1;
    	left.setLayoutData(gd);
    		
		leftTree = new TreeViewer(leftComp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		gd = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
		gd.heightHint = 150;
		gd.minimumWidth = 250;
		gd.widthHint = 250;
		gd.horizontalSpan = 1;
		leftTree.getControl().setLayoutData(gd);
		leftTree.setContentProvider(new TeilnehmerTreeContentProvider());
		leftTree.setLabelProvider(new TeilnehmerTreeLabelProvider());
		leftTree.setInput(teilnehmer);
		leftTree.refresh();
		
		leftSection.setClient(leftComp);
		// LEFT SECTION	
		
		
		// MIDDLE SECTION
		Section middleSection = toolkit.createSection(parentComp, Section.NO_TITLE);
    	gd = new GridData(GridData.VERTICAL_ALIGN_CENTER|GridData.GRAB_HORIZONTAL);
		gd.horizontalSpan = 1;
		middleSection.setLayoutData(gd);	
		Composite middleComp = toolkit.createComposite(middleSection);
		glayout = new GridLayout();
		glayout.numColumns = 1;					 
		middleComp.setLayout(glayout);
			
		gd = new GridData(GridData.CENTER);
		gd.horizontalSpan = 1;
		gd.minimumWidth = 60;
		gd.widthHint = 60;
	
		Button add = new Button(middleComp, SWT.PUSH);
		add.setText(Messages.EditLehrveranstaltungDialog_2);
		add.setLayoutData(gd);
		add.addListener(SWT.Selection, new Listener(){
			@Override
			public void handleEvent(Event event) {
				selection = rightTree.getSelection();
				Object [] selectedStudent = ((IStructuredSelection)selection).toArray();						
				for (int i = 0; i < selectedStudent.length; i++){
					Student selStudent = (Student)selectedStudent[i];
					Teilnehmer t1 = new Teilnehmer(lv, selStudent);
					t1 = (Teilnehmer) tnService.insert(t1);
				}
				getInput();
			}
		});
		
		Button del = new Button(middleComp, SWT.PUSH);
		del.setText(Messages.EditLehrveranstaltungDialog_3);
		del.setLayoutData(gd);
		del.addListener(SWT.Selection, new Listener(){
			@Override
			public void handleEvent(Event event) {
				selection = leftTree.getSelection();
				Object [] selectedTeilnehmer = ((IStructuredSelection)selection).toArray();
				for (int i = 0; i < selectedTeilnehmer.length; i++){
					Teilnehmer t1 = (Teilnehmer)selectedTeilnehmer[i];
					if (t1.getEndnote()==0.0){
						tnService.delete(t1);
					}else{
						MessageDialog.openError(
								getShell(), 
								Messages.EditLehrveranstaltungDialog_4, 
								Messages.EditLehrveranstaltungDialog_5+t1.getStudent().getLastname()+Messages.EditLehrveranstaltungDialog_6+t1.getStudent().getFirstname()+
								Messages.EditLehrveranstaltungDialog_7+t1.getStudent().getRegistrationNumber()+Messages.EditLehrveranstaltungDialog_8);
					}
				}
				getInput();
			}
			
		});
		
		middleSection.setClient(middleComp);
		// MIDDLE SECTION
		
		// RIGHT SECTION
		Section rightSection = toolkit.createSection(parentComp, Section.NO_TITLE);
    	gd = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
		gd.horizontalSpan = 1;
		rightSection.setLayoutData(gd);		
		Composite rightComp = toolkit.createComposite(rightSection);
		glayout = new GridLayout();
		glayout.numColumns = 1;					 
		rightComp.setLayout(glayout);	
		
		
		Label right = new Label(rightComp, 0);
		right.setText(Messages.EditLehrveranstaltungDialog_9);
		gd = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
		gd.horizontalSpan = 1;
		right.setLayoutData(gd);
		
		rightTree = new TreeViewer(rightComp, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 150;
		gd.minimumWidth = 250;
		gd.widthHint = 250;
		gd.horizontalSpan = 1;
		rightTree.getControl().setLayoutData(gd);
		rightTree.setContentProvider(new TeilnehmerTreeContentProvider());
		rightTree.setLabelProvider(new TeilnehmerTreeLabelProvider());
		rightTree.setInput(studenten);
		rightTree.refresh();

		rightSection.setClient(rightComp);
		//RIGHT SECTION		
		parentSection.setClient(parentComp);
		//PARENT SECTION    
		
		getInput();
		
        return parentSection;
    }
    
    public void getInput(){
    	teilnehmer = null;
    	toCheck = null;
    	studenten = null;
    	teilnehmer = TeilnehmerServiceHibernate.getInstance().getTeilnehmerFuerLehrveranstaltung(lv);
		toCheck = StudentServiceHibernate.getInstance().selectAll();
		studenten = getStudenten.check(teilnehmer, toCheck);
		leftTree.getTree().removeAll();
		rightTree.getTree().removeAll();
		leftTree.setInput(teilnehmer);
		rightTree.setInput(studenten);
		leftTree.refresh();
		rightTree.refresh();	
    }
}
