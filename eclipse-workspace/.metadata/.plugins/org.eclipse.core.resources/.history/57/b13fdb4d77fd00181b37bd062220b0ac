package de.htw.lsf.tools.mpa.bewertungen.gui.sorters;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import de.htw.lsf.tools.mpa.bewertungen.scheme.Bewertung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;

public class BewertungTableSorter extends ViewerComparator {

	private static final int ASCENDING = 0;
	private static final int DESCENDING = 1;

	private int propertyIndex;
	private int direction = ASCENDING;

	public BewertungTableSorter() {
		this.propertyIndex = 0;
		direction = ASCENDING;
	}

	public void setColumn(int column) {
		if (column == this.propertyIndex) {
			// Same column as last sort; toggle the direction
			direction = 1 - direction;
		} else {
			// New column; do an ascending sort
			this.propertyIndex = column;
			direction = ASCENDING;
		}
	}

	@Override
	public int compare(Viewer viewer, Object o1, Object o2) {
		Bewertung r1 = (Bewertung) o1;
		Bewertung r2 = (Bewertung) o2;
		if (r1.getTeilnehmer() instanceof Teilnehmer
				&& r2.getTeilnehmer() instanceof Teilnehmer) {
			return compareParticipant(r1, r2);
		} else {
			return compareWorkgroup(r1, r2);
		}
	}

	public int compareParticipant(Bewertung r1, Bewertung r2) {
		Teilnehmer p1 = (Teilnehmer) r1.getTeilnehmer();
		Teilnehmer p2 = (Teilnehmer) r2.getTeilnehmer();
		int rc = 0;
		switch (propertyIndex) {
		case 0:
			rc = p1.getStudent().getLastname().compareTo(
					p2.getStudent().getLastname());
			break;
		case 1:
			rc = p1.getStudent().getFirstname().compareTo(
					p2.getStudent().getFirstname());
			break;
		case 2:
			rc = p1.getStudent().getRegistrationNumber().compareTo(
					p2.getStudent().getRegistrationNumber());
			break;
		case 3:
			rc = compare(r1.getPunkte(), r2.getPunkte());
			break;
		default:
			rc = 0;
		}
		// If descending order, flip the direction
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}

	public static int compare(Double d1, Double d2) {
		if (d1 == null && d2 == null) {
			return 0;
		} else if (d1 == null && d2 != null) {
			return -1;
		} else if (d1 != null && d2 == null) {
			return 1;
		} else {
			return d1.compareTo(d2);
		}
	}

	public int compareWorkgroup(Bewertung r1, Bewertung r2) {
		Uebungsgruppe g1 = (Uebungsgruppe) r1.getTeilnehmer();
		Uebungsgruppe g2 = (Uebungsgruppe) r2.getTeilnehmer();
		int rc = 0;
		switch (propertyIndex) {
		case 0:
			rc = g1.getBezeichnung().compareTo(g2.getBezeichnung());
			break;
		case 1:
			rc = g1.getBezeichnung().compareTo(g2.getBezeichnung());
			break;
		case 2:
			rc = compare(r1.getPunkte(), r2.getPunkte());
			break;
		default:
			rc = 0;
		}
		// If descending order, flip the direction
		if (direction == DESCENDING) {
			rc = -rc;
		}
		return rc;
	}

}
