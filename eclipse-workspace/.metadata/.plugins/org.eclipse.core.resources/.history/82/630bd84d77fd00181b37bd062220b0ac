package de.htw.lsf.tools.mpa.bewertungen.gui.dialogs;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import de.htw.lsf.tools.mpa.bewertungen.scheme.TeilnehmerId;

public class WorkingGroupEditorInput implements IEditorInput {

	// Minimale Informationen, mit der das zu bearbeitende Objekt beschrieben
	// und referenziert werden kann
	private TeilnehmerId workingGroupId;

	public WorkingGroupEditorInput(TeilnehmerId workingGroupId) {
		super();
		this.workingGroupId = workingGroupId;
	}

	public TeilnehmerId getWorkingGroupId() {
		return this.workingGroupId;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	@Override
	public String getName() {
		// Text für den Benutzer, wird initial im Reiter des Editors angezeigt
		return "Working group " + workingGroupId;
	}

	@Override
	public String getToolTipText() {
		return getName();
	}

	@Override
	public IPersistableElement getPersistable() {
		// getPersistable muss nur implementiert werden, wenn das
		// Editor-Inputobjekt
		// über mehrere Anwendungssessions hinweg gelten soll, z.B. wenn eine
		// "Zuletzt geöffnete Dokumente"-Liste verwendet wird.
		return null;
	}

	@Override
	public boolean exists() {
		// Ggf. prüfen ob Objekt noch existiert und false zurückgeben wenn
		// nicht.
		// Vor allem relevant im Zusammenspiel mit getPersistable.
		return true;
	}

	@Override
	public Object getAdapter(Class adapterTarget) {
		// Editor-Input-Objekte können optional adaptierbar gestaltet werden
		return null;
	}

	// equals und hashCode müssen implementiert werden, damit nur ein
	// Editor für dasselbe Dokumente geöffnet werden kann

	@Override
	public int hashCode() {
		return this.getWorkingGroupId().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
        if (obj == null || !obj.getClass().isAssignableFrom(getClass()))
            return false;
		return (workingGroupId.equals(((WorkingGroupEditorInput) obj).workingGroupId));
	}

}
