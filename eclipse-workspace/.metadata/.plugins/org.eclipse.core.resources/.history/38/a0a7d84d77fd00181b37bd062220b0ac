package de.htw.lsf.tools.mpa.bewertungen.gui.editors;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.EditorPart;

import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.gui.handlers.EditGruppe;
import de.htw.lsf.tools.mpa.bewertungen.gui.providers.BewertungContentProvider;
import de.htw.lsf.tools.mpa.bewertungen.gui.providers.BewertungGruppeLabelProvider;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;
import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungServiceFactory;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;

public class BewertungGruppeEditor extends EditorPart {

	public static final String ID = Messages.BewertungGruppeEditor_0;
	private AbstractAssessment ln;
	private TableViewer viewer;
	private BewertungGruppeLabelProvider labelProvider;
	private List<Uebungsgruppe> ueb = null;
	private Logger logger = Logger.getLogger(BewertungGruppeEditor.class);

	public BewertungGruppeEditor() {
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		// ln = ((BewertungEditorInput) input).getLeistungsnachweis();
		Long ln_id = ((BewertungEditorInput) input).getLeistungsnachweisId();
		LeistungsnachweisService lns = LehrveranstaltungServiceFactory
				.getInstance(LehrveranstaltungServiceFactory.HIBERNATE)
				.getLeistungsnachweisService();
		ln = lns.find(ln_id);
		setPartName(ln.getLongName());
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		parent.setLayout(layout);

		Composite mainClient = toolkit.createComposite(parent, SWT.WRAP);

		// TabFolder tf = new TabFolder(mainClient, SWT.BORDER);

		layout = new GridLayout();
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		mainClient.setLayout(layout);
		mainClient.setLayoutData(new GridData(GridData.FILL_BOTH));
		Section section1 = toolkit.createSection(mainClient, Section.TITLE_BAR);
		section1.setText(Messages.BewertungGruppeEditor_1);// TODO edit
		Composite client1 = toolkit.createComposite(section1, SWT.WRAP);
		layout = new GridLayout();
		layout.marginWidth = 2;
		layout.marginHeight = 2;
		client1.setLayout(layout);
		section1.setClient(client1);
		section1.setLayoutData(new GridData(GridData.FILL_BOTH));

		viewer = new TableViewer(client1, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION);
		viewer.setContentProvider(new BewertungContentProvider());
		labelProvider = new BewertungGruppeLabelProvider(this.ln);
		createColumns(viewer);
		viewer.setLabelProvider(labelProvider);
		refreshData();

		// Layout the viewer
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);

	}

	public void setUeb(List<Uebungsgruppe> ueb) {
		this.ueb = ueb;
		this.refreshData();
	}

	public void setAbstrakterLeistungsnachweis(AbstractAssessment ln) {
		// BewertungEditorInput input = new BewertungEditorInput(ln);
		BewertungEditorInput input = new BewertungEditorInput(null, ln.getNumber());
		setInput(input);
		this.ln = ln;
		setPartName(ln.getLongName());
		labelProvider.setAbstrakterLeistungsnachweis(ln);
	}

	private void refreshData() {
		try {

			viewer.setInput(ueb);
			for (int i = 0; i < viewer.getTable().getColumnCount(); i++) {
				viewer.getTable().getColumn(i).pack();
			}
			viewer.refresh();
		} catch (Exception e) {
			logger.log(Level.WARN, Messages.BewertungGruppeEditor_2, e);
		}
	}

	private void createColumns(final TableViewer viewer) {

		String[] titles = labelProvider.getTitles();
		int[] bounds = labelProvider.getBounds();
		for (int i = 0; i < titles.length; i++) {
			TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
					SWT.NONE);
			final TableColumn column = viewerColumn.getColumn();
			column.setText(titles[i]);
			column.setWidth(bounds[i]);
			column.setResizable(true);
			column.setMoveable(false);
			// viewerColumn.setEditingSupport(new
			// BewertungEditingSupport(viewer,
			// labelProvider.getLeistungsnachweisForColumn(i)));
		}
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}

	@Override
	public void setFocus() {
	}

}
