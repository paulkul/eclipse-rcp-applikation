package de.htw.lsf.tools.mpa.bewertungen.scheme.dao;

import java.util.List;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;

public interface TeilnehmerDAO extends AbstractDAO<AbstrakterTeilnehmer> {

	public boolean exists(Teilnehmer t);

	public boolean exists(Uebungsgruppe ug);

	public List<Teilnehmer> getTeilnehmerByMatrNr(Long matrNr);

	public List<Teilnehmer> getTeilnehmerFuerLehrveranstaltung(
			Lehrveranstaltung lehrveranstaltung);

	public Teilnehmer getTeilnehmer(Lehrveranstaltung lehrveranstaltung,
			long matrikelNummer);

	public List<Teilnehmer> getTeilnehmerFuerUebungsgruppe(Uebungsgruppe gruppe);

}
