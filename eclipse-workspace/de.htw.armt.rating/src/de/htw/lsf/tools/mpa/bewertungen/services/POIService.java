package de.htw.lsf.tools.mpa.bewertungen.services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.structures.ExportData;
import de.htw.lsf.tools.mpa.bewertungen.structures.ExportDataEntry;
import de.htw.lsf.tools.mpa.bewertungen.structures.ImportData;
import de.htw.lsf.tools.mpa.bewertungen.structures.ImportDataEntry;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Term;
import de.htw.lsf.tools.mpa.student.scheme.Student;
import de.htw.lsf.tools.mpa.student.services.StudentServiceFactory;

public class POIService {

	public static ImportData loadData(String filename) throws Exception {
		ImportData data = null;
		String semesterPrefix = Messages.POIService_0;
		int cols = 0;
		String semester = null;
		String[] headers;
		List<String[]> values = new ArrayList<String[]>();
//		OPCPackage p = OPCPackage.open(new FileInputStream(filename));
//		Workbook wb = new XSSFWorkbook(p);
		// XSSFWorkbook wb = new XSSFWorkbook(filename);
//		 Workbook wb = new XSSFWorkbook(new FileInputStream(filename));
		 Workbook wb = new HSSFWorkbook(new FileInputStream(filename));
		if (wb.getNumberOfSheets() == 1) {
			Sheet sheet = wb.getSheetAt(0);
			String sname = wb.getSheetName(0);
			if ((sname != null)
					&& sname.equalsIgnoreCase(Messages.POIService_1)) {
				int rows = sheet.getPhysicalNumberOfRows();
				if (rows > 4) {
					Row row = sheet.getRow(0);
					int cells = row.getPhysicalNumberOfCells();
					if (cells == 1) {
						Cell cell = row.getCell(0);
						String value = cell.getStringCellValue();
						if (value != null) {
							if (value.startsWith(semesterPrefix)) {
								semester = value.substring(semesterPrefix
										.length());
							}
						}
					}
					row = sheet.getRow(3);
					cells = row.getPhysicalNumberOfCells();
					cols = cells;
					headers = new String[cols];
					int[] columnIndizes = new int[cols];
					for (int c = 0; c < cells; c++) {
						Cell cell = row.getCell(c);
						headers[c] = cell.getStringCellValue();
						columnIndizes[c] = cell.getColumnIndex();
					}
					for (int r = 4; r <= rows; r++) {
						row = sheet.getRow(r);
						if (row == null) {
							continue;
						}
						cells = row.getPhysicalNumberOfCells();
						if (cells > 0) {
							String[] vs = new String[cols];
							for (int c = 0; c < cols; c++) {
								Cell cell = row.getCell(columnIndizes[c]);
								if (cell != null) {
									int ct = cell.getCellType();
									if (ct == Cell.CELL_TYPE_NUMERIC) {
										vs[c] = String.valueOf((long) cell
												.getNumericCellValue());
									} else {
										vs[c] = cell.getStringCellValue();
									}
								}
							}
							values.add(vs);
						}
					}
					data = new ImportData(semester, headers, values);
				} else {
					throw new Exception();
				}
			} else {
				throw new Exception();
			}
		} else {
			throw new Exception();
		}
		return data;
	}

	public static void storeData(ImportData data) throws Exception {
		String semester = data.getSemester();
		List<ImportDataEntry> entries = data.getEntries();
		LehrveranstaltungServiceFactory factory = LehrveranstaltungServiceFactory
				.getInstance(LehrveranstaltungServiceFactory.HIBERNATE);
		StudentServiceFactory studentFactory = StudentServiceFactory
				.getInstance(StudentServiceFactory.HIBERNATE);
		BewertungServiceFactory bewertungFactory = BewertungServiceFactory
				.getInstance(BewertungServiceFactory.HIBERNATE);
		Term sem = factory.getSemesterService().getSemesterBeiBezeichnung(
				semester);
		if (sem == null) {
			sem = factory.getSemesterService().insert(new Term(semester));
		}
		for (ImportDataEntry entry : entries) {
			if ((entry.getVeranstaltung() != null)
					&& (entry.getVeranstaltung().length() > 0)
					&& (entry.getVeranstaltungsnummer() != null)
					&& (entry.getVeranstaltungsnummer().length() > 0)) {
				long moduleNr = Long.parseLong(entry.getVeranstaltungsnummer()
						.substring(0, 6));
				Module modul = null;
				if (!factory.getModulService().exists(moduleNr)) {
					modul = new Module(entry.getVeranstaltung(), moduleNr);
					modul = factory.getModulService().insert(modul);
				} else {
					modul = factory.getModulService().find(moduleNr);
				}

				if (entry.getVerID() > 0) {
					Lehrveranstaltung lv = new Lehrveranstaltung();
					lv.setLehrveranstaltungsID(entry.getVerID());
					lv.setModul(modul);
					lv.setSemester(sem);
					if (!factory.getLehrveranstaltungService().exists(
							lv.getLehrveranstaltungsID())) {
						lv = factory.getLehrveranstaltungService().insert(lv);
					}
					else {
						lv = factory.getLehrveranstaltungService().find(lv.getLehrveranstaltungsID());
					}
					Student student = null;
					long matrNr = entry.getMatrikelnummer();
					if ((matrNr > 0)
							&& (entry.getNachname() != null)
							&& (entry.getNachname().length() > 0)) {
						if (!studentFactory.getStudentService().exists(
								matrNr)) {
							student = new Student(
									matrNr, entry.getNachname(),
									entry.getVorname(), entry.getNamenszusatz(),
									entry.getTitel(), entry.isMaenlich(), entry
											.getEmail());
							student = studentFactory.getStudentService()
									.insert(student);
						} else {
							student = studentFactory.getStudentService().find(matrNr);
							student = studentFactory.getStudentService()
									.update(student);
						}
						Teilnehmer tn = new Teilnehmer(lv, student);
						if (!bewertungFactory.getTeilnehmerService().exists(tn)) {
							bewertungFactory.getTeilnehmerService().insert(tn);
						}
					}
				}
			}
		}
	}

	public static void updateFromXLS(String filename) throws Exception {
		ImportData data = loadData(filename);
		storeData(data);
	}

	public static ExportData loadExportData(String filename) throws Exception {
		ExportData data = null;
		int cols = 0;
		String modulnummer = null;
		String[] headers;
		List<String[]> values = new ArrayList<String[]>();
		HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(filename));
		if (wb.getNumberOfSheets() == 1) {
			HSSFSheet sheet = wb.getSheetAt(0);
			int rows = sheet.getPhysicalNumberOfRows();
			if (rows > 4) {
				HSSFRow row = sheet.getRow(0);
				int cells = row.getPhysicalNumberOfCells();
				if (cells == 1) {
					HSSFCell cell = row.getCell(0);
					modulnummer = cell.getStringCellValue();
				}
				row = sheet.getRow(3);
				cells = row.getPhysicalNumberOfCells();
				cols = cells;
				headers = new String[cols];
				int[] columnIndizes = new int[cols];
				for (int c = 0; c < cells; c++) {
					HSSFCell cell = row.getCell(c);
					headers[c] = cell.getStringCellValue();
					columnIndizes[c] = cell.getColumnIndex();
				}
				for (int r = 4; r <= rows; r++) {
					row = sheet.getRow(r);
					if (row == null) {
						continue;
					}
					cells = row.getPhysicalNumberOfCells();
					if (cells > 0) {
						String[] vs = new String[cols];
						if ("endHISsheet".equals(row.getCell(columnIndizes[0]).getStringCellValue()))
							break;
						for (int c = 0; c < cols; c++) {
							HSSFCell cell = row.getCell(columnIndizes[c]);
							if (cell != null) {
								int ct = cell.getCellType();
								if (ct == Cell.CELL_TYPE_NUMERIC) {
									vs[c] = String.valueOf((long) cell
											.getNumericCellValue());
								} else {
									vs[c] = cell.getStringCellValue();
								}
							}
						}
						values.add(vs);
					}
				}
				data = new ExportData(modulnummer, headers, values);
				data.setWorkbook(wb);
			} else {
				throw new Exception();
			}
		} else {
			throw new Exception();
		}
		return data;
	}

	public static void storeExportData(ExportData data, String filename)
			throws Exception {
		Workbook wb = data.getWorkbook();
		if (wb.getNumberOfSheets() == 1) {
			Sheet sheet = wb.getSheetAt(0);
			int rows = sheet.getPhysicalNumberOfRows();
			if (rows > 4) {
				Row row = sheet.getRow(3);
				int cells = row.getPhysicalNumberOfCells();
				String header = null;
				int columnIndex = -1;
				int mnColumnIndex = -1;
				for (int c = 0; c < cells; c++) {
					Cell cell = row.getCell(c);
					header = cell.getStringCellValue();
					if (header.equalsIgnoreCase(Messages.POIService_2)) {
						columnIndex = cell.getColumnIndex();
					} else if (header.equalsIgnoreCase(Messages.POIService_3)) {
						mnColumnIndex = cell.getColumnIndex();
					}
				}
				for (int r = 4; r <= rows; r++) {
					row = sheet.getRow(r);
					if (row == null) {
						continue;
					}
					Cell cell = row.getCell(mnColumnIndex);
					if (cell != null) {
						int ct = cell.getCellType();
						String mn;
						if (ct == Cell.CELL_TYPE_NUMERIC) {
							mn = String.valueOf((long) cell
									.getNumericCellValue());
						} else {
							mn = cell.getStringCellValue();
						}
						cell = row.getCell(columnIndex);
						if (cell != null) {
							ExportDataEntry entry = data
									.getEntryByMatrikelnummer(Long
											.parseLong(mn));
							String value;
							if (entry.getBerechneteBewertung() > 0) {
								value = String.valueOf(entry
										.getBerechneteBewertung());
							} else if (entry.getBewertung() > 0) {
								value = String.valueOf(entry.getBewertung());
							} else {
								value = ""; //$NON-NLS-1$
							}
							cell.setCellValue(value);
						}
					}
				}
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(filename);
					wb.write(fos);
				} finally {
					if (fos != null) {
						fos.close();
					}
				}
			} else {
				throw new Exception();
			}
		} else {
			throw new Exception();
		}
	}

}
