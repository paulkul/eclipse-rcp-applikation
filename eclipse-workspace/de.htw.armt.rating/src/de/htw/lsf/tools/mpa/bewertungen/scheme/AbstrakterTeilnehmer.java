package de.htw.lsf.tools.mpa.bewertungen.scheme;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;

@Entity(name = "teilnehmer")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DISC", discriminatorType = DiscriminatorType.STRING, length = 3)
@DiscriminatorValue("AT")
@IdClass(TeilnehmerId.class)
public abstract class AbstrakterTeilnehmer extends ModelObject {

	/** */
	private static final long serialVersionUID = -8664679248358943806L;
	private Long number;
	private Lehrveranstaltung veranstaltung;

	public AbstrakterTeilnehmer() {
		super();
	}

	public AbstrakterTeilnehmer(long moId, Lehrveranstaltung veranstaltung) {
		super();
		this.number = moId;
		this.veranstaltung = veranstaltung;
	}

	public AbstrakterTeilnehmer(Lehrveranstaltung veranstaltung) {
		super();
		this.veranstaltung = veranstaltung;
	}

	@ManyToOne(cascade = { CascadeType.MERGE })
	@Id
	@JoinColumn(name = "lehrveranstaltung")
	public Lehrveranstaltung getVeranstaltung() {
		return veranstaltung;
	}

	public void setVeranstaltung(Lehrveranstaltung veranstaltung) {
		this.veranstaltung = veranstaltung;
	}

	@Id
	@Column(name = "id")
	// @GeneratedValue(strategy=GenerationType.TABLE)
	@Override
	public Long getNumber() {
		return number;
	}

	@Override
	public void setNumber(Long number) {
		this.number = number;
	}

	@Transient
	public TeilnehmerId getTeilnehmerId() {
		return new TeilnehmerId(this.getNumber(), this.getVeranstaltung()
				.getLehrveranstaltungsID());
	}
	
	@Override
	public int hashCode() {
		return getTeilnehmerId().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		AbstrakterTeilnehmer other = (AbstrakterTeilnehmer) obj;
		if (getTeilnehmerId() == null) {
			if (other.getTeilnehmerId() != null)
				return false;
		} else if (!getTeilnehmerId().equals(other.getTeilnehmerId()))
			return false;
		return true;
	}

}
