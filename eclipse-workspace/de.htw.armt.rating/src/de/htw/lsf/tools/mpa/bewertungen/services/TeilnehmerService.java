/*
 * TeilnehmerService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services;

import java.math.BigDecimal;
import java.util.List;

import de.htw.lsf.tools.mpa.base.services.Service;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;

/**
 * @author V.Medvid
 */
public interface TeilnehmerService extends Service<AbstrakterTeilnehmer> {

	public List<Teilnehmer> getTeilnehmerByMatrNr(Long matrNr);

	public boolean exists(Teilnehmer t);

	public boolean exists(Uebungsgruppe ug);

	public List<Teilnehmer> getTeilnehmerFuerLehrveranstaltung(
			Lehrveranstaltung lehrveranstaltung);

	public BigDecimal konvertiereZuEndnote(BigDecimal punkte);

	public boolean berechneEndnoten(Lehrveranstaltung lehrveranstaltung);

	public Teilnehmer getTeilnehmer(Lehrveranstaltung lehrveranstaltung,
			long matrikelNummer);

	public List<Teilnehmer> getTeilnehmerFuerUebungsgruppe(Uebungsgruppe gruppe);
}
