package de.htw.lsf.tools.mpa.bewertungen.scheme;

import java.io.Serializable;

public class BewertungId implements Serializable {
	
	/** */
	private static final long serialVersionUID = 1L;

	private Long leistungsnachweis;
	private TeilnehmerId teilnehmer;

	public BewertungId() {
		super();
	}
	
	public BewertungId(Long leistungsnachweis, TeilnehmerId teilnehmer) {
		super();
		this.leistungsnachweis = leistungsnachweis;
		this.teilnehmer = teilnehmer;
	}
	
	public Long getLeistungsnachweis() {
		return leistungsnachweis;
	}

	public void setLeistungsnachweis(Long leistungsnachweis) {
		this.leistungsnachweis = leistungsnachweis;
	}

	public TeilnehmerId getTeilnehmer() {
		return teilnehmer;
	}

	public void setTeilnehmer(TeilnehmerId teilnehmer) {
		this.teilnehmer = teilnehmer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 3;
		result = prime
				* result
				+ ((leistungsnachweis == null) ? 0
						: leistungsnachweis.hashCode());
		result = prime * result
				+ ((teilnehmer == null) ? 0 : teilnehmer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		BewertungId other = (BewertungId) obj;
		if (leistungsnachweis == null) {
			if (other.leistungsnachweis != null)
				return false;
		} else if (!leistungsnachweis.equals(other.leistungsnachweis))
			return false;
		if (teilnehmer == null) {
			if (other.teilnehmer != null)
				return false;
		} else if (!teilnehmer.equals(other.teilnehmer))
			return false;
		return true;
	}

}
