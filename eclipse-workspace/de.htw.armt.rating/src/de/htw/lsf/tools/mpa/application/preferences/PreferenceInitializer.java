package de.htw.lsf.tools.mpa.application.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.htw.lsf.tools.mpa.bewertungen.Activator;
import de.htw.lsf.tools.mpa.bewertungen.Messages;


/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.P_STRING_MAIL_CC, ""); //$NON-NLS-1$
		store.setDefault(PreferenceConstants.P_STRING_MAIL_MESSAGE, Messages.PreferenceInitializer_1);
		store.setDefault(PreferenceConstants.P_STRING_MAIL_SUBJECT, Messages.PreferenceInitializer_2);
		store.setDefault(PreferenceConstants.P_STRING_MAIL_WILDCARD_LN, "</ln/>"); //$NON-NLS-1$
		store.setDefault(PreferenceConstants.P_STRING_MAIL_WILDCARD_LV, "</lv/>"); //$NON-NLS-1$
		store.setDefault(PreferenceConstants.P_STRING_MAIL_WILDCARD_PK, "</pk/>"); //$NON-NLS-1$
		store.setDefault(PreferenceConstants.P_STRING_MAIL_WILDCARD_TN, "</tn/>"); //$NON-NLS-1$
	}
}
