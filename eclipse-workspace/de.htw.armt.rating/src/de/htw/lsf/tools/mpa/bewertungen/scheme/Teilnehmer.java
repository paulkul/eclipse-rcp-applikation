package de.htw.lsf.tools.mpa.bewertungen.scheme;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import de.htw.lsf.tools.mpa.student.scheme.Student;

@Entity
@DiscriminatorValue("T")
public class Teilnehmer extends AbstrakterTeilnehmer {
	// TODO rename in Participant

	/**
	 * 
	 */
	private static final long serialVersionUID = -3611618446582631595L;

	private Student student;
	// @ManyToOne(fetch=FetchType.EAGER, cascade={CascadeType.MERGE,
	// CascadeType.PERSIST}, optional=true)
	// private Uebungsgruppe uebungsgruppe;

	private Set<Uebungsgruppe> uebungsgruppe = new HashSet<Uebungsgruppe>(10);
	private Double endnote;
	private boolean manuellHinzugefuegt = false;

	public Teilnehmer() {
		super();
	}

	public Teilnehmer(long id, Lehrveranstaltung lehrveranstaltung,
			Student student) {
		this(id, lehrveranstaltung, student, false);
	}

	public Teilnehmer(Lehrveranstaltung lehrveranstaltung, Student student) {
		this(lehrveranstaltung, student, false);
	}

	// public Teilnehmer(Lehrveranstaltung lehrveranstaltung, Student
	// student,Uebungsgruppe uebungsgruppe){
	// this(lehrveranstaltung, student, false, uebungsgruppe);
	// }

	public Teilnehmer(long id, Lehrveranstaltung lehrveranstaltung,
			Student student, boolean manuellHinzugefuegt) {
		super(id, lehrveranstaltung);
		this.student = student;
		this.manuellHinzugefuegt = manuellHinzugefuegt;
	}

	public Teilnehmer(Lehrveranstaltung lehrveranstaltung, Student student,
			boolean manuellHinzugefuegt) {
		super(lehrveranstaltung);
		this.student = student;
		this.manuellHinzugefuegt = manuellHinzugefuegt;
	}

	// public Teilnehmer(Lehrveranstaltung lehrveranstaltung, Student student,
	// boolean manuellHinzugefuegt, Uebungsgruppe uebungsgruppe) {
	// super(lehrveranstaltung);
	// this.student = student;
	// this.manuellHinzugefuegt = manuellHinzugefuegt;
	// this.uebungsgruppe = uebungsgruppe;
	// }

	
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST })
	@JoinColumn(name = "student")
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Column(name = "manuellhinzugefuegt")
	public boolean isManuellHinzugefuegt() {
		return manuellHinzugefuegt;
	}

	public void setManuellHinzugefuegt(boolean manuellHinzugefuegt) {
		this.manuellHinzugefuegt = manuellHinzugefuegt;
	}

	public void addUebungsgruppe(Uebungsgruppe uebungsgruppe) {
		this.uebungsgruppe.add(uebungsgruppe);
//		uebungsgruppe.addTeilnehmer(this);
	}

	public void removeUebungsgruppe(Uebungsgruppe uebungsgruppe) {
		this.uebungsgruppe.remove(uebungsgruppe);
//		uebungsgruppe.removeTeilnehmer(this);
	}

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST })
	public Set<Uebungsgruppe> getUebungsgruppe() {
		return this.uebungsgruppe;
	}

	/**
	 * dont use
	 */
	public void setUebungsgruppe(Set<Uebungsgruppe> uebungsgruppe) {
		this.uebungsgruppe = uebungsgruppe;
	}

	@Column(name = "endnote")
	public Double getEndnote() {
		return endnote;
	}

	public void setEndnote(Double endnote) {
		this.endnote = endnote;
	}

	public boolean hasAktiveWorkingGroup() {
		for (Uebungsgruppe workingGroup : this.getUebungsgruppe()) {
			if (workingGroup.isActive()) {
				return true;
			}
		}
		return false;
	}

}
