package de.htw.lsf.tools.mpa.application.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	public static final String P_STRING_MAIL_SUBJECT = "mail_subject";
	public static final String P_STRING_MAIL_CC = "mail_cc";
	public static final String P_STRING_MAIL_MESSAGE = "mail_message";
	public static final String P_STRING_MAIL_WILDCARD_LV = "mail_wildcard_lv";
	public static final String P_STRING_MAIL_WILDCARD_LN = "mail_wildcard_ln";
	public static final String P_STRING_MAIL_WILDCARD_PK = "mail_wildcard_pk";
	public static final String P_STRING_MAIL_WILDCARD_TN = "mail_wildcard_tn";
}
