/*
 * TeilnehmerService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.HibernateException;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.base.utils.HibernateUtil;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.UebungsgruppeDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl.UebungsgruppeDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.services.UebungsgruppeService;

/**
 * @author V.Medvid
 */
public class UebungsgruppeServiceHibernate extends AbstractServiceHibernate<Uebungsgruppe>
		implements UebungsgruppeService {

	private static UebungsgruppeServiceHibernate instance = null;
	
	public UebungsgruppeServiceHibernate() {
		super(Uebungsgruppe.class);
	}

	@Override
	public AbstractDAO<Uebungsgruppe> getDAO(EntityManager entityManager) {
		UebungsgruppeDAOHibernate dao = UebungsgruppeDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static UebungsgruppeServiceHibernate getInstance() {
		if (instance == null) {
			instance = new UebungsgruppeServiceHibernate();
		}
		return instance;
	}

	@Override
	public List<Uebungsgruppe> getWorkingGroupsByMatrNr(long matrNr) throws HibernateException {
		EntityManager entityManager = null;
		List<Uebungsgruppe> objects = new ArrayList<Uebungsgruppe>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((UebungsgruppeDAO) getDAO(entityManager)).getWorkingGroupByMatrNr(matrNr);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}

	

	@Override
	public List<Uebungsgruppe> getUebungsgruppenFuerLehrveranstaltung(
			Lehrveranstaltung lehrveranstaltung, boolean activeOnly) throws HibernateException {
		EntityManager entityManager = null;
		List<Uebungsgruppe> objects = new ArrayList<Uebungsgruppe>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((UebungsgruppeDAO) getDAO(entityManager)).getUebungsgruppeFuerLehrveranstaltung(
					lehrveranstaltung, activeOnly);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}
}
