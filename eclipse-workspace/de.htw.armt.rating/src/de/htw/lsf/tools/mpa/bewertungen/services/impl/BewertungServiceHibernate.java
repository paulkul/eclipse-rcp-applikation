/*
 * BewertungService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.base.utils.HibernateUtil;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Bewertung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.BewertungDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl.BewertungDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.services.BewertungService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.*;

/**
 * @author V.Medvid
 */
public class BewertungServiceHibernate extends
		AbstractServiceHibernate<Bewertung> implements BewertungService {

	private static final Logger LOGGER = Logger
			.getLogger(BewertungServiceHibernate.class);

	private static BewertungServiceHibernate instance = null;

	private BewertungDAO dao = null;

	public BewertungServiceHibernate() {
		super(Bewertung.class);
	}

	@Override
	public AbstractDAO<Bewertung> getDAO(EntityManager entityManager) {
		BewertungDAOHibernate dao = BewertungDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static BewertungServiceHibernate getInstance() {
		if (instance == null) {
			instance = new BewertungServiceHibernate();
		}
		return instance;
	}

	@Override
	public double getBewertung(AbstrakterTeilnehmer tn, AssessmentGroup ln) {
		EntityManager session = null;
		double bewertung = 0;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			bewertung = this.getBewertung(tn, ln, session);
			session.getTransaction().commit();
		} catch (RuntimeException e) {
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			LOGGER.log(Level.ERROR, "Error:", e);
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return bewertung;
	}

	@Override
	public double getBewertung(AbstrakterTeilnehmer tn, AssessmentGroup ln,
			EntityManager session) {
		double bewertung = 0;
		String formel = ln.getFormula();
		List<AbstractAssessment> tln = ln.getAssessments();
		Map<String, Double> bws = new HashMap<String, Double>();
		for (AbstractAssessment aln : tln) {
			double wert = 0;
			if (aln instanceof Assessment) {
				Bewertung b = ((BewertungDAO) getDAO(session)).getBewertung(tn,
						(Assessment) aln);
				Assessment eln = (Assessment) aln;
				if (b != null && b.getPunkte() != null) {
					wert = b.getPunkte();
				} else if (b == null && eln.isMandatory()) {
					wert = -1;
				}
			} else if (aln instanceof AssessmentGroup) {
				wert = getBewertung(tn, (AssessmentGroup) aln, session);
			}
			bws.put(aln.getAbbreviation(), wert);
		}
		bewertung = ScriptService.parseFormel(formel, bws);
		return bewertung;
	}

	@Override
	public Bewertung getBewertung(AbstrakterTeilnehmer tn, Assessment ln) {
		EntityManager session = null;
		Bewertung bewertung = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			bewertung = ((BewertungDAO) getDAO(session)).getBewertung(tn, ln);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return bewertung;
	}

	@Override
	public Bewertung getTeilnehmerBewertung(Teilnehmer tn, Assessment ln) {
		EntityManager session = null;
		Bewertung bewertung = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			bewertung = ((BewertungDAO) getDAO(session))
					.getTeilnehmerBewertung(tn, ln);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return bewertung;
	}

	// public Map<AbstrakterTeilnehmer, Map<Leistungsnachweis, Bewertung>>
	// determineBewertungen(
	// Long lehrveranstaltungId, List<Leistungsnachweis> leistungsnachweise) {
	// Map<AbstrakterTeilnehmer, Map<Leistungsnachweis, Bewertung>> result =
	// null;
	// Session session = null;
	// try {
	// session = HibernateUtil.getSessionFactory().openSession();
	// session.beginTransaction();
	//
	// dao = ((BewertungDAO) getDAO(session));
	// result = determineBewertung_intern(lehrveranstaltungId,
	// leistungsnachweise);
	//
	// session.getTransaction().commit();
	// } catch (HibernateException e) {
	// if (session != null && session.isOpen()) {
	// session.getTransaction().rollback();
	// }
	// throw e;
	// } finally {
	// if (session != null && session.isOpen()) {
	// session.close();
	// }
	// }
	// return result;
	// }

	public Map<AbstrakterTeilnehmer, Bewertung> determineBewertungen(
			List<? extends AbstrakterTeilnehmer> teilnehmer,
			Assessment leistungsnachweis) {
		Map<AbstrakterTeilnehmer, Bewertung> result = null;
		EntityManager session = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();

			dao = ((BewertungDAO) getDAO(session));

			result = determineBewertungen2(teilnehmer, leistungsnachweis);

			session.getTransaction().commit();
		} catch (PersistenceException e) {
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return result;
	}

	private Map<AbstrakterTeilnehmer, Bewertung> determineBewertungen2(
			List<? extends AbstrakterTeilnehmer> participants,
			Assessment assessment) {
		Map<AbstrakterTeilnehmer, Bewertung> result = new HashMap<AbstrakterTeilnehmer, Bewertung>();
		for (AbstrakterTeilnehmer participant : participants) {
			Bewertung rating = dao.getBewertung(participant,
					(Assessment) assessment);
			// TODO Teilnahmepflicht
			// rating = (rating == null) ? new Bewertung(assessment,
			// participant,
			// null) : rating;
			result.put(participant, rating);
		}
		return result;
	}

	public Map<Teilnehmer, Map<AbstractAssessment, Double>> determineBewertungen(
			List<Teilnehmer> teilnehmer, AssessmentGroup leistungsnachweis) {
		Map<Teilnehmer, Map<AbstractAssessment, Double>> result;
		EntityManager session = null;
		boolean activeTx = false;
		try {
			session = HibernateUtil.getCurrentSession(); // TODO This service is
															// called from a
															// service
			activeTx = session.getTransaction().isActive();
			if (!activeTx) {
				session.getTransaction().begin();
			}

			dao = ((BewertungDAO) getDAO(session));
			result = calculateBewertung2(teilnehmer, leistungsnachweis);

			if (!activeTx) {
				session.getTransaction().commit();
			}
		} catch (PersistenceException e) {
			if (!activeTx && session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return result;
	}

	public Map<Teilnehmer, Map<AbstractAssessment, Double>> calculateBewertung2(
			List<Teilnehmer> participants, AssessmentGroup assessment) {
		Map<Teilnehmer, Map<AbstractAssessment, Double>> result = new HashMap<Teilnehmer, Map<AbstractAssessment, Double>>();
		for (Teilnehmer participant : participants) {
			result.put(participant,
					calculateBewertung2(participant, assessment));
		}
		return result;
	}

	public Map<AbstractAssessment, Double> calculateBewertung2(
			Teilnehmer participant, AbstractAssessment abstractAssessment) {
		Map<AbstractAssessment, Double> result = new HashMap<AbstractAssessment, Double>();
		Double points = null;
		if (abstractAssessment instanceof Assessment) {
			Assessment assessment = (Assessment) abstractAssessment;
			Bewertung rating = null;
			if (assessment.isGroupAssessment()) {
				rating = dao.getBewertung(participant.getUebungsgruppe(),
						(Assessment) assessment);
			} else {
				rating = dao.getBewertung(participant, (Assessment) assessment);
			}
			// TODO Teilnahmepflicht
			// if (rating == null && !assessment.isTeilnahmepflicht()) {
			// points = 0.0;
			// } else {
			points = (rating == null) ? null : rating.getPunkte();
			// }
		} else if (abstractAssessment instanceof AssessmentGroup) {
			AssessmentGroup dividableAssessment = (AssessmentGroup) abstractAssessment;
			Map<String, Double> ratingParts = new HashMap<String, Double>();
			boolean isTeilnahmepflichtErfuellt = false;
			for (AbstractAssessment assessmentPart : dividableAssessment
					.getAssessments()) {
				result.putAll(calculateBewertung2(participant, assessmentPart));
				Double partPoints = result.get(assessmentPart);
				// ratingParts.put(assessmentPart.getKuerzel(),
				// (partPoints == null) ? 0.0 : partPoints);
				if (assessmentPart.isMandatory() && (partPoints != null)) {
					isTeilnahmepflichtErfuellt = true;
				}
				ratingParts.put(assessmentPart.getAbbreviation(), partPoints);
			}
			if (!dividableAssessment.isMandatory()
					|| isTeilnahmepflichtErfuellt) {
				points = ScriptService.parseFormel(
						dividableAssessment.getFormula(), ratingParts);
			} else {
				points = null;
			}
		}
		result.put(abstractAssessment, points);
		return result;
	}

	@Override
	public void updateRatings(List<Bewertung> ratings) {
		EntityManager session = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();

			dao = ((BewertungDAO) getDAO(session));
			updateRatings_intern(ratings);

			session.getTransaction().commit();
		} catch (PersistenceException e) {
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
	}

	private void updateRatings_intern(List<Bewertung> ratings) {
		List<Bewertung> updatedRatings = new ArrayList<Bewertung>(ratings);
		for (Bewertung rating : updatedRatings) {
			if ((rating.getPunkte() == null) && (rating.getNumber() != null)) {
				dao.delete(rating);
				rating.setNumber(null); // NOTE: removing the id is essential in
									// case of a following saveOrUpdate
			} else if (rating.getPunkte() != null) {
				replace(rating, dao.saveOrUpdate(rating), ratings);
			}
		}
	}

	private <T> void replace(T oldElem, T newElem, List<T> list) {
		list.set(list.indexOf(oldElem), newElem);
	}

	@Override
	public List<Bewertung> determineAssessments(
			List<? extends AbstrakterTeilnehmer> participants,
			Assessment assessment) {
		List<Bewertung> result;
		EntityManager session = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();

			dao = ((BewertungDAO) getDAO(session));
			result = determineAssessments_intern(participants, assessment);

			session.getTransaction().commit();
		} catch (PersistenceException e) {
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return result;
	}

	private List<Bewertung> determineAssessments_intern(
			List<? extends AbstrakterTeilnehmer> participants,
			Assessment assessment) {
		List<Bewertung> result = new ArrayList<Bewertung>();
		for (AbstrakterTeilnehmer participant : participants) {
			Bewertung rating = dao.getBewertung(participant,
					(Assessment) assessment);
			if (rating == null) {
				rating = new Bewertung(assessment, participant, null);
				// dao.insert(rating);
			}
			result.add(rating);
		}
		return result;
	}

	// private Map<AbstrakterTeilnehmer, Map<Leistungsnachweis, Bewertung>>
	// determineBewertung_intern(
	// Long lehrveranstaltungId, List<Leistungsnachweis> leistungsnachweise) {
	// Map<AbstrakterTeilnehmer, Map<Leistungsnachweis, Bewertung>> result = new
	// HashMap<AbstrakterTeilnehmer, Map<Leistungsnachweis, Bewertung>>();
	// List<Bewertung> bewertungen = dao
	// .findByLehrveranstaltungIdAndLeistungsnachweisen(
	// lehrveranstaltungId, leistungsnachweise);
	// for (Bewertung bewertung : bewertungen) {
	// if (!result.containsKey(bewertung.getTeilnehmer())) {
	// result.put(bewertung.getTeilnehmer(),
	// new HashMap<Leistungsnachweis, Bewertung>());
	// }
	// result.get(bewertung.getTeilnehmer()).put(
	// bewertung.getLeistungsnachweis(), bewertung);
	// }
	// return result;
	// }

	// private Map<AbstrakterTeilnehmer, Map<LeistungsnachweisMitTeilleistungen,
	// Double>> determineBewertung_int(
	// List<? extends AbstrakterTeilnehmer> teilnehmer,
	// List<LeistungsnachweisMitTeilleistungen> leistungsnachweise) {
	// Map<AbstrakterTeilnehmer, Map<LeistungsnachweisMitTeilleistungen,
	// Double>> result = new HashMap<AbstrakterTeilnehmer,
	// Map<LeistungsnachweisMitTeilleistungen, Double>>();
	// for (AbstrakterTeilnehmer t : teilnehmer) {
	// if (!result.containsKey(t)) {
	// result
	// .put(
	// t,
	// new HashMap<LeistungsnachweisMitTeilleistungen, Double>());
	// }
	// for (LeistungsnachweisMitTeilleistungen leistungsnachweis :
	// leistungsnachweise) {
	// result.get(t).put(leistungsnachweis,
	// calculateBewertung(t, leistungsnachweis));
	// }
	// }
	//
	// return result;
	// }

	// public Double calculateBewertung(AbstrakterTeilnehmer teilnehmer,
	// AbstrakterLeistungsnachweis leistungsnachweis) {
	// if (leistungsnachweis instanceof Leistungsnachweis) {
	// Bewertung bewertung = dao.getBewertung(teilnehmer,
	// (Leistungsnachweis) leistungsnachweis);
	// return (bewertung == null) ? 0 : bewertung.getPunkte();
	// // TODO Teilnahmepflicht
	// } else if (leistungsnachweis instanceof
	// LeistungsnachweisMitTeilleistungen) {
	// LeistungsnachweisMitTeilleistungen lmt =
	// (LeistungsnachweisMitTeilleistungen) leistungsnachweis;
	// Map<String, Double> bws = new HashMap<String, Double>();
	// for (AbstrakterLeistungsnachweis teilleistung : lmt
	// .getTeilleistungsnachweise()) {
	// bws.put(teilleistung.getKuerzel(), calculateBewertung(
	// teilnehmer, teilleistung));
	// }
	// return ScriptService.parseFormel(lmt.getFormel(), bws);
	// }
	// return null;
	// }

}
