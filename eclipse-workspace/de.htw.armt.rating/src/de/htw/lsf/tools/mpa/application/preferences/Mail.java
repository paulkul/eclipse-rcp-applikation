package de.htw.lsf.tools.mpa.application.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.htw.lsf.tools.mpa.bewertungen.Activator;
import de.htw.lsf.tools.mpa.bewertungen.Messages;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class Mail extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public Mail() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription(Messages.Mail_0);
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {
		int width = 70;
		int width_wildcards = 10;
		
		addField(new StringFieldEditor(PreferenceConstants.P_STRING_MAIL_CC, Messages.Mail_1, width, getFieldEditorParent()));
		addField(new StringFieldEditor(PreferenceConstants.P_STRING_MAIL_SUBJECT, Messages.Mail_2, width, getFieldEditorParent()));
		addField(new StringFieldEditor(PreferenceConstants.P_STRING_MAIL_MESSAGE, Messages.Mail_3, width, getFieldEditorParent()));
		
		addField(new StringFieldEditor(PreferenceConstants.P_STRING_MAIL_WILDCARD_LN, Messages.Mail_4, width_wildcards, getFieldEditorParent()));
		addField(new StringFieldEditor(PreferenceConstants.P_STRING_MAIL_WILDCARD_LV, Messages.Mail_5, width_wildcards, getFieldEditorParent()));
		addField(new StringFieldEditor(PreferenceConstants.P_STRING_MAIL_WILDCARD_PK, Messages.Mail_6, width_wildcards, getFieldEditorParent()));
		addField(new StringFieldEditor(PreferenceConstants.P_STRING_MAIL_WILDCARD_TN, Messages.Mail_7, width_wildcards, getFieldEditorParent()));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}