package de.htw.lsf.tools.mpa.bewertungen.services;

import java.awt.Desktop;
import java.net.URI;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.jface.preference.IPreferenceStore;

import de.htw.lsf.tools.mpa.application.preferences.PreferenceConstants;
import de.htw.lsf.tools.mpa.bewertungen.Activator;
import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Bewertung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.services.impl.TeilnehmerServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.student.scheme.Student;

public class SendMail {

	private Assessment ln;
	private AbstractAssessment aln;
	private Lehrveranstaltung lv;
	private Student s;
	
	private Logger logger = Logger.getLogger(SendMail.class);
	
	private List <Bewertung> punkte;
	private Vector <Bewertung> vecBewertung;
	private List <Teilnehmer> tn;
	private List <Lehrveranstaltung> allLv;
	
	private LehrveranstaltungService lvService;
	private IPreferenceStore preferenceStore;
	private String 
		mail_address, mail_cc, mail_subject, mail_message, final_message, mail_wildcard_lv, 
		mail_wildcard_ln, mail_wildcard_pk, mail_wildcard_tn, gender;
	
	public SendMail(AbstractAssessment abstractLn){
		this.ln = (Assessment)abstractLn;
		while (abstractLn.getAssessmentGroup() != null) {
			abstractLn = abstractLn.getAssessmentGroup();
		}
		aln = abstractLn;
				
	}
	
	public void createInput(){
		// Get Lehrveranstaltung
		LehrveranstaltungServiceFactory lvFactory = LehrveranstaltungServiceFactory.getInstance(
				LehrveranstaltungServiceFactory.HIBERNATE);
		lvService = lvFactory.getLehrveranstaltungService();
		allLv = lvService.getLehrveranstaltungenFuerLeistungsnachweis(
				(AssessmentGroup) aln);
		if ((allLv != null) && (allLv.size() == 1)) {
			lv = allLv.get(0);
		}
		BewertungService bs = BewertungServiceFactory.getInstance(
				BewertungServiceFactory.HIBERNATE).getBewertungService();
		// Get Teilnehmer
		tn = TeilnehmerServiceHibernate.getInstance().getTeilnehmerFuerLehrveranstaltung(lv);	
		// Get Bewertungen
		vecBewertung = new Vector <Bewertung> ();
		for (Teilnehmer t:tn){
			vecBewertung.add(bs.getTeilnehmerBewertung(t, ln));
		}	
		punkte = vecBewertung;
		
		// Get stored EmailParameter
		preferenceStore = Activator.getDefault().getPreferenceStore();
		mail_cc = preferenceStore.getString(PreferenceConstants.P_STRING_MAIL_CC);
		mail_subject = preferenceStore.getString(PreferenceConstants.P_STRING_MAIL_SUBJECT);
		final_message = preferenceStore.getString(PreferenceConstants.P_STRING_MAIL_MESSAGE);
		mail_wildcard_lv = preferenceStore.getString(PreferenceConstants.P_STRING_MAIL_WILDCARD_LV);
		mail_wildcard_ln = preferenceStore.getString(PreferenceConstants.P_STRING_MAIL_WILDCARD_LN);
		mail_wildcard_pk = preferenceStore.getString(PreferenceConstants.P_STRING_MAIL_WILDCARD_PK);
		mail_wildcard_tn = preferenceStore.getString(PreferenceConstants.P_STRING_MAIL_WILDCARD_TN);
	}

	public void postMail(){
		for (int i = 0; i < punkte.size(); i++){
			
			if ((Bewertung)punkte.get(i)!=null){
				Bewertung bew = punkte.get(i);
				
				s = ((Teilnehmer)bew.getTeilnehmer()).getStudent();
				// point- and emailvalueecheck
				if (bew.getPunkte()!=-1 && s.getEmail().length()>0){
					// if male
					if(s.isMale()){
						gender = Messages.SendMail_0;
					}else{
						gender = Messages.SendMail_1;
					}
					// setup emailvalues for client
					mail_address = this.s.getEmail();
					
					mail_subject = mail_subject.replaceAll(mail_wildcard_lv, lv.getBezeichnung());
					mail_subject = mail_subject.replaceAll(mail_wildcard_ln, ln.getName());
					
					mail_message = final_message;
					mail_message = mail_message.replaceAll(mail_wildcard_lv, lv.getBezeichnung());
					mail_message = mail_message.replaceAll(mail_wildcard_ln, ln.getName());
					mail_message = mail_message.replaceAll(mail_wildcard_pk, String.valueOf(bew.getPunkte())+Messages.SendMail_2);
					mail_message = mail_message.replaceAll(mail_wildcard_tn, gender+" "+this.s.getLastname()); //$NON-NLS-1$
					mail_message = mail_message.replaceAll("\\\\n", "\n"); //$NON-NLS-1$ //$NON-NLS-2$
	
//					try {
//						mail_message = URLEncoder.encode(mail_message, "UTF-8");
//						mail_subject = URLEncoder.encode(mail_subject, "UTF-8");
//					} catch (UnsupportedEncodingException e1) {
//
//						e1.printStackTrace();
//					}
			
					// create uri and open client
					try {	
						URI uriMailTo = new URI(
								"mailTo", this.mail_address +  //$NON-NLS-1$
								"?subject=" + this.mail_subject +  //$NON-NLS-1$
								"&cc=" + this.mail_cc + //$NON-NLS-1$
								"&body=" + this.mail_message, null //$NON-NLS-1$
						);
									          
						Desktop
			                    .getDesktop()
			                    .mail(uriMailTo);
						
			        } catch (Exception e) {
			            logger.log(Level.WARN, Messages.SendMail_10, e);  
			        }
				}
			}
		}
	}
}
