package de.htw.lsf.tools.mpa.bewertungen.structures;

public class ExportDataEntry {
	
	private int row = -1;
	
	private long pordnr = -1;
	private long labnr = -1;
	private long studiengang = -1;
	private String sortname = "";
	private long matrikelnummer = -1;
	private long pversuch = -1;
	private long bewertung = -1;
	private long berechneteBewertung = -1;
	

	public ExportDataEntry(int row, long pordnr, long labnr, long studiengang, String sortname, 
			long matrikelnummer, long pversuch, long bewertung) {
		super();
		this.row = row;
		this.pordnr = pordnr;
		this.labnr = labnr;
		this.studiengang = studiengang;
		this.matrikelnummer = matrikelnummer;
		this.sortname = sortname;
		this.pversuch = pversuch;
		this.bewertung = bewertung;
	}
	
	public int getRow() {
		return row;
	}
	
	public long getPordnr() {
		return pordnr;
	}
	public long getLabnr() {
		return labnr;
	}
	public long getStudiengang() {
		return studiengang;
	}
	public long getMatrikelnummer() {
		return matrikelnummer;
	}
	public String getSortname() {
		return sortname;
	}
	public long getPversuch() {
		return pversuch;
	}
	public long getBewertung() {
		return bewertung;
	}

	public long getBerechneteBewertung() {
		return berechneteBewertung;
	}

	public void setBerechneteBewertung(long berechneteBewertung) {
		this.berechneteBewertung = berechneteBewertung;
	}
}
