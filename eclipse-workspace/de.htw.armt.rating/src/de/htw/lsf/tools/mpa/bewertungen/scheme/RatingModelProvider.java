package de.htw.lsf.tools.mpa.bewertungen.scheme;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.databinding.observable.list.WritableList;

public enum RatingModelProvider {
	INSTANCE;

	private Map<Long, Map<Long, WritableList>> ratings;

	private RatingModelProvider() {
		ratings =  new HashMap<Long, Map<Long, WritableList>>();
	}

	public WritableList getRatings(Long lvId, Long lnId) {
		if (!ratings.containsKey(lvId)) {
			ratings.put(lvId, new HashMap<Long, WritableList>());
		}
		if (!ratings.get(lvId).containsKey(lnId)) {
			ratings.get(lvId).put(lnId, WritableList.withElementType(Bewertung.class));
		}
		return ratings.get(lvId).get(lnId);
	}

}
