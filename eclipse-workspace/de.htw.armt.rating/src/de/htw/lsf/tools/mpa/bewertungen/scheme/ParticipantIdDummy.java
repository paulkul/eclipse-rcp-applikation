package de.htw.lsf.tools.mpa.bewertungen.scheme;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class ParticipantIdDummy implements Serializable {

	/** */
	private static final long serialVersionUID = -3611618446582631595L;

	private Long id;

	@Id
	@Column(name = "id")
	@TableGenerator(name = "Participant_Seq", allocationSize = 40, pkColumnValue = "Participant")
	@GeneratedValue(generator = "Participant_Seq", strategy = GenerationType.TABLE)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
