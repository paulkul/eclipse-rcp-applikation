package de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl;

import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import de.htw.lsf.tools.mpa.base.scheme.dao.impl.AbstractDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Bewertung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.BewertungDAO;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;

public class BewertungDAOHibernate extends AbstractDAOHibernate<Bewertung>
		implements BewertungDAO {

	private static BewertungDAOHibernate instance = null;

	private BewertungDAOHibernate() {
	}

	public static synchronized BewertungDAOHibernate getInstance() {
		if (instance == null) {
			instance = new BewertungDAOHibernate();
		}
		return instance;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Bewertung getBewertung(AbstrakterTeilnehmer participant,
			Assessment assessment) {
		Bewertung bewertung = null;
		Query query = getEntityManager()
				.createQuery(
						"from Bewertung b where b.leistungsnachweis = :assessment "
								+ "and b.teilnehmer = :participant")
				.setParameter("assessment", assessment)
				.setParameter("participant", participant);
		try {
			bewertung = (Bewertung) query.getSingleResult();
		} catch (NoResultException exp) {
			bewertung = null;
		}
		// List l = query.getResultList();
		// if ((l != null) && (l.size() > 0)) {
		// bewertung = (Bewertung) l.get(0);
		// }

		// TODO edited
		// if (bewertung == null) {
		// if (tn instanceof Teilnehmer) {
		// Set<Uebungsgruppe> ug = ((Teilnehmer) tn).getUebungsgruppe();
		// for (Uebungsgruppe uebungsgruppe : ug) {
		// if (uebungsgruppe != null) {
		// bewertung = getBewertung(uebungsgruppe, ln);
		// }
		// }
		// }
		// }
		return bewertung;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Bewertung getTeilnehmerBewertung(Teilnehmer tn, Assessment ln) {
		Bewertung bewertung = null;
		Query query = getEntityManager()
				.createQuery(
						"from bewertung b where b.leistungsnachweis = :ln "
								+ "and b.teilnehmer = :tn").setParameter("ln", ln)
				.setParameter("tn", tn);
		List l = query.getResultList();
		if ((l != null) && (l.size() > 0)) {
			bewertung = (Bewertung) l.get(0);
		}
		return bewertung;
	}

	public List<Bewertung> findByLehrveranstaltungIdAndLeistungsnachweisen(
			Long lehrveranstaltungId, List<Assessment> leistungsnachweise) {
		Query query = getEntityManager()
				.createQuery(
						"from bewertung b "
								+ "where b.teilnehmer.lehrveranstaltung.lehrveranstaltungsID = :lehrveranstaltungId "
								+ "and b.leistungsnachweis IN (:leistungsnachweise)")
				.setParameter("lehrveranstaltungId", lehrveranstaltungId)
				.setParameter("leistungsnachweise", leistungsnachweise);
		List<Bewertung> result = query.getResultList();
		return result;
	}

	@Override
	public Bewertung getBewertung(Set<Uebungsgruppe> workgroups,
			Assessment assessment) {
		if (workgroups.isEmpty()) {
			return null;
		} else {
			Query query = getEntityManager().createQuery(
					"FROM Bewertung b WHERE b.leistungsnachweis = :assessment "
							+ "AND b.teilnehmer IN (:workgroups)"
							+ "AND b.punkte IS NOT NULL");
			query.setParameter("assessment", assessment).setParameter(
					"workgroups", workgroups);
			try {
				Bewertung result = (Bewertung) query.getSingleResult();
				return result;
			} catch (NoResultException exp) {
				return null;
			}
		}
	}

}
