package de.htw.lsf.tools.mpa.bewertungen.scheme.dao;

import java.util.*;

import de.htw.lsf.tools.mpa.base.scheme.dao.*;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.*;

public interface LehrveranstaltungDAO extends AbstractDAO<Lehrveranstaltung> {

	public List<Lehrveranstaltung> getLehrveranstaltungenFuerLeistungsnachweis(
			AssessmentGroup ln);

	public List<Lehrveranstaltung> getLehrveranstaltungenFuerModul(String modulnummer);
}
