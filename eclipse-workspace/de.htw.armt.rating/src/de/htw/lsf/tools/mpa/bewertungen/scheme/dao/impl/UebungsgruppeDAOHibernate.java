package de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.scheme.dao.impl.AbstractDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.TeilnehmerDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.UebungsgruppeDAO;

public class UebungsgruppeDAOHibernate extends
		AbstractDAOHibernate<Uebungsgruppe> implements UebungsgruppeDAO {

	private static UebungsgruppeDAOHibernate instance = null;

	private UebungsgruppeDAOHibernate() {
	}

	public static synchronized UebungsgruppeDAOHibernate getInstance() {
		if (instance == null) {
			instance = new UebungsgruppeDAOHibernate();
		}
		return instance;
	}
	
	public AbstractDAO<AbstrakterTeilnehmer> getDAO(EntityManager entityManager) {
		TeilnehmerDAOHibernate dao = TeilnehmerDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	@Override
	public List<Uebungsgruppe> getWorkingGroupByMatrNr(long matrNr) {
		Set<Uebungsgruppe> temp = new HashSet<Uebungsgruppe>();
		List<Uebungsgruppe> objects = new ArrayList<Uebungsgruppe>();
		List<Teilnehmer> teilnehmer = new ArrayList<Teilnehmer>();
		teilnehmer = ((TeilnehmerDAO) getDAO(getEntityManager())).getTeilnehmerByMatrNr(matrNr);
		

		for (int i = 0; i < teilnehmer.size(); i++) {
			Teilnehmer t = teilnehmer.get(i);
			temp = t.getUebungsgruppe();
			objects.addAll(temp);
		}

		return objects;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Uebungsgruppe> getUebungsgruppeFuerLehrveranstaltung(
			Lehrveranstaltung veranstaltung, boolean activeOnly) {
		List<Uebungsgruppe> objects = new ArrayList<Uebungsgruppe>();
		Query query = getEntityManager().createQuery(
				"from Uebungsgruppe u "
						+ "where u.veranstaltung = :veranstaltung "
						+ ((activeOnly) ? "and u.active = true" : ""));
		query.setParameter("veranstaltung", veranstaltung);
		objects = query.getResultList();
		return objects;
	}

}
