package de.htw.armt.assessment.parts;

import java.util.ArrayList;
import java.util.List;



import javax.annotation.PostConstruct;
import org.eclipse.core.databinding.observable.list.WritableList;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.adapterfactory.LeistungsnachweisAdapterFactory;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;

public class SamplePart1 {

	private TreeViewer treeViewer;
	public WritableList input;

	private IAdapterFactory adapterFactory = new LeistungsnachweisAdapterFactory();

	ModulService ms = ModulServiceHibernate.getInstance();

	/**
	 * 
	 * @PostConstruct public void createComposite(Composite parent) { TreeViewer
	 *                viewer = new TreeViewer(parent); viewer.setContentProvider(new
	 *                TreeContentProvider());
	 *                viewer.getTree().setHeaderVisible(true);
	 *                viewer.getTree().setLinesVisible(true);
	 * 
	 *                TreeViewerColumn viewerColumn = new TreeViewerColumn(viewer,
	 *                SWT.NONE); viewerColumn.getColumn().setWidth(300);
	 *                viewerColumn.getColumn().setText("Names");
	 *                viewerColumn.setLabelProvider(new ColumnLabelProvider());
	 * 
	 *                List<Module> moduls = ms.selectAll(); List<String>
	 *                modulsNames= new ArrayList<>(); for(int
	 *                i=0;i<moduls.size();i++) {
	 *                modulsNames.add(moduls.get(i).getName()); }
	 * 
	 *                viewer.setInput(moduls.toArray(new Module[moduls.size()]));
	 * 
	 *                GridLayoutFactory.fillDefaults().generateLayout(parent);
	 * 
	 *                }
	 */

	@PostConstruct
	public void createComposite(Composite parent) {
		
		 

		treeViewer = new TreeViewer(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		Platform.getAdapterManager().registerAdapters(adapterFactory, AbstractAssessment.class);
		Platform.getAdapterManager().registerAdapters(adapterFactory, Module.class);
		Platform.getAdapterManager().registerAdapters(adapterFactory, Module[].class);

		//treeViewer.setLabelProvider(new WorkbenchLabelProvider());
		treeViewer.setContentProvider(new BaseWorkbenchContentProvider());
		refreshData();
		treeViewer.expandToLevel(2);

	}

	public void refreshData() {
		// save tree path and selection
		TreePath[] paths = treeViewer.getExpandedTreePaths();
		ISelection sel = treeViewer.getSelection();

		List<Module> moduls = ms.selectAll();
		treeViewer.setInput(moduls.toArray(new Module[moduls.size()]));

		// restore tree path and selection
		treeViewer.setExpandedTreePaths(paths);
		treeViewer.setSelection(sel);
	}
	
	

}
