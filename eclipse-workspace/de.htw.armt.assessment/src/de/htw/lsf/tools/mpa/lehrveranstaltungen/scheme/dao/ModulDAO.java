package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao;

import de.htw.lsf.tools.mpa.base.scheme.dao.*;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.*;

public interface ModulDAO extends AbstractDAO<Module> {

}
