package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.adapterfactory;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.model.IWorkbenchAdapter;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

public class LeistungsnachweisAdapterFactory implements IAdapterFactory {

	private Logger logger = Logger
			.getLogger(LeistungsnachweisAdapterFactory.class);

	private IWorkbenchAdapter modulsListAdapter = new IWorkbenchAdapter() {
		public Object getParent(Object o) {
			return null;
		}

		public String getLabel(Object o) {
			return Messages.LeistungsnachweisAdapterFactory_1;
		}

		public ImageDescriptor getImageDescriptor(Object object) {
			return null;
		}

		public Object[] getChildren(Object o) {
			return (Module[]) o;
		}
	};

	private IWorkbenchAdapter modulAdapter = new IWorkbenchAdapter() {
		public Object getParent(Object o) {
			return null;
		}

		public String getLabel(Object o) {
			Module entry = ((Module) o);
			return entry.getName();
		}

		public ImageDescriptor getImageDescriptor(Object object) {
			return null;
		}

		public Object[] getChildren(Object o) {
			List<AssessmentGroup> list = null;
			try {
				list = LeistungsnachweisServiceHibernate.getInstance()
						.getLeistungsnachweiseFuerModul((Module) o);
			} catch (Exception e) {
				logger.log(Level.ERROR,
						Messages.LeistungsnachweisAdapterFactory_0, e);
			}
			if (list != null) {
				return list.toArray(new AssessmentGroup[list
						.size()]);
			}
			return new AssessmentGroup[0];
		}
	};

	private IWorkbenchAdapter leistungsnachweisMitTeilleistungenAdapter = new IWorkbenchAdapter() {
		public Object getParent(Object o) {
			return ((AssessmentGroup) o).getAssessmentGroup();
		}

		public String getLabel(Object o) {
			AssessmentGroup group = ((AssessmentGroup) o);
			return group.getName();
		}

		public ImageDescriptor getImageDescriptor(Object object) {
			return null;
		}

		public Object[] getChildren(Object o) {
			return ((AssessmentGroup) o)
					.getAssessments().toArray();
		}
	};

	private IWorkbenchAdapter leistungsnachweisAdapter = new IWorkbenchAdapter() {
		public Object getParent(Object o) {
			return ((Assessment) o).getAssessmentGroup();
		}

		public String getLabel(Object o) {
			Assessment entry = ((Assessment) o);
			return entry.getName();
		}

		public ImageDescriptor getImageDescriptor(Object object) {
			return null;
		}

		public Object[] getChildren(Object o) {
			return new Object[0];
		}
	};

	@SuppressWarnings("unchecked")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adapterType == IWorkbenchAdapter.class) {
			if (adaptableObject instanceof Module[]) {
				return modulsListAdapter;
			}
			if (adaptableObject instanceof Module) {
				return modulAdapter;
			}
			if (adaptableObject instanceof AssessmentGroup) {
				return leistungsnachweisMitTeilleistungenAdapter;
			}
			if (adaptableObject instanceof Assessment) {
				return leistungsnachweisAdapter;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public Class[] getAdapterList() {
		return new Class[] { IWorkbenchAdapter.class };
	}
}
