/*
 * ModulService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl;

import javax.persistence.EntityManager;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.impl.ModulDAOHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;

/**
 * @author V.Medvid
 */
public class ModulServiceHibernate extends AbstractServiceHibernate<Module>
		implements ModulService {

	private static ModulServiceHibernate instance = null;
	
	public ModulServiceHibernate() {
		super(Module.class);
	}

	@Override
	public AbstractDAO<Module> getDAO(EntityManager entityManager) {
		ModulDAOHibernate dao = ModulDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static ModulServiceHibernate getInstance() {
		if (instance == null) {
			instance = new ModulServiceHibernate();
		}
		return instance;
	}

}
