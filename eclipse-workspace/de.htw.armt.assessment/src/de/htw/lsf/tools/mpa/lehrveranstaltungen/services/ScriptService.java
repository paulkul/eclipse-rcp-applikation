package de.htw.lsf.tools.mpa.lehrveranstaltungen.services;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ScriptService {

	private static final Logger LOGGER = Logger.getLogger(ScriptService.class);

	public static Double parseFormel(String formel, Map<String, Double> bws) {
		if (containsOnlyNull(bws.values())) {
			return null;
		} else {
			double bewertung = 0;
			// DecimalFormat df = new DecimalFormat("0.00");
			if ((formel != null) && (formel.length() > 0)) {
				ScriptEngineManager factory = new ScriptEngineManager();
				ScriptEngine engine = factory.getEngineByName("JavaScript");
				for (Map.Entry<String, Double> entry : bws.entrySet()) {
					engine.put(entry.getKey(), (entry.getValue() == null) ? 0.0
							: entry.getValue());
				}
				try {
					engine.eval("var resMPA = " + formel);
					Object o2 = engine.get("resMPA");
					bewertung = (Double) o2;
				} catch (ScriptException e) {
					LOGGER.log(Level.ERROR, "Fehler beim Formel parsen! ", e);
					throw new RuntimeException(e);
				}
			}
			return bewertung;
		}
	}

//	private static Map<String, Double> replaceNulls(
//			Map<String, Double> ratingParts, Double d) {
//		for (String key : ratingParts.keySet()) {
//			if (ratingParts.get(key) == null) {
//				ratingParts.put(key, d);
//			}
//		}
//		return ratingParts;
//	}

	private static boolean containsOnlyNull(Collection<Double> c) {
		Set<Double> set = new HashSet<Double>(c);
		if ((set.size() == 1) && (set.contains(null))) {
			return true;
		} else {
			return false;
		}
	}

}
