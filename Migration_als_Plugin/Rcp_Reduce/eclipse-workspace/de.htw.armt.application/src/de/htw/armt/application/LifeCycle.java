package de.htw.armt.application;

import de.htw.lsf.tools.mpa.base.log.InitLog4jConfig;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;

/**
 * Class to execute code on specific lifecycle events, based on annotations
 * 
 * @author paulkulschewski
 *
 */
public class LifeCycle {

	@PostContextCreate
	public void postContextCreate() {
		new InitLog4jConfig();
	}

}
