package de.htw.lsf.tools.mpa.base.utils;

import org.eclipse.core.databinding.conversion.IConverter;


public class SimpleConverter {

	@SuppressWarnings("rawtypes")
	public static final IConverter LONG_TO_STRING_CONVERTER = new IConverter() {
	
		@Override
		public Object getFromType() {
			return Long.class;
		}
	
		@Override
		public Object getToType() {
			return String.class;
		}
	
		@Override
		public Object convert(Object fromObject) {
			return String.valueOf((Long) fromObject);
		}
	};
	
	@SuppressWarnings("rawtypes")
	public static final IConverter STRING_TO_LONG_CONVERTER = new IConverter() {
	    
		@Override
	    public Object getFromType() {
	        return String.class;
	    }
	
		@Override
	    public Object getToType() {
	        return Long.class;
	    }
	                
		@Override
	    public Object convert(Object fromObject) {
	        return Long.valueOf((String) fromObject);
	    }
	};

}
