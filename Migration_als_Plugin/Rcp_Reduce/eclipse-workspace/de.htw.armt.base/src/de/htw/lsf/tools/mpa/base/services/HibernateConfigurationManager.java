/*
 * HibernateService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.base.services;

import org.hibernate.cfg.*;

/**
 * @author V.Medvid
 */
public class HibernateConfigurationManager {
	
	private Configuration configuration = null;
	private static HibernateConfigurationManager instance = null;
	
	private HibernateConfigurationManager() {
	}

	public static HibernateConfigurationManager getInstance() {
		if (instance == null) {
			instance = new HibernateConfigurationManager();
		}
		return instance;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
}
