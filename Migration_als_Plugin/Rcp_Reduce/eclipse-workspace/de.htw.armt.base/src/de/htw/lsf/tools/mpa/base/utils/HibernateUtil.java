/*
 * HibernateHelper.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.base.utils;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import de.htw.lsf.tools.mpa.base.services.HibernateConfigurationManager;

/**
 * @author V.Medvid
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class HibernateUtil {

//	private static final SessionFactory sessionFactory = buildSessionFactory();
//    private static final EntityManagerFactory FACTORY = Persistence.createEntityManagerFactory("manager");
    private static EntityManagerFactory FACTORY;
    private static EntityManager ENTITY_MANAGER; // = FACTORY.createEntityManager();
    
    private static final Logger LOGGER = Logger.getLogger(HibernateUtil.class);

	@SuppressWarnings("unused")
	private static SessionFactory buildSessionFactory() {
		try {
			Configuration conf = HibernateConfigurationManager.getInstance()
					.getConfiguration();
			if (conf != null) {
				return conf.buildSessionFactory();
			}
			LOGGER.log(Level.ERROR, "configuration for hibernate is missing");
		} catch (RuntimeException ex) {
			LOGGER.log(Level.ERROR, " ", ex);
			throw ex;
		}
		return null;
	}

	@SuppressWarnings("unused")
	private static EntityManagerFactory getSessionFactory() {
		return FACTORY;
	}

	public static EntityManager getCurrentSession() {
//		return getSessionFactory().openSession();
		return ENTITY_MANAGER;
	}

	public static void releaseCurrentSession(EntityManager session) {
//		if (session != null && session.isOpen()) {
//			session.close();
//		}
	}

	public static void shutdown() {
		EntityManager entityManager = null;
		try {
			entityManager = HibernateUtil.getCurrentSession();
			// removing newlines in csv files
			entityManager.getTransaction().begin();
			entityManager.createNativeQuery("SHUTDOWN COMPACT").executeUpdate();
			entityManager.getTransaction().commit();
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}
	
	static {
		try {			
		FACTORY = Persistence.createEntityManagerFactory("manager");
			Map conf = new HashMap();
			conf.put("hibernate.connection.url", "jdbc:hsqldb:file:data/db;shutdown=true");
//		ENTITY_MANAGER = FACTORY.createEntityManager(conf);
		ENTITY_MANAGER = FACTORY.createEntityManager();
		}
		catch (RuntimeException exp) {
			exp.printStackTrace();
			LOGGER.log(Level.FATAL, exp);
			if (exp.getCause() != null) {
				LOGGER.log(Level.FATAL, exp.getCause());				
			}
			throw exp;
		}
	}
 
	

}
