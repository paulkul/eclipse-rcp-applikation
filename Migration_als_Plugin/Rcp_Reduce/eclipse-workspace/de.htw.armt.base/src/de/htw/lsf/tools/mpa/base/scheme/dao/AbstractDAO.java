package de.htw.lsf.tools.mpa.base.scheme.dao;

import java.io.*;
import java.util.*;

import de.htw.lsf.tools.mpa.base.scheme.*;

public interface AbstractDAO<T extends ModelObject> {

	public T insert(T object);
	public void delete(T object);
	public T find(Serializable id, Class<T> clazz);
	public T update(T object);
	public T saveOrUpdate(T object);
	public List<T> selectAll(Class<T> clazz);
	public boolean exists(Serializable id, Class<T> clazz);
}
