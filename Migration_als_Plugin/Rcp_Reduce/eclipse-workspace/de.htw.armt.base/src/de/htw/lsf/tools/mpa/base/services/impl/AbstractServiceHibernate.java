package de.htw.lsf.tools.mpa.base.services.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;
import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.Service;
import de.htw.lsf.tools.mpa.base.utils.HibernateUtil;

/**
 * @author Volodymyr Medvid
 */
abstract public class AbstractServiceHibernate<T extends ModelObject> implements Service<T> {

	private static Logger LOGGER = Logger.getLogger(AbstractServiceHibernate.class);

	private Class<T> clazz;
//	private EntityManager entityManager;

	public AbstractServiceHibernate(Class<T> clazz) {
		if (clazz == null) {
			throw new IllegalArgumentException("clazz can not be null");
		}
		this.clazz = clazz;
//		this.entityManager = HibernateUtil.getCurrentSession();
//		this.entityManager.setFlushMode(FlushModeType.COMMIT);
	}

	abstract public AbstractDAO<T> getDAO(EntityManager entityManager);

	public T insert(T object) {
		EntityManager session = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			object = getDAO(session).insert(object);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			LOGGER.log(Level.ERROR, "INSERT Error", e);
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return object;
	}

	public void delete(T object) {
		EntityManager session = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			getDAO(session).delete(object);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			LOGGER.log(Level.ERROR, "DELETE Error", e);
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
	}

	public T find(Serializable id) {
		EntityManager session = null;
		T object = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			object = getDAO(session).find(id, this.clazz);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			LOGGER.log(Level.ERROR, "T FIND Error", e);
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return object;
	}

	public T update(T object) {
		EntityManager session = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			object = getDAO(session).update(object);
			session.getTransaction().commit();
			//session.flush();
		} catch (PersistenceException e) {
			e.printStackTrace();
			LOGGER.log(Level.ERROR, "UPDATE Error", e);
			if (session != null && session.isOpen()) {
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return object;
	}

	public List<T> selectAll() {
		EntityManager session = null;
		List<T> objects = new ArrayList<T>();
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			objects = getDAO(session).selectAll(this.clazz);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			LOGGER.log(Level.ERROR, "SELECT ALL Teilnehmer Error", e);
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return objects;
	}

	public boolean exists(Serializable id) {
		boolean exists = false;
		EntityManager session = null;
		try {
			session = HibernateUtil.getCurrentSession();
			session.getTransaction().begin();
			exists = getDAO(session).exists(id, this.clazz);
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			LOGGER.log(Level.ERROR, "BOOLEAN EXISTS Error", e);
			if (session != null && session.isOpen()) {
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(session);
		}
		return exists;
	}
}
