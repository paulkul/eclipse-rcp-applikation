package de.htw.lsf.tools.mpa.base.log;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class InitLog4jConfig {

	private Logger logger = Logger.getRootLogger();
	
	public InitLog4jConfig(){
		
		try{
			DOMConfigurator.configure("log4j_Config.xml");
		} catch (Exception e) {
			BasicConfigurator.configure();
		    logger.log(Level.WARN, "Logging started (using BasicConfigurator)", e);
		}
	}
}
