package de.htw.lsf.tools.mpa.base.scheme;

import java.io.Serializable;

public abstract class ModelObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1306953971303250478L;

	abstract public void setNumber(Long number);

	abstract public Long getNumber();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 3;
		result = prime * result + ((getNumber() == null) ? 0 : getNumber().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		ModelObject other = (ModelObject) obj;
		if (!getNumber().equals(other.getNumber()))
			return false;
		return true;
	}

}
