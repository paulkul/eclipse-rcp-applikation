/*
 * BewertungServiceFactoryHibernate.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services.impl;

import de.htw.lsf.tools.mpa.bewertungen.services.*;

/**
 * @author V.Medvid
 */
public class BewertungServiceFactoryHibernate extends BewertungServiceFactory {

	@Override
	public BewertungService getBewertungService() {
		return BewertungServiceHibernate.getInstance();
	}

	@Override
	public TeilnehmerService getTeilnehmerService() {
		return TeilnehmerServiceHibernate.getInstance();
	}

}
