package de.htw.lsf.tools.mpa.bewertungen.scheme.dao;

import java.util.List;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;

public interface UebungsgruppeDAO extends AbstractDAO<Uebungsgruppe> {

	public List<Uebungsgruppe> getWorkingGroupByMatrNr(long matrNr);

	public List<Uebungsgruppe> getUebungsgruppeFuerLehrveranstaltung(
			Lehrveranstaltung lehrveranstaltung, boolean activeOnly);

}
