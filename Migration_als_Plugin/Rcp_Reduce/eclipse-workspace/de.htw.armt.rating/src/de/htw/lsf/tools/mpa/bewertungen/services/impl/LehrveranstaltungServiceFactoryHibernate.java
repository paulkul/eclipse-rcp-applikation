/*
 * LehrveranstaltungServiceFactory.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services.impl;

import javax.inject.Inject;

import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungService;
import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungServiceFactory;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.*;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.SemesterServiceHibernate;

/**
 * @author V.Medvid
 */
public class LehrveranstaltungServiceFactoryHibernate extends LehrveranstaltungServiceFactory {

	@Inject
	LeistungsnachweisServiceHibernate ls;

	@Inject
	ModulServiceHibernate ms;

	@Override
	public LehrveranstaltungService getLehrveranstaltungService() {
		return LehrveranstaltungServiceHibernate.getInstance();
	}

	@Override
	public LeistungsnachweisService getLeistungsnachweisService() {

		return ls;
	}

	@Override
	public ModulService getModulService() {
		return ms;
	}

	@Override
	public SemesterService getSemesterService() {
		return SemesterServiceHibernate.getInstance();
	}
}
