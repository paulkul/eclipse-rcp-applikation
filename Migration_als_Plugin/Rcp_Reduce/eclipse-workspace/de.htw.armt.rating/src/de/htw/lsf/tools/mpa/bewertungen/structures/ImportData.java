package de.htw.lsf.tools.mpa.bewertungen.structures;

import java.util.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


public class ImportData {
	
	private String semester;
	private List<ImportDataEntry> entries;
	
	private Logger logger = Logger.getLogger(ImportData.class);
	
	public ImportData(String semester, List<ImportDataEntry> entries) {
		this.semester = semester;
		this.entries = entries;
	}

	public ImportData(String semester, String[] headers, List<String[]> values) {
		this.semester = semester;
		this.entries = new ArrayList<ImportDataEntry>(); 
		for (String[] strings : values) {
			String veranstaltung = "";
			String veranstaltungsnummer = "";
			long verID = -1;
			long matrikelnummer = -1;
			String nachname = "";
			String vorname = "";
			String namenszusatz = "";
			String titel = "";
			boolean maenlich = true;
			String email = "";
			for (int i = 0; i < headers.length; i++) {
				if ((headers[i] != null) && (headers[i].length() > 0)) {
					if (headers[i].equalsIgnoreCase("Veranstaltung")) {
						veranstaltung = strings[i];
					} else if (headers[i].equalsIgnoreCase("Veranstaltungsnummer")) {
						veranstaltungsnummer = strings[i];
					} else if (headers[i].equalsIgnoreCase("VerID")) {
						try {
							verID = Long.parseLong(strings[i]);
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("Matrikelnummer")) {
						try {
							matrikelnummer = Long.parseLong(strings[i]);
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("Nachname")) {
						nachname = strings[i];
					} else if (headers[i].equalsIgnoreCase("Vorname")) {
						vorname = strings[i];
					} else if (headers[i].equalsIgnoreCase("Namenszusatz")) {
						namenszusatz = strings[i];
					} else if (headers[i].equalsIgnoreCase("Titel")) {
						titel = strings[i];
					} else if (headers[i].equalsIgnoreCase("Geschlecht")) {
						if (strings[i] != null)
						try {
							maenlich = strings[i].substring(0, 1).equalsIgnoreCase("M");
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("E-Mail")) {
						email = strings[i];
					}
				}
			}
			this.entries.add(new ImportDataEntry(veranstaltung, veranstaltungsnummer, verID, 
					matrikelnummer, nachname, vorname, namenszusatz, titel, maenlich, email));
		}
	}

	public String getSemester() {
		return semester;
	}

	public List<ImportDataEntry> getEntries() {
		return entries;
	}

}
