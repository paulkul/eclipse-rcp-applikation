package de.htw.lsf.tools.mpa.bewertungen.gui.wizards;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;

import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.services.POIService;
import de.htw.lsf.tools.mpa.bewertungen.structures.ImportData;

public class ImportWizard extends Wizard {

	private FirstImportWizardPage page1;
	private SecondImportWizardPage page2;
	private Logger logger = Logger.getLogger(ImportWizard.class);

	@Execute
	public void execute() {
		setWindowTitle(Messages.ImportWizard_0);
		WizardDialog wd = new WizardDialog(null, this);
		wd.open();
	}

	@Override
	public void addPages() {
		page1 = new FirstImportWizardPage();
		page2 = new SecondImportWizardPage();
		page2.setFirstPage(page1);
		addPage(page1);
		addPage(page2);
	}

	@Override
	public boolean performFinish() {
		boolean ok = false;
		ImportData data = page1.getImportData();
		try {
			POIService.storeData(data);
			ok = true;
		} catch (Exception e) {
			page2.setErrorMessage(Messages.ImportWizard_1);
			logger.log(Level.WARN, "", e); //$NON-NLS-1$
		}
		return ok;
	}
}
