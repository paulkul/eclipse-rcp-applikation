/*
 * LehrveranstaltungService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services;

import java.util.*;

import de.htw.lsf.tools.mpa.base.services.*;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.*;

/**
 * @author V.Medvid
 */
public interface LehrveranstaltungService extends Service<Lehrveranstaltung> {

	public List<Lehrveranstaltung> getLehrveranstaltungenFuerLeistungsnachweis(
			AssessmentGroup ln);
	public List<Lehrveranstaltung> getLehrveranstaltungenFuerModul(String modulnummer);
}
