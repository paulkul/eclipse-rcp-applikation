/*
 * TeilnehmerService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;

import org.hibernate.HibernateException;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.base.utils.HibernateUtil;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.TeilnehmerDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl.TeilnehmerDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.services.BewertungService;
import de.htw.lsf.tools.mpa.bewertungen.services.BewertungServiceFactory;
import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungServiceFactory;
import de.htw.lsf.tools.mpa.bewertungen.services.TeilnehmerService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;

/**
 * @author V.Medvid
 */
public class TeilnehmerServiceHibernate extends AbstractServiceHibernate<AbstrakterTeilnehmer>
		implements TeilnehmerService {

	private static TeilnehmerServiceHibernate instance = null;

	public TeilnehmerServiceHibernate() {
		super(AbstrakterTeilnehmer.class);
	}

	@Override
	public AbstractDAO<AbstrakterTeilnehmer> getDAO(EntityManager entityManager) {
		TeilnehmerDAOHibernate dao = TeilnehmerDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static TeilnehmerServiceHibernate getInstance() {
		if (instance == null) {
			instance = new TeilnehmerServiceHibernate();
		}
		return instance;
	}

	@Override
	public List<Teilnehmer> getTeilnehmerByMatrNr(Long matrNr) throws HibernateException {
		EntityManager entityManager = null;
		List<Teilnehmer> objects = new ArrayList<Teilnehmer>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((TeilnehmerDAO) getDAO(entityManager)).getTeilnehmerByMatrNr(matrNr);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}

	@Override
	public boolean exists(Teilnehmer t) throws HibernateException {
		EntityManager entityManager = null;
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			boolean exists = ((TeilnehmerDAO) getDAO(entityManager)).exists(t);
			entityManager.getTransaction().commit();
			return exists;
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
	}

	@Override
	public boolean exists(Uebungsgruppe ug) throws HibernateException {
		EntityManager entityManager = null;
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			boolean exists = ((TeilnehmerDAO) getDAO(entityManager)).exists(ug);
			entityManager.getTransaction().commit();
			return exists;
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
	}

	@Override
	public List<Teilnehmer> getTeilnehmerFuerLehrveranstaltung(Lehrveranstaltung lehrveranstaltung)
			throws HibernateException {
		EntityManager entityManager = null;
		List<Teilnehmer> objects = new ArrayList<Teilnehmer>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((TeilnehmerDAO) getDAO(entityManager)).getTeilnehmerFuerLehrveranstaltung(lehrveranstaltung);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}

	private static Properties GRADE_PROPERTIES = foo(
			TeilnehmerServiceHibernate.class.getClassLoader().getResourceAsStream("grading_scheme.properties"));

	private static final BigDecimal ONE_POINT_ZERO_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("OnePointZeroLimit"));
	private static final BigDecimal ONE_POINT_THREE_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("OnePointThreeLimit"));
	private static final BigDecimal ONE_POINT_SEVEN_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("OnePointSevenLimit"));
	private static final BigDecimal TWO_POINT_ZERO_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("TwoPointZeroLimit"));
	private static final BigDecimal TWO_POINT_THREE_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("TwoPointThreeLimit"));
	private static final BigDecimal TWO_POINT_SEVEN_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("TwoPointSevenLimit"));
	private static final BigDecimal THREE_POINT_ZERO_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("ThreePointZeroLimit"));
	private static final BigDecimal THREE_POINT_THREE_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("ThreePointThreeLimit"));
	private static final BigDecimal THREE_POINT_SEVEN_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("ThreePointSevenLimit"));
	private static final BigDecimal FOUR_POINT_ZERO_LIMIT = new BigDecimal(
			(String) GRADE_PROPERTIES.get("FourPointZeroLimit"));

	private static Properties foo(InputStream stream) {
		Properties props = new Properties();
		try {
			props.load(stream);
		} catch (IOException exp) {
			// TODO Auto-generated catch block
			exp.printStackTrace();
			throw new RuntimeException(exp);
		}
		return props;
	}

	@Override
	public BigDecimal konvertiereZuEndnote(BigDecimal punkte) {
		BigDecimal endnote = null;
		if (greaterThan(punkte, ONE_POINT_ZERO_LIMIT)) {
			endnote = new BigDecimal("1.0");
		} else if (greaterThan(punkte, ONE_POINT_THREE_LIMIT)) {
			endnote = new BigDecimal("1.3");
		} else if (greaterThan(punkte, ONE_POINT_SEVEN_LIMIT)) {
			endnote = new BigDecimal("1.7");
		} else if (greaterThan(punkte, TWO_POINT_ZERO_LIMIT)) {
			endnote = new BigDecimal("2.0");
		} else if (greaterThan(punkte, TWO_POINT_THREE_LIMIT)) {
			endnote = new BigDecimal("2.3");
		} else if (greaterThan(punkte, TWO_POINT_SEVEN_LIMIT)) {
			endnote = new BigDecimal("2.7");
		} else if (greaterThan(punkte, THREE_POINT_ZERO_LIMIT)) {
			endnote = new BigDecimal("3.0");
		} else if (greaterThan(punkte, THREE_POINT_THREE_LIMIT)) {
			endnote = new BigDecimal("3.3");
		} else if (greaterThan(punkte, THREE_POINT_SEVEN_LIMIT)) {
			endnote = new BigDecimal("3.7");
		} else if (greaterThan(punkte, FOUR_POINT_ZERO_LIMIT)) {
			endnote = new BigDecimal("4.0");
		} else {
			endnote = new BigDecimal("5.0");
		}
		return endnote;
	}

	public static boolean greaterThan(BigDecimal x, BigDecimal y) {
		return x.compareTo(y) >= 0;
	}

	@Override
	public boolean berechneEndnoten(Lehrveranstaltung lehrveranstaltung) {
		boolean alleBerechnet = true;
		EntityManager entityManager = null;
		List<Teilnehmer> participants = new ArrayList<Teilnehmer>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			TeilnehmerDAO dao = ((TeilnehmerDAO) getDAO(entityManager));
			participants = dao.getTeilnehmerFuerLehrveranstaltung(lehrveranstaltung);
			AssessmentGroup assessment = lehrveranstaltung.getLeistungsnachweisGruppe();
			BewertungService bs = BewertungServiceFactory.getInstance(BewertungServiceFactory.HIBERNATE)
					.getBewertungService();

			Map<Teilnehmer, Map<AbstractAssessment, Double>> ratings = bs.determineBewertungen(participants,
					assessment);

			for (Teilnehmer teilnehmer : participants) {
				// double bew = bs.getBewertung(teilnehmer, lng, session);
				Double bew = ratings.get(teilnehmer).get(assessment);

				LeistungsnachweisService lns = LehrveranstaltungServiceFactory
						.getInstance(LehrveranstaltungServiceFactory.HIBERNATE).getLeistungsnachweisService();
				double max = lns.getMaxPunkte(assessment);
				// double endnote = this.konvertiereZuEndnote(bew * 100 / max);
				Double endnote = null;
				if (bew != null) {
					endnote = this.konvertiereZuEndnote(BigDecimal.valueOf(bew / max)).multiply(BigDecimal.valueOf(100))
							.doubleValue();
				}
				teilnehmer.setEndnote(endnote);
				// dao.update(teilnehmer);
			}

			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return alleBerechnet;
	}

	@Override
	public Teilnehmer getTeilnehmer(Lehrveranstaltung lehrveranstaltung, long matrikelNummer) {
		EntityManager entityManager = null;
		Teilnehmer object = null;
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			object = ((TeilnehmerDAO) getDAO(entityManager)).getTeilnehmer(lehrveranstaltung, matrikelNummer);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return object;
	}

	@Override
	public List<Teilnehmer> getTeilnehmerFuerUebungsgruppe(Uebungsgruppe gruppe) {
		EntityManager entityManager = null;
		List<Teilnehmer> objects = new ArrayList<Teilnehmer>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((TeilnehmerDAO) getDAO(entityManager)).getTeilnehmerFuerUebungsgruppe(gruppe);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}
}
