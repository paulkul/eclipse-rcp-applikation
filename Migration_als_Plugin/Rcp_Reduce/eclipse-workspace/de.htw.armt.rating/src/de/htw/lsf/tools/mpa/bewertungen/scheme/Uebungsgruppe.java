package de.htw.lsf.tools.mpa.bewertungen.scheme;

import java.util.*;

import javax.persistence.*;

@Entity
@DiscriminatorValue("UG")
public class Uebungsgruppe extends AbstrakterTeilnehmer {
	// TODO rename in WorkGroup

	private static final long serialVersionUID = -7246784253279244645L;

	/**
	 * 
	 */
	private String bezeichnung = "";
	private boolean active = true;
	private Set<Teilnehmer> teilnehmer = new HashSet<Teilnehmer>();

	public Uebungsgruppe() {
		super();
	}

	public Uebungsgruppe(Lehrveranstaltung lehrveranstaltung, String bezeichnung) {
		super(lehrveranstaltung);
		this.bezeichnung = bezeichnung;
		this.active = true;
	}

	@Column(name = "bezeichnung")
	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	@Column(name = "IsActive")
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean isActive) {
		this.active = isActive;
	}

	public void addTeilnehmer(Teilnehmer teilnehmer) {
		// TODO correct reference from teilnehmer
		this.teilnehmer.add(teilnehmer);
		teilnehmer.addUebungsgruppe(this);
	}

	public void removeTeilnehmer(Teilnehmer teilnehmer) {
		// TODO correct reference from teilnehmer
		this.teilnehmer.remove(teilnehmer);
		teilnehmer.removeUebungsgruppe(this);
	}

	@ManyToMany(mappedBy = "uebungsgruppe", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Teilnehmer> getTeilnehmer() {
		return this.teilnehmer;
	}

	/**
	 * dont use
	 */
	public void setTeilnehmer(Set<Teilnehmer> teilnehmer) {
		this.teilnehmer = teilnehmer;
	}

	@Transient
	public String getTeilnehmerLabel() {
		String result = "";
		boolean isFirst = true;
		for (Teilnehmer teilnehmer : this.getTeilnehmer()) {
			if (isFirst) {
				result = teilnehmer.getStudent().getLastname();
				isFirst = false;
			} else {
				result = result + ", " + teilnehmer.getStudent().getLastname();
			}
		}
		return result;
	}
}
