package de.htw.lsf.tools.mpa.bewertungen;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	
	private static final String BUNDLE_NAME = "de.htw.lsf.tools.mpa.bewertungen.messages"; //$NON-NLS-1$

	private Messages() {
	}

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
	
	public static String AddGruppeDialog_0;
	public static String AddGruppeDialog_1;
	public static String AddGruppeDialog_2;
	public static String AddGruppeDialog_3;
	public static String AddGruppeDialog_4;
	
	public static String BewertungEditingSupport_2;
	public static String BewertungEditingSupport_4;
	public static String BewertungEditingSupport_5;
	
	public static String BewertungEditor_0;
	public static String GroupAssessmentLabel;
	public static String IndividualAssessmentLabel;
	public static String BewertungEditor_2;
	public static String BewertungEditor_3;
	
	public static String BewertungGruppeEditor_0;
	public static String BewertungGruppeEditor_1;
	public static String BewertungGruppeEditor_2;
	
	public static String BewertungGruppeLabelProvider_6;
	public static String BewertungGruppeLabelProvider_7;
	public static String BewertungGruppeLabelProvider_8;
	public static String BewertungGruppeLabelProvider_9;
	
	public static String SurnameLabel;
	public static String ForenameLabel;
	public static String MatricNumberLabel;
	public static String WorkingGroupLabel;
	public static String MaxLabel;
	public static String GradeLabel;
	
	public static String CallBewertungEditor_0;
	public static String CallBewertungEditor_1;
	public static String CallBewertungEditor_2;
	
	public static String DeleteGruppe_0;
	public static String DeleteGruppe_1;
	public static String DeleteGruppe_2;
	
	public static String EditGruppeDialog_0;
	public static String EditGruppeDialog_1;
	public static String EditGruppeDialog_2;
	public static String EditGruppeDialog_3;
	public static String EditGruppeDialog_4;
	public static String EditGruppeDialog_5;
	
	public static String EditLehrveranstaltungDialog_0;
	public static String EditLehrveranstaltungDialog_1;
	public static String EditLehrveranstaltungDialog_2;
	public static String EditLehrveranstaltungDialog_3;
	public static String EditLehrveranstaltungDialog_4;
	public static String EditLehrveranstaltungDialog_5;
	public static String EditLehrveranstaltungDialog_6;
	public static String EditLehrveranstaltungDialog_7;
	public static String EditLehrveranstaltungDialog_8;
	public static String EditLehrveranstaltungDialog_9;
	
	public static String GruppenauswahlLabelProvider_2;
	
	public static String SelectGruppenToScoreDialog_0;
	public static String SelectGruppenToScoreDialog_1;
	public static String SelectGruppenToScoreDialog_2;
	public static String SelectGruppenToScoreDialog_3;
	public static String SelectGruppenToScoreDialog_4;
	public static String SelectGruppenToScoreDialog_5;
	
	public static String StudentBewertungenView_0;
	public static String StudentBewertungenView_1;
	public static String StudentBewertungenView_11;
	public static String StudentBewertungenView_12;
	public static String StudentBewertungenView_15;
	public static String StudentBewertungenView_19;
	public static String StudentBewertungenView_23;
	
	public static String StudentGruppenView_1;
	public static String StudentGruppenView_2;
	public static String StudentGruppenView_3;
	public static String StudentGruppenView_4;
	public static String StudentGruppenView_5;
	
	public static String StudentLehrveranstaltungView_1;
	public static String StudentLehrveranstaltungView_2;
	public static String StudentLehrveranstaltungView_3;
	public static String StudentLehrveranstaltungView_4;
	public static String StudentLehrveranstaltungView_5;
	public static String StudentLehrveranstaltungView_6;
	
	public static String TeilnehmerView_0;
	public static String TeilnehmerView_1;
	public static String TeilnehmerView_2;
	public static String TeilnehmerView_3;
	public static String TeilnehmerView_4;
	public static String TeilnehmerView_5;
	public static String TeilnehmerView_6;
	public static String TeilnehmerView_7;
	public static String TeilnehmerView_8;
	public static String TeilnehmerView_9;
	
	public static String LehrveranstaltungView_0;
	public static String LehrveranstaltungView_1;
	public static String LehrveranstaltungView_10;
	public static String LehrveranstaltungView_11;
	public static String LehrveranstaltungView_12;
	public static String LehrveranstaltungView_13;
	public static String LehrveranstaltungView_2;
	public static String LehrveranstaltungView_3;
	public static String LehrveranstaltungView_4;
	public static String LehrveranstaltungView_5;
	public static String LehrveranstaltungView_6;
	public static String LehrveranstaltungView_7;
	public static String LehrveranstaltungView_8;
	public static String LehrveranstaltungView_9;

	public static String SetLeistungsnachweisAction_2;
	public static String SetLeistungsnachweisAction_3;
	public static String SetLeistungsnachweisAction_4;
	public static String SetLeistungsnachweisAction_5;
	public static String SetLeistungsnachweisAction_8;

	public static String ExportWizard_0;
	
	public static String FirstExportWizardPage_0;
	public static String FirstExportWizardPage_1;
	public static String FirstExportWizardPage_10;
	public static String FirstExportWizardPage_11;
	public static String FirstExportWizardPage_12;
	public static String FirstExportWizardPage_13;
	public static String FirstExportWizardPage_2;
	public static String FirstExportWizardPage_3;
	public static String FirstExportWizardPage_4;
	public static String FirstExportWizardPage_5;
	public static String FirstImportWizardPage_0;
	public static String FirstImportWizardPage_1;
	public static String FirstImportWizardPage_10;
	public static String FirstImportWizardPage_11;
	public static String FirstImportWizardPage_2;
	public static String FirstImportWizardPage_3;
	public static String FirstImportWizardPage_4;
	public static String FirstImportWizardPage_5;
	
	public static String ImportWizard_0;
	public static String ImportWizard_1;
	
	public static String Mail_0;
	public static String Mail_1;
	public static String Mail_2;
	public static String Mail_3;
	public static String Mail_4;
	public static String Mail_5;
	public static String Mail_6;
	public static String Mail_7;
	
	public static String POIService_0;
	public static String POIService_1;
	public static String POIService_2;
	public static String POIService_3;
	
	public static String PreferenceInitializer_1;
	public static String PreferenceInitializer_2;
	
	public static String SecondExportWizardPage_0;
	public static String SecondExportWizardPage_1;
	public static String SecondExportWizardPage_2;
	public static String SecondExportWizardPage_3;
	public static String SecondExportWizardPage_4;
	public static String SecondExportWizardPage_5;
	public static String SecondExportWizardPage_6;
	public static String SecondImportWizardPage_0;
	public static String SecondImportWizardPage_1;
	public static String SecondImportWizardPage_2;
	public static String SecondImportWizardPage_3;
	public static String SecondImportWizardPage_5;
	public static String SecondImportWizardPage_6;
	public static String SecondImportWizardPage_7;
	public static String SecondImportWizardPage_8;
	public static String SecondImportWizardPage_9;
	
	public static String SendMail_0;
	public static String SendMail_1;
	public static String SendMail_10;
	public static String SendMail_2;
	
	public static String ThirdExportWizardPage_0;
	public static String ThirdExportWizardPage_1;
	public static String ThirdExportWizardPage_2;
	public static String ThirdExportWizardPage_3;
	public static String ThirdExportWizardPage_4;
	public static String ThirdExportWizardPage_5;
	public static String ThirdExportWizardPage_6;
	
	public static String WizardAction_0;
	public static String WizardAction_1;

}
