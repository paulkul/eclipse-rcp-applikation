/*
 * BewertungServiceFactory.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services;

import de.htw.lsf.tools.mpa.bewertungen.services.impl.*;

/**
 * @author V.Medvid
 */
public abstract class BewertungServiceFactory {

	public static final Class<? extends BewertungServiceFactory> HIBERNATE =
			BewertungServiceFactoryHibernate.class;
	
	public static BewertungServiceFactory getInstance(
			Class<? extends BewertungServiceFactory> factory) {
        try {
            return (BewertungServiceFactory) factory.newInstance();
        } catch (Exception ex) {
            throw new RuntimeException("ServiceFactory kann nicht erzeugt werden: " + factory);
        }
    }

	abstract public BewertungService getBewertungService();
	abstract public TeilnehmerService getTeilnehmerService();
}
