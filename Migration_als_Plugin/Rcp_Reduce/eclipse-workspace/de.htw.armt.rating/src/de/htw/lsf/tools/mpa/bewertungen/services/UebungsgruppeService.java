/*
 * TeilnehmerService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services;

import java.util.List;

import de.htw.lsf.tools.mpa.base.services.Service;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;

/**
 * @author V.Medvid
 */
public interface UebungsgruppeService extends Service<Uebungsgruppe> {

	public List<Uebungsgruppe> getWorkingGroupsByMatrNr(long matrNr);

	public List<Uebungsgruppe> getUebungsgruppenFuerLehrveranstaltung(
			Lehrveranstaltung lehrveranstaltung, boolean activeOnly);

}
