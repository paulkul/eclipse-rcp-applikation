package de.htw.lsf.tools.mpa.bewertungen.scheme;

import java.io.Serializable;

public class TeilnehmerId implements Serializable {
	
	/** */
	private static final long serialVersionUID = 1L;
	
	Long number;
	long veranstaltung;
	
	public TeilnehmerId() {
		super();
	}
	
	public TeilnehmerId(Long number, long veranstaltung) {
		super();
		this.number = number;
		this.veranstaltung = veranstaltung;
	}
	
	public Long getNumber() {
		return number;
	}
	public void setNumber(Long number) {
		this.number = number;
	}
	public long getVeranstaltung() {
		return veranstaltung;
	}
	public void setVeranstaltung(long veranstaltung) {
		this.veranstaltung = veranstaltung;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 3;
		result = prime
				* result
				+ new Long(getVeranstaltung()).hashCode();
		result = prime * result
				+ ((getNumber() == null) ? 0 : getNumber().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		TeilnehmerId other = (TeilnehmerId) obj;
		if (getVeranstaltung() != other.getVeranstaltung())
			return false;
		if (getNumber() == null) {
			if (other.getNumber() != null)
				return false;
		} else if (!getNumber().equals(other.getNumber()))
			return false;
		return true;
	}

}
