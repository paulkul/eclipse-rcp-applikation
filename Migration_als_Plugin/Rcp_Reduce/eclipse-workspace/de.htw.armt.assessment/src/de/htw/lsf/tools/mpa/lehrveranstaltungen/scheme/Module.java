package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;

@Entity
@Table(name = "MODUL")
public class Module extends ModelObject {

	private static final long serialVersionUID = 3395526822167386410L;
	public static final String NUMBER_FIELD = "number";
	public static final String NAME_FIELD = "name";

	private Long number;
	private String name;

	public Module() {
		super();
	}

	public Module(String name, Long number) {
		super();
		this.name = name;
		this.number = number;
	}

	@Id
	@Column(name = "nummer")
	@Override
	public Long getNumber() {
		return number;
	}

	@Override
	public void setNumber(Long number) {
		this.number = number;
	}

	@Column(name = "bezeichnung")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		return obj == null ? false : (obj instanceof Module) ? this.getNumber()
				.equals(((Module) obj).getNumber()) : false;
	}
	
}
