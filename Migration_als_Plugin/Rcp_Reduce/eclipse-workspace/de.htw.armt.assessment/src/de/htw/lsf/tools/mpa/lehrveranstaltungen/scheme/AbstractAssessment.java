package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;

@Entity
@Table(name = "LEISTUNGSNACHWEIS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DISC", discriminatorType = DiscriminatorType.STRING, length = 3)
@DiscriminatorValue("AL")
public abstract class AbstractAssessment extends ModelObject {

	private static final long serialVersionUID = -8901285500377880512L;
	public static final String ABBREVIATION_FIELD = "abbreviation";
	public static final String NAME_FIELD = "name";
	
	private Long moidI;
	private String name;
	private String abbreviation;
	private AssessmentGroup assessmentGroup;

	public AbstractAssessment() {
		super();
	}
	
	public AbstractAssessment(String name, String abbreviation) {
		super();
		this.name = name;
		this.abbreviation = abbreviation;
	}

	@Id
	@Column(name = "id")
	@TableGenerator(name = "Assessment_Seq", allocationSize = 5)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "Assessment_Seq")
	@Override
	public Long getNumber() {
		return moidI;
	}

	@Override
	public void setNumber(Long number) {
		this.moidI = number;
	}

	@Column(name = "bezeichnung")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "kuerzel")
	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE,
			CascadeType.PERSIST }, optional = true)
	@JoinColumn(name = "parent")
	public AssessmentGroup getAssessmentGroup() {
		return assessmentGroup;
	}

	public void setAssessmentGroup(AssessmentGroup assessmentGroup) {
		this.assessmentGroup = assessmentGroup;
	}

	@Transient
	public abstract boolean isMandatory();

	@Transient
	public String getLongName() {
		return this.getName() + " (" + this.getAbbreviation() + ")";
	}

}
