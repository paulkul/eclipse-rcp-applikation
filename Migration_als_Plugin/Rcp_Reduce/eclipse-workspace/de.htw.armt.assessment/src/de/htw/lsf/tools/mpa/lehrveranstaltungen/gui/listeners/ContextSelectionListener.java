package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.listeners;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.MenuItem;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.handlers.CallLeistungsnachweiseditor;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;

/**
 * ContextSelectionListener is a Class to act as a listener 
 * It fulfils the task of managing the selection inside the context menu
 * 
 * @author paulkulschewski
 *
 */
@SuppressWarnings("restriction")
public class ContextSelectionListener extends SelectionAdapter {

	@Inject
	MApplication application;

	@Inject
	ECommandService commandService;

	@Inject
	EHandlerService handlerService;

	@Override
	public void widgetSelected(SelectionEvent event) {
		Object object = application.getContext().get(Object.class);

		if (((MenuItem) event.getSource()) != null) {
			if (((MenuItem) event.getSource()).getText().equals(Messages.CreateLeistungsnachweisAction_1)) {
				if (object.getClass().equals(AssessmentGroup.class)) {
					IEclipseContext child = application.getContext().createChild();
					child.set(Object.class, object);

					handlerService.activateHandler("de.htw.armt.app.handler.createassessment",
							new CallLeistungsnachweiseditor());

					ParameterizedCommand cmd = commandService
							.createCommand("de.htw.armt.app.command.createleistungsnachweis", null);
					handlerService.executeHandler(cmd, child);

				}
			}
			if (((MenuItem) event.getSource()).getText().equals(Messages.CreateLeistungsnachweisAction_0)) {
				if (object.getClass().equals(AssessmentGroup.class) || object.getClass().equals(Module.class)) {
					IEclipseContext child = application.getContext().createChild();
					child.set(Object.class, object);

					handlerService.activateHandler("de.htw.armt.app.handler.createassessmentgroup",
							new CallLeistungsnachweiseditor());

					ParameterizedCommand cmd = commandService
							.createCommand("de.htw.armt.app.command.createleistungsnachweismitteilleistungen", null);
					handlerService.executeHandler(cmd, child);

				}

			}

			if (((MenuItem) event.getSource()).getText().equals(Messages.DeleteLeistungsnachweisAction_0)) {
				if (object.getClass().equals(AssessmentGroup.class) || object.getClass().equals(Module.class)
						|| object.getClass().equals(Assessment.class)) {
					IEclipseContext child = application.getContext().createChild();
					child.set(Object.class, object);
					handlerService.activateHandler("de.htw.armt.app.handler.deleteassessmentgroup",
							new CallLeistungsnachweiseditor());

					ParameterizedCommand cmd = commandService
							.createCommand("de.htw.armt.app.command.deleteleistungsnachweis", null);
					handlerService.executeHandler(cmd, child);

				}

			}
		}
	}
}
