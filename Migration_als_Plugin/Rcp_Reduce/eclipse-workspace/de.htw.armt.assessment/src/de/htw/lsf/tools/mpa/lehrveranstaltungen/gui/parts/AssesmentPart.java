package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.listeners.AssesmentPartDoublecklickListener;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.listeners.AssessmentPartSelectionListener;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.listeners.ContextSelectionListener;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.providers.TreeContentProvider;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.providers.TreeLabelProvider;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;

/**
 * AssesmentPart is a Class to act as a Part It is used to display the left side
 * of the application, with the treeviewer
 * 
 * @author paulkulschewski
 *
 */
@SuppressWarnings("restriction")
public class AssesmentPart {

	Composite parent;

	TreeViewer treeViewer;

	@Inject
	MApplication application;

	@Inject
	ECommandService commandService;

	@Inject
	EHandlerService handlerService;

	@Inject
	MPart part;

	@Inject
	EPartService partService;

	@Inject
	ModulServiceHibernate ms;

	@Inject
	LeistungsnachweisServiceHibernate ls;

	Menu menu;

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	MenuItem newItem1;
	MenuItem newItem2;
	MenuItem newItem3;

	@PostConstruct
	public void createComposite(Composite parent) {

		this.parent = parent;
		this.treeViewer = new TreeViewer(parent);

		treeViewer
				.setContentProvider(ContextInjectionFactory.make(TreeContentProvider.class, application.getContext()));
		treeViewer.setLabelProvider(ContextInjectionFactory.make(TreeLabelProvider.class, application.getContext()));

		treeViewer.setInput(ms.selectAll());

		treeViewer.addDoubleClickListener(
				ContextInjectionFactory.make(AssesmentPartDoublecklickListener.class, application.getContext()));

		menu = new Menu(treeViewer.getControl());
		treeViewer.getControl().setMenu(menu);
		treeViewer.expandToLevel(2);
		treeViewer.addSelectionChangedListener(
				ContextInjectionFactory.make(AssessmentPartSelectionListener.class, application.getContext()));
	}

	@Focus
	public void setFocus() {
		treeViewer.getTree().setFocus();
	}

	public void setContextMenu(Object object) {

		if (object != null) {

			menu.addMenuListener(new MenuAdapter() {
				public void menuShown(MenuEvent e) {

					MenuItem[] items = menu.getItems();
					for (int i = 0; i < items.length; i++) {
						items[i].dispose();
					}

					newItem1 = new MenuItem(menu, SWT.NONE);
					newItem1.setText(Messages.CreateLeistungsnachweisAction_1);
					newItem1.setEnabled(false);
					newItem2 = new MenuItem(menu, SWT.NONE);
					newItem2.setText(Messages.CreateLeistungsnachweisAction_0);
					newItem2.setEnabled(false);
					newItem3 = new MenuItem(menu, SWT.NONE);
					newItem3.setText(Messages.DeleteLeistungsnachweisAction_0);
					newItem3.setEnabled(false);

					application.getContext().set(Object.class, object);

					newItem1.addSelectionListener(
							ContextInjectionFactory.make(ContextSelectionListener.class, application.getContext()));

					newItem2.addSelectionListener(
							ContextInjectionFactory.make(ContextSelectionListener.class, application.getContext()));

					newItem3.addSelectionListener(
							ContextInjectionFactory.make(ContextSelectionListener.class, application.getContext()));

					if (object.getClass().equals(Module.class)) {

						newItem2.setEnabled(true);

					}
					if (object.getClass().equals(AssessmentGroup.class)) {
						newItem1.setEnabled(true);

						newItem2.setEnabled(true);

						newItem3.setEnabled(true);

					}

					if (object.getClass().equals(Assessment.class)) {

						newItem3.setEnabled(true);
					}

				}
			});
		} else {
			return;
		}
	}

	public void update() {
		// save tree path and selection
		TreePath[] paths = treeViewer.getExpandedTreePaths();
		ISelection sel = treeViewer.getSelection();
		treeViewer
				.setContentProvider(ContextInjectionFactory.make(TreeContentProvider.class, application.getContext()));
		treeViewer.setLabelProvider(ContextInjectionFactory.make(TreeLabelProvider.class, application.getContext()));
		treeViewer.setInput(ms.selectAll());
		parent.getParent().layout();
		// restore tree path and selection
		treeViewer.setExpandedTreePaths(paths);
		treeViewer.setSelection(sel);
	}

}