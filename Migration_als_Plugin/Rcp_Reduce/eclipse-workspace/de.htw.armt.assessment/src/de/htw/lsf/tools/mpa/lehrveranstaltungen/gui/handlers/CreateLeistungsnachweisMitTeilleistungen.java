package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.handlers;

import java.util.Iterator;

import javax.inject.Inject;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts.AssesmentPart;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

/**
 * CreateLeistungsnachweisMitTeilleistungen is a Class to act as a handler It is used to create
 * new AssessmentGroups
 * 
 * @author paulkulschewski
 *
 */
public class CreateLeistungsnachweisMitTeilleistungen {
	@Inject
	EPartService partService;

	@Inject
	LeistungsnachweisServiceHibernate ls;

	@SuppressWarnings("rawtypes")
	@Execute
	public void execute(@Optional Object object) throws ExecutionException {
		if (object != null) {
			Object item = object;
			if (item != null) {
				if ((item instanceof Module) || (item instanceof AssessmentGroup)) {
					AssessmentGroup lnmtl = new AssessmentGroup(Messages.CreateLeistungsnachweisAction_2,
							Messages.CreateLeistungsnachweisAction_3);
					if (item instanceof Module) {
						Module modul = (Module) item;
						lnmtl.setModule(modul);
					} else if (item instanceof AssessmentGroup) {
						AssessmentGroup parent = (AssessmentGroup) item;
						parent.addAssessment(lnmtl);
						parent = (AssessmentGroup) ls.update(parent);
					}
					lnmtl = (AssessmentGroup) ls.insert(lnmtl);

					Iterator parts = partService.getParts().iterator();
					MPart part = null;

					while (parts.hasNext()) {
						part = (MPart) parts.next();
						if (part.getElementId().equals("de.htw.armt.assessment.part")) {
							break;
						} else {
							part = null;
						}

					}

					((AssesmentPart) part.getObject()).update();
				}
			}
		}

	}

}
