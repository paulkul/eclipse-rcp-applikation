package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.impl;

import java.util.List;

import javax.persistence.Query;

import de.htw.lsf.tools.mpa.base.scheme.dao.impl.AbstractDAOHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Term;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.SemesterDAO;

public class SemesterDAOHibernate extends AbstractDAOHibernate<Term> implements SemesterDAO {
	
	private static SemesterDAOHibernate instance;
	
	private SemesterDAOHibernate() {
	}

	public static synchronized SemesterDAOHibernate getInstance() {
		if (instance == null) {
			instance = new SemesterDAOHibernate();
		}
		return instance;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Term getSemesterBeiBezeichnung(String bezeichnung) {
		Query query = getEntityManager().createQuery("from Term where name = :name "
				).setParameter("name", bezeichnung);
		List l = query.setMaxResults(1).getResultList();
		if ((l != null) && (l.size() > 0)) {
			return (Term) l.get(0);
		}
		return null;
	}
}
