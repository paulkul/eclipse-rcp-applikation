package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.providers;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.jface.viewers.ITreeContentProvider;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

/**
 * TreeContentProvider is a Class to act as a Content Provider for the TreeViewer
 * 
 * @author paulkulschewski
 *
 */
public class TreeContentProvider implements ITreeContentProvider {
	@Inject
	LeistungsnachweisServiceHibernate ls;

	private static final Object[] EMPTY_ARRAY = new Object[0];

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List)
			return ((List<?>) inputElement).toArray();
		else
			return EMPTY_ARRAY;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof Module) {
			return ls.getLeistungsnachweiseFuerModul(((Module) parentElement)).toArray();
		}
		if (parentElement instanceof AssessmentGroup) {
			return ((AssessmentGroup) parentElement).getAssessments().toArray();
		}

		return null;
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof AssessmentGroup) {
			return ((AssessmentGroup) element).getModule();
		}
		if (element instanceof Assessment) {
			return ((Assessment) element).getAssessmentGroup();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof Module || element instanceof AssessmentGroup) {
			return true;
		}
		return false;
	}

}
