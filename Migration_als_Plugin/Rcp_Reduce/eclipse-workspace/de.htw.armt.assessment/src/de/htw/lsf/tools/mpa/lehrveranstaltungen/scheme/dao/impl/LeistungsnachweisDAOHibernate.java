/*
 * LeistungsnachweisDAOImpl.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import de.htw.lsf.tools.mpa.base.scheme.dao.impl.AbstractDAOHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.LeistungsnachweisDAO;

/**
 * @author V.Medvid
 */
public class LeistungsnachweisDAOHibernate extends AbstractDAOHibernate<AbstractAssessment>
		implements LeistungsnachweisDAO {

	private static LeistungsnachweisDAOHibernate instance = null;
	
	private LeistungsnachweisDAOHibernate() {
	}
	
	public static synchronized LeistungsnachweisDAOHibernate getInstance() {
		if (instance == null) {
			instance = new LeistungsnachweisDAOHibernate();
		}
		return instance;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AssessmentGroup> getLeistungsnachweiseFuerModul(Module m) {
		List<AssessmentGroup> objects =
				new ArrayList<AssessmentGroup>();
//		Query query = getSession().createQuery(
//				"from leistungsnachweis where modul=:modulNr ").setString(
//				"modulNr", m.getNummer());
		Query query = getEntityManager().createQuery(
		"from AssessmentGroup ag where ag.module.number = :modulNr ").setParameter(
		"modulNr", m.getNumber());
		objects = query.getResultList();
		return objects;
	}
}
