/*
 * ModulService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl;

import javax.inject.Singleton;
import javax.persistence.EntityManager;

import org.eclipse.e4.core.di.annotations.Creatable;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.impl.ModulDAOHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;

/**
 * @author V.Medvid
 */
@Creatable
@Singleton
public class ModulServiceHibernate extends AbstractServiceHibernate<Module> implements ModulService {

	public ModulServiceHibernate() {
		super(Module.class);
	}

	@Override
	public AbstractDAO<Module> getDAO(EntityManager entityManager) {
		ModulDAOHibernate dao = ModulDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

}
