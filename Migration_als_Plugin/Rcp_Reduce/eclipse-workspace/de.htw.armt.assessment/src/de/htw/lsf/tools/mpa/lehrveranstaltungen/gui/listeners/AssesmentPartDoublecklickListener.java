package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.listeners;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.handlers.CallLeistungsnachweiseditor;

/**
 * AssesmentPartDoublecklickListener is a Class to act as a listener It fulfils
 * the task of creating the command and executing the handler for the editor
 * creation
 * 
 * @author paulkulschewski
 *
 */
@SuppressWarnings("restriction")
public class AssesmentPartDoublecklickListener implements IDoubleClickListener {

	@Inject
	MApplication application;
	@Inject
	ECommandService commandService;
	@Inject
	EHandlerService handlerService;

	@Override
	public void doubleClick(DoubleClickEvent event) {

		IStructuredSelection thisSelection = (IStructuredSelection) event.getSelection();

		IEclipseContext child = application.getContext().createChild();
		child.set(IStructuredSelection.class, thisSelection);

		handlerService.activateHandler("de.htw.armt.app.handler.leistungsnachweiseditor",
				ContextInjectionFactory.make(CallLeistungsnachweiseditor.class, application.getContext()));

		ParameterizedCommand cmd = commandService.createCommand("de.htw.armt.app.command.assessment", null);

		handlerService.executeHandler(cmd, child);

	}

}
