package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.handlers;

import java.util.Collection;
import java.util.Iterator;

import javax.inject.Inject;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts.AssesmentEditor;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts.AssesmentGroupEditor;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts.ModulEditor;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;

/**
 * CallLeistungsnachweiseditor is a Class to act as a handler It creates an
 * Editor for the selected Element of the TreeViewer
 * 
 * @author paulkulschewski
 *
 */
public class CallLeistungsnachweiseditor {

	@Inject
	MApplication application;

	@Inject
	EPartService partService;

	@Inject
	EModelService modelService;

	@Execute
	public void execute(@Optional IStructuredSelection selection) throws ExecutionException {
		if (selection != null) {

		}

		Object selectedNode = selection.getFirstElement();
		Collection<MPartStack> stacks = null;
		if (!checkPartExists(selectedNode)) {
			MPart part = null;

			if (selectedNode.getClass().equals(AssessmentGroup.class)) {
				part = partService.createPart("de.htw.armt.app.partdescriptor.assessmentGroupEditor");
				stacks = modelService.findElements(application, null, MPartStack.class, null);
				part.setLabel(((AssessmentGroup) selectedNode).getName());
			}

			if (selectedNode.getClass().equals(Assessment.class)) {

				part = partService.createPart("de.htw.armt.app.partdescriptor.assessmentEditor");
				stacks = modelService.findElements(application, null, MPartStack.class, null);
				part.setLabel(((Assessment) selectedNode).getName());

			}
			if (selectedNode.getClass().equals(Module.class)) {
				part = partService.createPart("de.htw.armt.app.partdescriptor.modulEditor");
				stacks = modelService.findElements(application, null, MPartStack.class, null);
				part.setLabel(((Module) selectedNode).getName());
			}

			MPartStack partStack = null;
			Iterator<MPartStack> iter = stacks.iterator();

			while (iter.hasNext()) {
				MPartStack mpart = iter.next();
				if (mpart.getElementId().equals("de.htw.armt.app.partstack.0")) {
					partStack = (MPartStack) mpart;
					break;
				}
			}

			partStack.getChildren().add(part);
			partService.showPart(part, PartState.ACTIVATE);

			if (selectedNode.getClass().equals(Assessment.class)) {
				part.getContext().set("assessment", (Assessment) selectedNode);
				AssesmentEditor editor = (AssesmentEditor) part.getObject();
				editor.update();
			}

			if (selectedNode.getClass().equals(AssessmentGroup.class)) {
				part.getContext().set("assessmentGroup", (AssessmentGroup) selectedNode);
				AssesmentGroupEditor editor = (AssesmentGroupEditor) part.getObject();
				editor.update();
			}

			if (selectedNode.getClass().equals(Module.class)) {
				part.getContext().set("module", (Module) selectedNode);
				ModulEditor editor = (ModulEditor) part.getObject();
				editor.update();
			}

		}
	}

	public boolean checkPartExists(Object object) {
		Iterator<MPart> allParts = partService.getParts().iterator();
		while (allParts.hasNext()) {
			MPart mpart = (MPart) allParts.next();
			if (mpart.getElementId().equals("de.htw.armt.app.partdescriptor.modulEditor")) {
				ModulEditor editorPart = (ModulEditor) mpart.getObject();
				if (editorPart.getModule().equals(object)) {
					return true;
				}
			}
			if (mpart.getElementId().equals("de.htw.armt.app.partdescriptor.assessmentGroupEditor")) {
				AssesmentGroupEditor editorPart = (AssesmentGroupEditor) mpart.getObject();
				if (editorPart.getAssessmentGroup().equals(object)) {
					return true;
				}
			}
			if (mpart.getElementId().equals("de.htw.armt.app.partdescriptor.assessmentEditor")) {
				AssesmentEditor editorPart = (AssesmentEditor) mpart.getObject();
				if (editorPart.getAssessment().equals(object)) {
					return true;
				}
			}
		}
		return false;
	}
}