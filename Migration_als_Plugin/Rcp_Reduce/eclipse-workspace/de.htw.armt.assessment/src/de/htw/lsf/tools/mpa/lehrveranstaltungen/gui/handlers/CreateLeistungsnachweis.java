package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.handlers;

import java.util.Iterator;

import javax.inject.Inject;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts.AssesmentPart;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

/**
 * CreateLeistungsnachweis is a Class to act as a handler It is used to create
 * new Assessment
 * 
 * @author paulkulschewski
 *
 */
public class CreateLeistungsnachweis {
	@Inject
	EPartService partService;

	@Inject
	LeistungsnachweisServiceHibernate ls;

	@Execute
	public void execute(@Optional Object object) throws ExecutionException {

		if (object != null) {
			Object item = object;
			if (item != null) {
				if (item instanceof AssessmentGroup) {
					AssessmentGroup lmt = (AssessmentGroup) item;
					Assessment ln = new Assessment(Messages.CreateLeistungsnachweisAction_5);
					lmt.addAssessment(ln);
					lmt = (AssessmentGroup) ls.update(lmt);

					Iterator<MPart> parts = partService.getParts().iterator();
					MPart part = null;

					while (parts.hasNext()) {
						part = (MPart) parts.next();
						if (part.getElementId().equals("de.htw.armt.assessment.part")) {
							break;
						} else {
							part = null;
						}

					}

					((AssesmentPart) part.getObject()).update();

				}
			}
		}

	}

}
