package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.handlers;

import java.util.Iterator;

import javax.inject.Inject;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.dialogs.MessageDialog;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts.AssesmentPart;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

/**
 * DeleteLeistungsnachweis is a Class to act as a handler It is used to delete
 * objecs that are an instance of AbstractAssessment
 * 
 * @author paulkulschewski
 *
 */
public class DeleteLeistungsnachweis {
	private static final Logger LOGGER = Logger.getLogger(DeleteLeistungsnachweis.class);

	@Inject
	EPartService partService;

	@Inject
	LeistungsnachweisServiceHibernate ls;

	@Execute
	public void execute(@Optional Object object) throws ExecutionException {
		if (object != null) {
			Object item = object;
			if (item != null) {
				if (item != null && item instanceof AbstractAssessment) {
					AbstractAssessment ln = (AbstractAssessment) item;
					if (MessageDialog.openConfirm(null, Messages.DeleteLeistungsnachweisAction_1,
							Messages.DeleteLeistungsnachweisAction_2 + ln.getName()
									+ Messages.DeleteLeistungsnachweisAction_3 + ln.getAbbreviation()
									+ Messages.DeleteLeistungsnachweisAction_4)) {
						try {
							if (ln.getAssessmentGroup() != null) {
								ln.getAssessmentGroup().removeAssessment(ln);
							}
							ls.delete(ln);

							Iterator<MPart> parts = partService.getParts().iterator();
							MPart part = null;

							while (parts.hasNext()) {
								part = (MPart) parts.next();
								if (part.getElementId().equals("de.htw.armt.assessment.part")) {
									break;
								} else {
									part = null;
								}

							}

							((AssesmentPart) part.getObject()).update();

						} catch (Exception e) {
							LOGGER.log(Level.ERROR, Messages.DeleteLeistungsnachweisAction_5, e);
						}
						// }
					}
				}
			}

		}

	}
}