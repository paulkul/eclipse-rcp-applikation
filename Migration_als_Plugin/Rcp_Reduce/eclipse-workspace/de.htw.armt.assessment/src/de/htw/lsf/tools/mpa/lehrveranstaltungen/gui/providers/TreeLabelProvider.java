package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.providers;

import org.eclipse.jface.viewers.LabelProvider;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;

/**
 * TreeLabelProvider is a Class to act as a Label Provider for the TreeViewer
 * 
 * @author paulkulschewski
 *
 */
public class TreeLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof Assessment) {

			return ((Assessment) element).getName();

		}

		if (element instanceof Module) {
			return ((Module) element).getName();

		}

		if (element instanceof AssessmentGroup) {
			return ((AssessmentGroup) element).getAbbreviation();
		}
		return null;
	}

}
