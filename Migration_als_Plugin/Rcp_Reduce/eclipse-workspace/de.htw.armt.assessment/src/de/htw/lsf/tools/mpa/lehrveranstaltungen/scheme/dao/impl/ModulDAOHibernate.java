/*
 * ModulDAOImpl.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.impl;

import de.htw.lsf.tools.mpa.base.scheme.dao.impl.*;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.*;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.*;

/**
 * @author V.Medvid
 */
public class ModulDAOHibernate extends AbstractDAOHibernate<Module> implements ModulDAO {

	private static ModulDAOHibernate instance;
	
	private ModulDAOHibernate() {
	}

	public static synchronized ModulDAOHibernate getInstance() {
		if (instance == null) {
			instance = new ModulDAOHibernate();
		}
		return instance;
	}

}
