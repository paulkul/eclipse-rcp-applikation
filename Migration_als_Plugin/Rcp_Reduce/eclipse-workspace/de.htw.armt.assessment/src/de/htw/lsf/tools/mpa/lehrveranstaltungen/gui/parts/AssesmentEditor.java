package de.htw.lsf.tools.mpa.lehrveranstaltungen.gui.parts;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;

/**
 * AssesmentEditor is a Class to act as a Editor It is used to edit objects of
 * the class Assessment
 * 
 * @author paulkulschewski
 *
 */
public class AssesmentEditor {

	private static final Logger LOGGER = Logger.getLogger(AssesmentEditor.class);

	@Inject
	private MPart part;

	@Inject
	ModulServiceHibernate ms;

	@Inject
	LeistungsnachweisServiceHibernate ls;

	private DataBindingContext bindingContext;

	private Text bezeichnungValue;
	Label bezeichnung;
	Label kuerzel;
	Text kuerzelValue;
	Label maxPunkte;
	Text maxPunkteValue;
	Label gruppenleistung;
	Label teilnahmepflicht;
	Button gruppenleistungValue;
	Button teilnahmepflichtValue;
	Label formel;
	Text formelValue;
	FormToolkit toolkit;

	GridLayout gridLayout;
	Composite parent;

	private Assessment assessment;

	@PostConstruct
	public void createComposite(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());
		this.parent = parent;

		part.setCloseable(true);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		parent.setLayout(gridLayout);

		Section section = toolkit.createSection(parent, Section.TITLE_BAR);
		section.setText(Messages.LeistungsnachweisEditor_Titel);

		toolkit.createLabel(parent, null, SWT.NONE);
		kuerzel = toolkit.createLabel(parent, null, SWT.NONE);
		kuerzel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		kuerzelValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		kuerzelValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnung = toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnung.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnungValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		bezeichnungValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		maxPunkte = toolkit.createLabel(parent, null, SWT.NONE);
		maxPunkte.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		maxPunkteValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		maxPunkteValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		gruppenleistung = toolkit.createLabel(parent, null, SWT.NONE);
		gruppenleistung.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		gruppenleistungValue = toolkit.createButton(parent, null, SWT.CHECK);
		gruppenleistungValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		gruppenleistungValue.setVisible(false);
		gruppenleistungValue.setEnabled(false);

		toolkit.createLabel(parent, null, SWT.NONE);
		teilnahmepflicht = toolkit.createLabel(parent, null, SWT.NONE);
		teilnahmepflicht.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		teilnahmepflichtValue = toolkit.createButton(parent, null, SWT.CHECK);
		teilnahmepflichtValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		teilnahmepflichtValue.setVisible(false);
		teilnahmepflichtValue.setEnabled(false);

	}

	public void update() {

		this.setAssessment((Assessment) part.getContext().get("assessment"));

		boolean isGroup = false;
		boolean isMandatory = false;
		try {
			isGroup = getAssessment().isGroupAssessment();
		} catch (NullPointerException exp) {
			LOGGER.log(Level.ERROR, exp);

		}
		try {
			isMandatory = getAssessment().isMandatory();
		} catch (NullPointerException exp) {
			LOGGER.log(Level.ERROR, exp);
		}

		bezeichnungValue.setText(getAssessment().getLongName());
		kuerzelValue.setText(String.valueOf(getAssessment().getNumber()));

		bezeichnung.setText(Messages.LeistungsnachweisEditor_Bezeichnung);
		kuerzel.setText(Messages.LeistungsnachweisEditor_Kuerzel);

		maxPunkte.setText(Messages.LeistungsnachweisEditor_Punkte);
		maxPunkteValue.setText(String.valueOf(getAssessment().getMaxScore()));

		gruppenleistung.setText(Messages.LeistungsnachweisEditor_Gruppenleistung);
		gruppenleistungValue.setSelection(isGroup);
		gruppenleistungValue.setEnabled(true);

		teilnahmepflicht.setText(Messages.LeistungsnachweisEditor_Teilnahmepflicht);
		teilnahmepflichtValue.setSelection(isMandatory);
		teilnahmepflichtValue.setEnabled(true);

		bezeichnungValue.setVisible(true);
		kuerzelValue.setVisible(true);
		bezeichnung.setVisible(true);
		kuerzel.setVisible(true);
		maxPunkte.setVisible(true);
		maxPunkteValue.setVisible(true);
		gruppenleistung.setVisible(true);
		gruppenleistungValue.setVisible(true);
		teilnahmepflicht.setVisible(true);
		teilnahmepflichtValue.setVisible(true);

		bezeichnungValue.getParent().layout();

		bindValues();

		addModifyListener(bezeichnungValue);
		addModifyListener(kuerzelValue);
		addModifyListener(maxPunkteValue);
		addSelectionListener(gruppenleistungValue);
		addSelectionListener(teilnahmepflichtValue);

	}

	@Focus
	public void setFocus() {
		bezeichnungValue.setFocus();
	}

	@SuppressWarnings("unused")
	@Persist
	public void save() {
		List<?> bindings = bindingContext.getBindings();
		bindingContext.updateModels();
		ls.update(getAssessment());
		part.setDirty(false);
	}

	@PreDestroy
	public void exit() {
		part.setDirty(false);
	}

	private void addModifyListener(Text text) {
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				part.setDirty(true);

			}
		});
	}

	private void addSelectionListener(Button button) {
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				part.setDirty(true);
			}
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void bindValues() {
		// The DataBindingContext object will manage the databindings
		// Lets bind it
		bindingContext = new DataBindingContext();
		IObservableValue widgetValue = WidgetProperties.text(SWT.Modify).observe(bezeichnungValue);
		IObservableValue modelValue = PojoProperties.value(Assessment.class, Assessment.NAME_FIELD)
				.observe(getAssessment());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(kuerzelValue);
		modelValue = PojoProperties.value(Assessment.class, Assessment.ABBREVIATION_FIELD).observe(getAssessment());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(maxPunkteValue);
		modelValue = PojoProperties.value(Assessment.class, Assessment.MAXSCORE_FIELD).observe(getAssessment());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

		widgetValue = WidgetProperties.selection().observe(gruppenleistungValue);
		modelValue = PojoProperties.value(Assessment.class, Assessment.GROUPASSESSMENT_FIELD).observe(getAssessment());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

		widgetValue = WidgetProperties.selection().observe(teilnahmepflichtValue);
		modelValue = PojoProperties.value(Assessment.class, Assessment.MANDATORY_FIELD).observe(getAssessment());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

	}

	public Assessment getAssessment() {
		return assessment;
	}

	public void setAssessment(Assessment assessment) {
		this.assessment = assessment;
	}
}