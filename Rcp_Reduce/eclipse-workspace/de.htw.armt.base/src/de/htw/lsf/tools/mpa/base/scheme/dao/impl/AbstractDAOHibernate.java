package de.htw.lsf.tools.mpa.base.scheme.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;
import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;

abstract public class AbstractDAOHibernate<T extends ModelObject> implements AbstractDAO<T> {

	private EntityManager entityManager;
	private Logger logger = Logger.getLogger(AbstractDAOHibernate.class);

	public void setEntityManager(EntityManager s) {
		this.entityManager = s;
	}

	protected EntityManager getEntityManager() {
		if (entityManager == null) {
			logger.log(Level.WARN, "Session ist im DAO nicht gesetzt!");
		}
		return entityManager;
	}

	public T insert(T object) {
		getEntityManager().persist(object);
//		object = (T) getEntityManager().get(object.getClass(), id);
		return object;
	}

	public T saveOrUpdate(T object) {
//		if (getEntityManager().contains(object)) {
		object = getEntityManager().merge(object);
//		}
//		else {
//			getEntityManager().persist(object);
//		}
//		Serializable id = getEntityManager().getIdentifier(object);
//		return (T) getEntityManager().get(object.getClass(), id);
		return object;
	}

	public void delete(T object) {
		getEntityManager().remove(object);
	}

	public T find(Serializable id, Class<T> clazz) {
		T object = (T) getEntityManager().find(clazz, id);
		return object;
	}

	public T update(T object) {
		object = getEntityManager().merge(object);
//		Serializable id = getEntityManager().getIdentifier(object);
//		object = (T) getEntityManager().find(object.getClass(), id);
		return object;
	}

	@SuppressWarnings("unchecked")
	public List<T> selectAll(Class<T> clazz) {
//		List<T> objects = getEntityManager().createCriteria(clazz).list();
		List<T> objects = getEntityManager().createQuery("select o from " + clazz.getSimpleName() + " o")
				.getResultList();
		return objects;
	}

	public boolean exists(Serializable id, Class<T> clazz) {
		boolean exists = false;
		Object o = getEntityManager().find(clazz, id);
		exists = (o != null);
		return exists;
	}
}
