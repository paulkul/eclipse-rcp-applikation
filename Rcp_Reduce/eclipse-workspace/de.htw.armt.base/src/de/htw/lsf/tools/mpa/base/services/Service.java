/*
 * Service.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.base.services;

import java.io.Serializable;
import java.util.List;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;

/**
 * @author V.Medvid
 */
public interface Service<T extends ModelObject> {

	public T insert(T object);
	public void delete(T object);
	public T find(Serializable id);
	public T update(T object);
	public List<T> selectAll();
	public boolean exists(Serializable id);
}
