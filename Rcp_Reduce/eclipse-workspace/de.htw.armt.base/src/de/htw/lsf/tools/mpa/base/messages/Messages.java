package de.htw.lsf.tools.mpa.base.messages;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "messages"; //$NON-NLS-1$
	
	public static String AbstractServiceHibernate_0;
	public static String AbstractServiceHibernate_1;
	public static String AbstractServiceHibernate_2;
	public static String AbstractServiceHibernate_3;
	public static String AbstractServiceHibernate_4;
	public static String AbstractServiceHibernate_5;
	public static String AbstractServiceHibernate_6;
	public static String AbstractServiceHibernate_7;
	public static String AbstractServiceHibernate_8;

	public static String RefreshViewAction_0;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
