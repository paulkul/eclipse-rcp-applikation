package de.htw.armt.assessment.ui.handlers;

import java.util.Iterator;

import javax.inject.Inject;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import de.htw.armt.assessment.ui.parts.AssesmentPart;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

public class CreateLeistungsnachweis {
	@Inject
	MApplication application;

	@Inject
	EPartService partService;

	@Inject
	EModelService modelService;
	/**
	 * @Inject LeistungsnachweisServiceHibernate lsh;
	 * 
	 *         LeistungsnachweisService ls = lsh;
	 */

	LeistungsnachweisService ls = LeistungsnachweisServiceHibernate.getInstance();

	@Execute
	public void execute(@Optional Object object) throws ExecutionException {

		if (object != null) {
			Object item = object;
			if (item != null) {
				if (item instanceof AssessmentGroup) {
					AssessmentGroup lmt = (AssessmentGroup) item;
					Assessment ln = new Assessment(Messages.CreateLeistungsnachweisAction_5);
					lmt.addAssessment(ln);
					lmt = (AssessmentGroup) ls.update(lmt);

					Iterator<MPart> parts = partService.getParts().iterator();
					MPart part = null;

					while (parts.hasNext()) {
						part = (MPart) parts.next();
						if (part.getElementId().equals("de.htw.armt.assessment.part")) {
							break;
						} else {
							part = null;
						}

					}

					((AssesmentPart) part.getObject()).update();

				}
			}
		}

	}

}
