package de.htw.armt.assessment.ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import de.htw.armt.assessment.ui.TreeLabelProvider;
import de.htw.armt.assessment.ui.listeners.AssesmentPartDoublecklickListener;
import de.htw.armt.assessment.ui.listeners.AssessmentPartSelectionListener;
import de.htw.armt.assessment.ui.listeners.ContextSelectionListener;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;

@SuppressWarnings("restriction")
public class AssesmentPart {

	// private TableViewer tableViewer;

	Composite parent;

	@Inject
	MApplication application;

	@Inject
	ECommandService commandService;

	@Inject
	EHandlerService handlerService;

	@Inject
	MPart part;

	@Inject
	EPartService partService;

	TreeViewer treeViewer;

	ModulService ms = ModulServiceHibernate.getInstance();

	LeistungsnachweisService ls = LeistungsnachweisServiceHibernate.getInstance();

	Menu menu;

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	MenuItem newItem1;
	MenuItem newItem2;
	MenuItem newItem3;

	@PostConstruct
	public void createComposite(Composite parent) {

		this.parent = parent;
		this.treeViewer = new TreeViewer(parent);

		treeViewer.setContentProvider(new de.htw.armt.assessment.ui.TreeContentProvider());
		treeViewer.setLabelProvider(new TreeLabelProvider());

		treeViewer.setInput(ms.selectAll());

		treeViewer.addDoubleClickListener(
				new AssesmentPartDoublecklickListener(application, commandService, handlerService));

		menu = new Menu(treeViewer.getControl());
		treeViewer.getControl().setMenu(menu);

		treeViewer.addSelectionChangedListener(new AssessmentPartSelectionListener(partService));
	}

	@Focus
	public void setFocus() {
		treeViewer.getTree().setFocus();
	}

	public void setContextMenu(Object object) {

		menu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {

				MenuItem[] items = menu.getItems();
				for (int i = 0; i < items.length; i++) {
					items[i].dispose();
				}

				newItem1 = new MenuItem(menu, SWT.NONE);
				newItem1.setText("Leistungsnachweis erzeugen");
				newItem1.setEnabled(false);
				newItem2 = new MenuItem(menu, SWT.NONE);
				newItem2.setText("Leistungsnachweis mit Teilleistungen erzeugen");
				newItem2.setEnabled(false);
				newItem3 = new MenuItem(menu, SWT.NONE);
				newItem3.setText("Leistungsnachweis löschen");
				newItem3.setEnabled(false);

				if (object == null) {
					return;
				}

				newItem1.addSelectionListener(
						new ContextSelectionListener(application, commandService, handlerService, object, newItem1));

				newItem2.addSelectionListener(
						new ContextSelectionListener(application, commandService, handlerService, object, newItem2));

				newItem3.addSelectionListener(
						new ContextSelectionListener(application, commandService, handlerService, object, newItem3));

				if (object.getClass().equals(Module.class)) {

					newItem2.setEnabled(true);

				}
				if (object.getClass().equals(AssessmentGroup.class)) {
					newItem1.setEnabled(true);

					newItem2.setEnabled(true);

					newItem3.setEnabled(true);

				}

				if (object.getClass().equals(Assessment.class)) {

					newItem3.setEnabled(true);
				}

			}
		});

	}

	public void update() {
		treeViewer.setContentProvider(new de.htw.armt.assessment.ui.TreeContentProvider());
		treeViewer.setLabelProvider(new TreeLabelProvider());
		treeViewer.setInput(ms.selectAll());
		parent.getParent().layout();
	}

}