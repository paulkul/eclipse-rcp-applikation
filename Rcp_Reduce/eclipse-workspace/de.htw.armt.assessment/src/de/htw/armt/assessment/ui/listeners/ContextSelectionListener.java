package de.htw.armt.assessment.ui.listeners;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.MenuItem;

import de.htw.armt.assessment.ui.handlers.CallLeistungsnachweiseditor;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;

@SuppressWarnings("restriction")
public class ContextSelectionListener extends SelectionAdapter {

	MApplication application;

	ECommandService commandService;

	EHandlerService handlerService;

	Object object;

	MenuItem menuItem;

	public ContextSelectionListener(MApplication application, ECommandService commandService,
			EHandlerService handlerService, Object object, MenuItem menuItem) {
		this.application = application;
		this.commandService = commandService;
		this.handlerService = handlerService;
		this.object = object;
		this.menuItem = menuItem;
	}

	@Override
	public void widgetSelected(SelectionEvent event) {
		if (menuItem.getText().equals("Leistungsnachweis erzeugen")) {
			if (object.getClass().equals(AssessmentGroup.class)) {
				IEclipseContext child = application.getContext().createChild();
				child.set(Object.class, object);

				handlerService.activateHandler("de.htw.armt.app.handler.createassessment",
						new CallLeistungsnachweiseditor());

				ParameterizedCommand cmd = commandService
						.createCommand("de.htw.armt.app.command.createleistungsnachweis", null);
				handlerService.executeHandler(cmd, child);

			}
		}
		if (menuItem.getText().equals("Leistungsnachweis mit Teilleistungen erzeugen")) {
			if (object.getClass().equals(AssessmentGroup.class) || object.getClass().equals(Module.class)) {
				IEclipseContext child = application.getContext().createChild();
				child.set(Object.class, object);

				handlerService.activateHandler("de.htw.armt.app.handler.createassessmentgroup",
						new CallLeistungsnachweiseditor());

				ParameterizedCommand cmd = commandService
						.createCommand("de.htw.armt.app.command.createleistungsnachweismitteilleistungen", null);
				handlerService.executeHandler(cmd, child);

			}

		}

		if (menuItem.getText().equals("Leistungsnachweis löschen")) {
			if (object.getClass().equals(AssessmentGroup.class) || object.getClass().equals(Module.class)) {
				IEclipseContext child = application.getContext().createChild();
				child.set(Object.class, object);
				handlerService.activateHandler("de.htw.armt.app.handler.deleteassessmentgroup",
						new CallLeistungsnachweiseditor());

				ParameterizedCommand cmd = commandService
						.createCommand("de.htw.armt.app.command.deleteleistungsnachweis", null);
				handlerService.executeHandler(cmd, child);

			}

		}
	}

}
