package de.htw.armt.assessment.ui.handlers;

import java.util.Iterator;

import javax.inject.Inject;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import de.htw.armt.assessment.ui.parts.AssesmentPart;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

public class CreateLeistungsnachweisMitTeilleistungen {
	@Inject
	MApplication application;

	@Inject
	EPartService partService;

	@Inject
	EModelService modelService;

	LeistungsnachweisService ls = LeistungsnachweisServiceHibernate.getInstance();

	@SuppressWarnings("rawtypes")
	@Execute
	public void execute(@Optional Object object) throws ExecutionException {
		if (object != null) {
			Object item = object;
			if (item != null) {
				if ((item instanceof Module) || (item instanceof AssessmentGroup)) {
					AssessmentGroup lnmtl = new AssessmentGroup(Messages.CreateLeistungsnachweisAction_2,
							Messages.CreateLeistungsnachweisAction_3);
					if (item instanceof Module) {
						Module modul = (Module) item;
						lnmtl.setModule(modul);
					} else if (item instanceof AssessmentGroup) {
						AssessmentGroup parent = (AssessmentGroup) item;
						parent.addAssessment(lnmtl);
						parent = (AssessmentGroup) ls.update(parent);
					}
					lnmtl = (AssessmentGroup) ls.insert(lnmtl);

					Iterator parts = partService.getParts().iterator();
					MPart part = null;

					while (parts.hasNext()) {
						part = (MPart) parts.next();
						if (part.getElementId().equals("de.htw.armt.assessment.part")) {
							break;
						} else {
							part = null;
						}

					}

					((AssesmentPart) part.getObject()).update();
				}
			}
		}

	}

}
