package de.htw.armt.assessment.ui.parts;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;

public class AssesmentGroupEditor {

	@Inject
	private MPart part;

	ModulService ms = ModulServiceHibernate.getInstance();
	LeistungsnachweisService ls = LeistungsnachweisServiceHibernate.getInstance();
	private DataBindingContext bindingContext;

	private Text bezeichnungValue;
	Label bezeichnung;
	Label kuerzel;
	Text kuerzelValue;
	Label formel;
	Text formelValue;
	FormToolkit toolkit;

	GridLayout gridLayout;
	Composite parent;

	private AssessmentGroup assessmentGroup;

	@PostConstruct
	public void createComposite(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());
		this.parent = parent;

		part.setCloseable(true);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		parent.setLayout(gridLayout);

		Section section1 = toolkit.createSection(parent, Section.TITLE_BAR);
		section1.setText(Messages.LeistungsnachweisEditor_Titel);

		toolkit.createLabel(parent, null, SWT.NONE);
		kuerzel = toolkit.createLabel(parent, null, SWT.NONE);
		kuerzel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		kuerzelValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		kuerzelValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnung = toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnung.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnungValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		bezeichnungValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		formel = toolkit.createLabel(parent, null, SWT.NONE);
		formel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		formelValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		formelValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

	}

	@Focus
	public void setFocus() {

	}

	@SuppressWarnings("unused")
	@Persist
	public void save() {
		List<?> bindings = bindingContext.getBindings();
		bindingContext.updateModels();
		ls.update(getAssessmentGroup());
		part.setDirty(false);
	}

	@PreDestroy
	public void exit() {
		part.setDirty(false);
	}

	public void update() {

		this.setAssessmentGroup((AssessmentGroup) part.getContext().get("assessmentGroup"));

		bezeichnungValue.setText(getAssessmentGroup().getName());
		kuerzelValue.setText(String.valueOf(getAssessmentGroup().getNumber()));

		bezeichnung.setText(Messages.LeistungsnachweisEditor_Bezeichnung);
		kuerzel.setText(Messages.LeistungsnachweisEditor_Kuerzel);

		formel.setText(Messages.LeistungsnachweisEditor_Formel);
		if (getAssessmentGroup().getFormula() != null) {
			formelValue.setText(getAssessmentGroup().getFormula());
		}
		bezeichnungValue.setVisible(true);
		kuerzelValue.setVisible(true);
		kuerzel.setVisible(true);
		bezeichnung.setVisible(true);
		formel.setVisible(true);
		formelValue.setVisible(true);

		bezeichnungValue.getParent().layout();

		bindValues();

		addModifyListener(bezeichnungValue);
		addModifyListener(kuerzelValue);
		addModifyListener(formelValue);

	}

	private void addModifyListener(Text text) {
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				part.setDirty(true);

			}
		});
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void bindValues() {
		bindingContext = new DataBindingContext();
		IObservableValue widgetValue = WidgetProperties.text(SWT.Modify).observe(bezeichnungValue);
		IObservableValue modelValue = PojoProperties.value(AssessmentGroup.class, AssessmentGroup.NAME_FIELD)
				.observe(getAssessmentGroup());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(kuerzelValue);
		modelValue = PojoProperties.value(AssessmentGroup.class, AssessmentGroup.ABBREVIATION_FIELD)
				.observe(getAssessmentGroup());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(formelValue);
		modelValue = PojoProperties.value(AssessmentGroup.class, AssessmentGroup.FORMULA_FIELD)
				.observe(getAssessmentGroup());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);
	}

	public AssessmentGroup getAssessmentGroup() {
		return assessmentGroup;
	}

	public void setAssessmentGroup(AssessmentGroup assessmentGroup) {
		this.assessmentGroup = assessmentGroup;
	}

}