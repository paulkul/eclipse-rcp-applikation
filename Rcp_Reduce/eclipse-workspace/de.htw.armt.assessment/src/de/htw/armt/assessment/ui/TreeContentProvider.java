package de.htw.armt.assessment.ui;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;

import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.LeistungsnachweisServiceHibernate;

public class TreeContentProvider implements ITreeContentProvider {

	LeistungsnachweisService ls = LeistungsnachweisServiceHibernate.getInstance();
	private static final Object[] EMPTY_ARRAY = new Object[0];

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List)
			return ((List<?>) inputElement).toArray();
		else
			return EMPTY_ARRAY;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof Module) {
			return ls.getLeistungsnachweiseFuerModul(((Module) parentElement)).toArray();
		}
		if (parentElement instanceof AssessmentGroup) {
			return ((AssessmentGroup) parentElement).getAssessments().toArray();
		}

		return null;
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof AssessmentGroup) {
			return ((AssessmentGroup) element).getModule();
		}
		if (element instanceof Assessment) {
			return ((Assessment) element).getAssessmentGroup();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof Module || element instanceof AssessmentGroup) {
			return true;
		}
		return false;
	}

}
