package de.htw.armt.assessment.ui.listeners;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.htw.armt.assessment.ui.handlers.CallLeistungsnachweiseditor;

@SuppressWarnings("restriction")
public class AssesmentPartDoublecklickListener implements IDoubleClickListener {

	MApplication application;

	ECommandService commandService;

	EHandlerService handlerService;

	public AssesmentPartDoublecklickListener(MApplication application, ECommandService commandService,
			EHandlerService handlerService) {
		this.application = application;
		this.commandService = commandService;
		this.handlerService = handlerService;
	}

	@Override
	public void doubleClick(DoubleClickEvent event) {

		IStructuredSelection thisSelection = (IStructuredSelection) event.getSelection();

		IEclipseContext child = application.getContext().createChild();
		child.set(IStructuredSelection.class, thisSelection);

		handlerService.activateHandler("de.htw.armt.app.handler.leistungsnachweiseditor",
				new CallLeistungsnachweiseditor());

		ParameterizedCommand cmd = commandService.createCommand("de.htw.armt.app.command.assessment", null);

		handlerService.executeHandler(cmd, child);

	}

}
