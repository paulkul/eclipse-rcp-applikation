package de.htw.armt.assessment.ui.parts;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import de.htw.lsf.tools.mpa.base.utils.SimpleConverter;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.Messages;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl.ModulServiceHibernate;

public class ModulEditor {

	@Inject
	private EPartService partService;

	@Inject
	private MPart part;

	ModulService ms = ModulServiceHibernate.getInstance();

	private DataBindingContext bindingContext;

	private Text bezeichnungValue;
	Label bezeichnung;
	Label kuerzel;
	Text kürzelValue;
	FormToolkit toolkit;

	GridLayout gridLayout;
	Composite parent;

	private Module module;

	Section section;

	@SuppressWarnings("unused")
	@PostConstruct
	public void createComposite(Composite parent) {

		Iterator<?> parts = partService.getParts().iterator();

		while (parts.hasNext()) {
			MPart part = (MPart) parts.next();
		}

		toolkit = new FormToolkit(parent.getDisplay());
		this.parent = parent;

		part.setCloseable(true);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		parent.setLayout(gridLayout);

		section = toolkit.createSection(parent, Section.TITLE_BAR);
		section.setText(Messages.ModulEditor_Titel);

		toolkit.createLabel(parent, null, SWT.NONE);
		kuerzel = toolkit.createLabel(parent, null, SWT.NONE);
		kuerzel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		kürzelValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		kürzelValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnung = toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnung.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		toolkit.createLabel(parent, null, SWT.NONE);
		bezeichnungValue = toolkit.createText(parent, null, SWT.SINGLE | SWT.BORDER);
		bezeichnungValue.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

	}

	@Focus
	public void setFocus() {
		bezeichnungValue.setFocus();
	}

	@SuppressWarnings("unused")
	@Persist
	public void save() {
		List<?> bindings = bindingContext.getBindings();
		bindingContext.updateModels();
		ms.update(getModule());
		part.setDirty(false);
	}

	@PreDestroy
	public void exit() {
		part.setDirty(false);
	}

	private void addModifyListener(Text text) {
		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				part.setDirty(true);

			}
		});
	}

	public void update() {

		this.setModule((Module) part.getContext().get("module"));

		bezeichnungValue.setText(getModule().getName());
		kürzelValue.setText(String.valueOf(getModule().getNumber()));

		bezeichnung.setText(Messages.LeistungsnachweisEditor_Bezeichnung);
		kuerzel.setText(Messages.LeistungsnachweisEditor_Kuerzel);

		bezeichnungValue.setVisible(true);
		kürzelValue.setVisible(true);
		kuerzel.setVisible(true);
		bezeichnung.setVisible(true);

		bezeichnungValue.getParent().layout();

		bindValues();
		addModifyListener(bezeichnungValue);
		addModifyListener(kürzelValue);

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	private void bindValues() {
		bindingContext = new DataBindingContext();
		IObservableValue widgetValue = WidgetProperties.text(SWT.Modify).observe(kürzelValue);
		IObservableValue modelValue = PojoProperties.value(Module.class, Module.NUMBER_FIELD).observe(getModule());
		UpdateValueStrategy targetToModel = new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT);
		targetToModel.setConverter(SimpleConverter.STRING_TO_LONG_CONVERTER);
		UpdateValueStrategy modelToTarget = new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE);
		modelToTarget.setConverter(SimpleConverter.LONG_TO_STRING_CONVERTER);
		bindingContext.bindValue(widgetValue, modelValue, targetToModel, modelToTarget);

		widgetValue = WidgetProperties.text(SWT.Modify).observe(bezeichnungValue);
		modelValue = PojoProperties.value(Module.class, Module.NAME_FIELD).observe(getModule());
		bindingContext.bindValue(widgetValue, modelValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_CONVERT),
				null);

		List bindings = bindingContext.getBindings();

	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

}