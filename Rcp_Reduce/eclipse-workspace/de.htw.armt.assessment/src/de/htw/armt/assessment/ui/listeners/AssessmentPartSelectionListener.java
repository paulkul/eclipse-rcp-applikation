package de.htw.armt.assessment.ui.listeners;

import java.util.Iterator;


import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;

import de.htw.armt.assessment.ui.parts.AssesmentPart;

public class AssessmentPartSelectionListener implements ISelectionChangedListener {

	private IStructuredSelection selection;

	private EPartService partService;

	public AssessmentPartSelectionListener(EPartService partService) {
		this.partService = partService;
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {

		Iterator<MPart> parts = partService.getParts().iterator();
		MPart part = null;

		while (parts.hasNext()) {
			part = (MPart) parts.next();
			if (part.getElementId().equals("de.htw.armt.assessment.part")) {
				break;
			} else {
				part = null;
			}

		}

		ISelection incoming = event.getSelection();
		AssesmentPart aPart = (AssesmentPart) part.getObject();
		if (incoming instanceof IStructuredSelection) {
			selection = (IStructuredSelection) incoming;
			if (selection.size() == 1) {
				Object obj = selection.getFirstElement();

				aPart.setContextMenu(obj);
			} else {
				aPart.setContextMenu(null);
			}
		} else {
			aPart.setContextMenu(null);
		}

	}

}
