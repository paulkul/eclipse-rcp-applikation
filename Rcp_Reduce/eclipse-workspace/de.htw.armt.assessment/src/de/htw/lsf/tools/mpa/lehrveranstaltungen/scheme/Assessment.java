package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("L")
public class Assessment extends AbstractAssessment {

	private static final long serialVersionUID = -4170783865912929808L;
	public static final String MAXSCORE_FIELD = "maxScore";
	public static String GROUPASSESSMENT_FIELD = "groupAssessment";
	public static String MANDATORY_FIELD = "mandatory";

	private double maxScore;
	private boolean groupAssessment = false;
	private boolean mandatory = false;

	public Assessment() {
		super();
	}

	public Assessment(String description) {
		super(description, null);		
	}

	@Column(name = "maxpunkte")
	public double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(double maxScore) {
		this.maxScore = maxScore;
	}

	@Column(name = "gruppenleistung")
	public boolean isGroupAssessment() {
		return groupAssessment;
	}

	public void setGroupAssessment(boolean groupAssessment) {
		this.groupAssessment = groupAssessment;
	}

	@Column(name = "teilnahmepflicht")
	@Override
	public boolean isMandatory() {
		return mandatory;
	}

	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}

}
