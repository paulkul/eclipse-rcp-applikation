package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao;

import java.util.*;

import de.htw.lsf.tools.mpa.base.scheme.dao.*;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.*;

public interface LeistungsnachweisDAO extends AbstractDAO<AbstractAssessment> {

	public List<AssessmentGroup> getLeistungsnachweiseFuerModul(Module m);
}
