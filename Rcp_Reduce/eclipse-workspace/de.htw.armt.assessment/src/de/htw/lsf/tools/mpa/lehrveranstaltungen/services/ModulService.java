/*
 * ModulService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.services;

import de.htw.lsf.tools.mpa.base.services.*;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.*;

/**
 * @author V.Medvid
 */
public interface ModulService extends Service<Module> {

}
