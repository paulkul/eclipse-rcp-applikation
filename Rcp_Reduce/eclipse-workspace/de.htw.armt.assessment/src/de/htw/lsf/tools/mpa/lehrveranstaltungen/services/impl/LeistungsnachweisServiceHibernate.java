/*
 * LeistungsnachweisService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.hibernate.HibernateException;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.base.utils.HibernateUtil;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.LeistungsnachweisDAO;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.impl.LeistungsnachweisDAOHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ScriptService;

/**
 * @author V.Medvid
 */
public class LeistungsnachweisServiceHibernate extends AbstractServiceHibernate<AbstractAssessment>
		implements LeistungsnachweisService {

	private static LeistungsnachweisServiceHibernate instance = null;

	public LeistungsnachweisServiceHibernate() {
		super(AbstractAssessment.class);
	}

	@Override
	public AbstractDAO<AbstractAssessment> getDAO(EntityManager entityManager) {
		LeistungsnachweisDAOHibernate dao = LeistungsnachweisDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static LeistungsnachweisServiceHibernate getInstance() {
		if (instance == null) {
			instance = new LeistungsnachweisServiceHibernate();
		}
		return instance;

	}

	@Override
	public List<AssessmentGroup> getLeistungsnachweiseFuerModul(Module m) throws HibernateException {
		EntityManager entityManager = null;
		List<AssessmentGroup> objects = new ArrayList<AssessmentGroup>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((LeistungsnachweisDAO) getDAO(entityManager)).getLeistungsnachweiseFuerModul(m);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}

	@Override
	public double getMaxPunkte(AbstractAssessment assessment) {
		if (assessment instanceof Assessment) {
			return ((Assessment) assessment).getMaxScore();
		} else {
			AssessmentGroup ln = (AssessmentGroup) assessment;
			double max = 0;
			String formel = ln.getFormula();
			List<AbstractAssessment> tln = ln.getAssessments();
			Map<String, Double> bws = new HashMap<String, Double>();
			for (AbstractAssessment aln : tln) {
				double wert = 0;
				if (aln instanceof Assessment) {
					wert = ((Assessment) aln).getMaxScore();
				} else if (aln instanceof AssessmentGroup) {
					wert = getMaxPunkte((AssessmentGroup) aln);
				}
				bws.put(aln.getAbbreviation(), wert);
			}
			max = ScriptService.parseFormel(formel, bws);
			return max;
		}
	}

	public List<Assessment> findByLeistungsnachweisId(Long leistungsnachweisId) throws HibernateException {
		EntityManager entityManager = null;
		List<Assessment> result = null;
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();

			LeistungsnachweisDAO dao = ((LeistungsnachweisDAO) getDAO(entityManager));
			AbstractAssessment leistungsnachweis = dao.find(leistungsnachweisId, AbstractAssessment.class);
			result = foo(leistungsnachweis);

			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return result;
	}

	public List<AssessmentGroup> findByLeistungsnachweisId2(Long leistungsnachweisId) throws HibernateException {
		EntityManager entityManager = null;
		List<AssessmentGroup> result = null;
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();

			LeistungsnachweisDAO dao = ((LeistungsnachweisDAO) getDAO(entityManager));
			AbstractAssessment leistungsnachweis = dao.find(leistungsnachweisId, AbstractAssessment.class);
			result = foo2(leistungsnachweis);

			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return result;
	}

	private List<Assessment> foo(AbstractAssessment leistungsnachweis) {
		List<Assessment> result = new ArrayList<Assessment>();
		// result.add(leistungsnachweis);
		if (leistungsnachweis instanceof AssessmentGroup) {
			AssessmentGroup lmt = (AssessmentGroup) leistungsnachweis;
			for (AbstractAssessment child : lmt.getAssessments()) {
				result.addAll(foo(child));
			}
		} else if (leistungsnachweis instanceof Assessment) {
			result.add((Assessment) leistungsnachweis);
		}
		return result;
	}

	private List<AssessmentGroup> foo2(AbstractAssessment leistungsnachweis) {
		List<AssessmentGroup> result = new ArrayList<AssessmentGroup>();
		// result.add(leistungsnachweis);
		if (leistungsnachweis instanceof AssessmentGroup) {
			AssessmentGroup lmt = (AssessmentGroup) leistungsnachweis;
			result.add(lmt);
			for (AbstractAssessment child : lmt.getAssessments()) {
				result.addAll(foo2(child));
			}
			// } else if (leistungsnachweis instanceof Leistungsnachweis) {
			// result.add((Leistungsnachweis) leistungsnachweis);
		}
		return result;
	}
}
