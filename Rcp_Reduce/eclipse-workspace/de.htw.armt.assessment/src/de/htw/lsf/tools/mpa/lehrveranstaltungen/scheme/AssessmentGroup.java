package de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("LMT")
public class AssessmentGroup extends AbstractAssessment {

	private static final long serialVersionUID = 3542312513424067894L;

	public static final String FORMULA_FIELD = "formula";

	private List<AbstractAssessment> assessments = new ArrayList<AbstractAssessment>();;
	private String formula;
	private Module module;

	public AssessmentGroup() {
		super();
	}
	
	public AssessmentGroup(String description, String abbreviation) {
		super(description, abbreviation);
	}

	@OneToMany(mappedBy = "assessmentGroup", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public List<AbstractAssessment> getAssessments() {
		return this.assessments;
	}

	public void setAssessments(List<AbstractAssessment> assessments) {
		this.assessments = assessments;
	}

	@Column(name = "formel")
	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "modul")
	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public void addAssessment(AbstractAssessment assessment) {
		assessments.add(assessment);
		assessment.setAssessmentGroup(this);
	}

	public void removeAssessment(AbstractAssessment assessment) {
		assessments.remove(assessment);
		assessment.setAssessmentGroup(null);
	}

	@Transient
	public boolean isMandatory() {
		boolean mandatory = false;
		for (AbstractAssessment assessment : this.getAssessments()) {
			mandatory = mandatory || assessment.isMandatory();
		}
		return mandatory;
	}

}
