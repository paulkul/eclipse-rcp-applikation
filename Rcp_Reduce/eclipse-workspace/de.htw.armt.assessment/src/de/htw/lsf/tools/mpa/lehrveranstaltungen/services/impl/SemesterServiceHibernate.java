/*
 * SemesterService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.services.impl;

import javax.persistence.EntityManager;

import org.hibernate.HibernateException;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.base.utils.HibernateUtil;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Term;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.SemesterDAO;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.dao.impl.SemesterDAOHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.SemesterService;

/**
 * @author V.Medvid
 */
public class SemesterServiceHibernate extends AbstractServiceHibernate<Term>
		implements SemesterService {

	private static SemesterServiceHibernate instance = null;
	
	public SemesterServiceHibernate() {
		super(Term.class);
	}

	@Override
	public AbstractDAO<Term> getDAO(EntityManager entityManager) {
		SemesterDAOHibernate dao = SemesterDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static SemesterServiceHibernate getInstance() {
		if (instance == null) {
			instance = new SemesterServiceHibernate();
		}
		return instance;
	}

	@Override
	public Term getSemesterBeiBezeichnung(String bezeichnung) throws HibernateException {
		EntityManager entityManager = null;
		Term object = null;
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			object = ((SemesterDAO) getDAO(entityManager)).getSemesterBeiBezeichnung(bezeichnung);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return object;
	}

}
