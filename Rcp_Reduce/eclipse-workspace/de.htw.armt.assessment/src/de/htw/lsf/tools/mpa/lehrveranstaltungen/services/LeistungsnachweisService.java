/*
 * LeistungsnachweisService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.lehrveranstaltungen.services;

import java.util.List;

import de.htw.lsf.tools.mpa.base.services.Service;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;

/**
 * @author V.Medvid
 */
public interface LeistungsnachweisService extends
		Service<AbstractAssessment> {

	public List<AssessmentGroup> getLeistungsnachweiseFuerModul(
			Module m);

	public double getMaxPunkte(AbstractAssessment ln);

	public List<Assessment> findByLeistungsnachweisId(
			Long leistungsnachweisId);

	public List<AssessmentGroup> findByLeistungsnachweisId2(
			Long leistungsnachweisId);

}
