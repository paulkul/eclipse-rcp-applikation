package de.htw.lsf.tools.mpa.lehrveranstaltungen;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	
	private static final String BUNDLE_NAME = "de.htw.lsf.tools.mpa.lehrveranstaltungen.messages"; //$NON-NLS-1$
	
	public static String CallLeistungsnachweisEditor_0;
	public static String CallLeistungsnachweisEditor_1;

	public static String CreateLeistungsnachweisAction_0;
	public static String CreateLeistungsnachweisAction_1;
	public static String CreateLeistungsnachweisAction_2;
	public static String CreateLeistungsnachweisAction_3;
	public static String CreateLeistungsnachweisAction_4;
	public static String CreateLeistungsnachweisAction_5;
	public static String CreateLeistungsnachweisAction_6;
	
	public static String DeleteLeistungsnachweisAction_0;
	public static String DeleteLeistungsnachweisAction_1;
	public static String DeleteLeistungsnachweisAction_2;
	public static String DeleteLeistungsnachweisAction_3;
	public static String DeleteLeistungsnachweisAction_4;
	public static String DeleteLeistungsnachweisAction_5;
	
	public static String LeistungsnachweisAdapterFactory_0;
	public static String LeistungsnachweisAdapterFactory_1;
	public static String LeistungsnachweisAdapterFactory_2;
	
	public static String LeistungsnachweisEditor_0;
	public static String LeistungsnachweisEditor_1;
	public static String LeistungsnachweisEditor_Gruppenleistung;
	public static String LeistungsnachweisEditor_11;
	public static String LeistungsnachweisEditor_Teilnahmepflicht;
	public static String LeistungsnachweisEditor_13;
	public static String LeistungsnachweisEditor_2;
	public static String LeistungsnachweisEditor_Titel;
	public static String LeistungsnachweisEditor_4;
	public static String LeistungsnachweisEditor_5;
	public static String LeistungsnachweisEditor_Formel;
	public static String LeistungsnachweisEditor_Bezeichnung;
	public static String LeistungsnachweisEditor_Kuerzel;
	public static String LeistungsnachweisEditor_Punkte;

	public static String LeistungsnachweisView_0;
	public static String LeistungsnachweisView_Titel;
	public static String LeistungsnachweisView_2;
	public static String LeistungsnachweisView_3;
	
	public static String ModulEditor_Titel;
	
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
