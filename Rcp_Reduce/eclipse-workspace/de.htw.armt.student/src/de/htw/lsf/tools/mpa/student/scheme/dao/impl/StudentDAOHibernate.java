/*
 * StudentDAOImpl.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.student.scheme.dao.impl;

import de.htw.lsf.tools.mpa.base.scheme.dao.impl.*;
import de.htw.lsf.tools.mpa.student.scheme.*;
import de.htw.lsf.tools.mpa.student.scheme.dao.*;

/**
 * @author V.Medvid
 */
public class StudentDAOHibernate extends AbstractDAOHibernate<Student> implements StudentDAO {

	private static StudentDAOHibernate instance = null;
	
	private StudentDAOHibernate() {
	}
	
	public static synchronized StudentDAOHibernate getInstance() {
		if (instance == null) {
			instance = new StudentDAOHibernate();
		}
		return instance;
	}
}
