/*
 * StudentDAO.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.student.scheme.dao;

import de.htw.lsf.tools.mpa.base.scheme.dao.*;
import de.htw.lsf.tools.mpa.student.scheme.*;

/**
 * @author V.Medvid
 */
public interface StudentDAO extends AbstractDAO<Student> {

}
