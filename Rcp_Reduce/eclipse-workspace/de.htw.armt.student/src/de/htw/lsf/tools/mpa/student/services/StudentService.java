/*
 * StudentService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.student.services;

import de.htw.lsf.tools.mpa.base.services.*;
import de.htw.lsf.tools.mpa.student.scheme.*;

/**
 * @author V.Medvid
 */
public interface StudentService extends Service<Student> {
	
}
