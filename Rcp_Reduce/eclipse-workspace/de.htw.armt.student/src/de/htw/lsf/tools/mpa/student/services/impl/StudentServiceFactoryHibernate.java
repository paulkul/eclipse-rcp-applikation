/*
 * StudentServiceFactory.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.student.services.impl;

import de.htw.lsf.tools.mpa.student.services.*;

/**
 * @author V.Medvid
 */
public class StudentServiceFactoryHibernate extends StudentServiceFactory {
	
	@Override
	public StudentService getStudentService() {
		return StudentServiceHibernate.getInstance();
	}
}
