/*
 * StudentServiceFactory.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.student.services;

import de.htw.lsf.tools.mpa.student.services.impl.*;

/**
 * @author V.Medvid
 */
public abstract class StudentServiceFactory {
	
	public static final Class<? extends StudentServiceFactory> HIBERNATE =
			StudentServiceFactoryHibernate.class;

	public static StudentServiceFactory getInstance(Class<? extends StudentServiceFactory> factory) {
        try {
            return (StudentServiceFactory) factory.newInstance();
        } catch (Exception ex) {
            throw new RuntimeException("ServiceFactory kann nicht erzeugt werden: " + factory);
        }
    }
	
	abstract public StudentService getStudentService();
}
