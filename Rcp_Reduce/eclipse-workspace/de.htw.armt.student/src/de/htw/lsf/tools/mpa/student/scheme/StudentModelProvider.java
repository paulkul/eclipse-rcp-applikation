package de.htw.lsf.tools.mpa.student.scheme;

import org.eclipse.core.databinding.observable.list.WritableList;

public enum StudentModelProvider {
	INSTANCE;

	@SuppressWarnings("rawtypes")
	private WritableList students;

	private StudentModelProvider() {
		students = WritableList.withElementType(Student.class);
	}

	@SuppressWarnings("rawtypes")
	public WritableList getStudents() {
		return students;
	}

}
