package de.htw.lsf.tools.mpa.student;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "de.htw.lsf.tools.mpa.student.messages"; //$NON-NLS-1$
	
	public static String StudentEditor_0;
	public static String StudentEditor_1;
	public static String StudentEditor_weiblich;
	public static String StudentEditor_Matrikelnummer;
	public static String StudentEditor_Email;
	public static String StudentEditor_2;
	public static String StudentEditor_3;
	public static String StudentEditor_Nachname;
	public static String StudentEditor_Vorname;
	public static String StudentEditor_Namenszusatz;
	public static String StudentEditor_Titel;
	public static String StudentEditor_Geschlecht;
	public static String StudentEditor_maennlich;
	
	public static String StudentenView_0;
	public static String StudentenView_1;
	public static String StudentenView_10;
	public static String StudentenView_2;
	public static String StudentenView_3;
	public static String StudentenView_4;
	public static String StudentenView_5;
	public static String StudentenView_6;
	public static String StudentenView_7;
	public static String StudentenView_8;
	public static String StudentenView_9;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
