/*
 * BewertungService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import de.htw.lsf.tools.mpa.base.services.Service;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Bewertung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;

/**
 * @author V.Medvid
 */
public interface BewertungService extends Service<Bewertung> {

	public double getBewertung(AbstrakterTeilnehmer tn,
			AssessmentGroup ln);

	public Bewertung getBewertung(AbstrakterTeilnehmer tn, Assessment ln);

	public Bewertung getTeilnehmerBewertung(Teilnehmer tn, Assessment ln);

	double getBewertung(AbstrakterTeilnehmer tn,
			AssessmentGroup ln, EntityManager entityManager);

	// public Map<AbstrakterTeilnehmer, Map<Leistungsnachweis, Bewertung>>
	// determineBewertungen(
	// Long lehrveranstaltungId, List<Leistungsnachweis> leistungsnachweise);

	public Map<Teilnehmer, Map<AbstractAssessment, Double>> determineBewertungen(
			List<Teilnehmer> participants,
			AssessmentGroup assessment);

	public Map<AbstrakterTeilnehmer, Bewertung> determineBewertungen(
			List<? extends AbstrakterTeilnehmer> teilnehmer,
			Assessment leistungsnachweis);

	/**
	 * 
	 * @param participants
	 * @param assessment
	 * @return
	 */
	List<Bewertung> determineAssessments(
			List<? extends AbstrakterTeilnehmer> participants,
			Assessment assessment);
	
	void updateRatings(List<Bewertung> ratings);
}
