package de.htw.lsf.tools.mpa.bewertungen.gui.wizards;

import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.services.POIService;
import de.htw.lsf.tools.mpa.bewertungen.structures.ImportData;

public class FirstImportWizardPage extends WizardPage {
	
	private FileFieldEditor fileFieldEditor;
	private ImportData importData;
	private Logger logger = Logger.getLogger(FirstImportWizardPage.class);

	protected FirstImportWizardPage() {
		super(Messages.FirstImportWizardPage_0);
        setTitle(Messages.FirstImportWizardPage_1);
        setDescription(Messages.FirstImportWizardPage_2
        		+ Messages.FirstImportWizardPage_3);
        
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.marginWidth = 200;
		container.setLayout(layout);
        fileFieldEditor = new FileFieldEditor(Messages.FirstImportWizardPage_4, Messages.FirstImportWizardPage_5, container);
        fileFieldEditor.setFileExtensions(new String[]{"*.xls", "*.*"}); //$NON-NLS-1$ //$NON-NLS-2$
        fileFieldEditor.setPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				String oldV = (String) event.getOldValue();
				String newV = (String) event.getNewValue();
				oldV = (oldV == null) ? "" : oldV; //$NON-NLS-1$
				newV = (newV == null) ? "" : newV; //$NON-NLS-1$
				if (!oldV.equals(newV)) {
					String fname = fileFieldEditor.getStringValue();
					boolean ok = isFileCorrect(fname);
					setErrorMessage(ok ? null : Messages.FirstImportWizardPage_10
							+ Messages.FirstImportWizardPage_11);
					setPageComplete(ok);
				}
			}
		});
        setControl(container);
		setPageComplete(false);
	}

	private boolean isFileCorrect(String filename) {
		boolean ok = false;
		File f = new File(filename);
		if (f.exists() && f.isFile()) {
			try {
				this.importData = POIService.loadData(filename);
				SecondImportWizardPage secondPage = (SecondImportWizardPage) this.getNextPage();
				secondPage.refreshData();
				ok = true;
			} catch (Exception e) {
				logger.log(Level.WARN, "", e); //$NON-NLS-1$
			}
		}
		return ok;
	}
	
	public String getFilename() {
		return fileFieldEditor.getStringValue();
	}

	public ImportData getImportData() {
		return importData;
	}
}
