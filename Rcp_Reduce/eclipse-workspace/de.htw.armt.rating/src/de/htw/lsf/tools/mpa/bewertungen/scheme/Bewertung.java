package de.htw.lsf.tools.mpa.bewertungen.scheme;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.htw.lsf.tools.mpa.base.scheme.ModelObject;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AbstractAssessment;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;

@Entity
@Table(name = "BEWERTUNG")
@IdClass(BewertungId.class)
public class Bewertung extends ModelObject {
	// TODO rename in Rating

	/**
	 * 
	 */
	private static final long serialVersionUID = -7664220386647286027L;
	private Assessment leistungsnachweis;
	private AbstrakterTeilnehmer teilnehmer;
	private Double punkte;
	private Long number;

	public Bewertung() {
		super();
	}

	public Bewertung(Long moId, Assessment leistungsnachweis,
			AbstrakterTeilnehmer teilnehmer, Double punkte) {
		super();
		this.number = moId;
		this.leistungsnachweis = leistungsnachweis;
		this.teilnehmer = teilnehmer;
		this.punkte = punkte;
	}

	public Bewertung(Assessment leistungsnachweis,
			AbstrakterTeilnehmer teilnehmer, Double punkte) {
		super();
		this.leistungsnachweis = leistungsnachweis;
		this.teilnehmer = teilnehmer;
		this.punkte = punkte;
	}

	@ManyToOne(cascade = { CascadeType.MERGE })
	@Id
	@JoinColumn(name = "leistungsnachweis")
	public AbstractAssessment getLeistungsnachweis() {
		return leistungsnachweis;
	}

	public void setLeistungsnachweis(Assessment leistungsnachweis) {
		this.leistungsnachweis = leistungsnachweis;
	}

	@ManyToOne(cascade = { CascadeType.MERGE })
	@Id
	@JoinColumns( {
		@JoinColumn(name = "nrTeilnehmer"),
		@JoinColumn(name = "lvTeilnehmer")})
//	@JoinColumn(name = "teilnehmerId")
	public AbstrakterTeilnehmer getTeilnehmer() {
		return teilnehmer;
	}

	public void setTeilnehmer(AbstrakterTeilnehmer teilnehmer) {
		this.teilnehmer = teilnehmer;
	}

	@Column(name = "punkte")
	public Double getPunkte() {
		return punkte;
	}

	public void setPunkte(Double punkte) {
		this.punkte = punkte;
	}

	@Override
	@Column(name = "moId")
//	@GeneratedValue(strategy = GenerationType.TABLE)
	public Long getNumber() {
		return number;
	}

	@Override
	public void setNumber(Long number) {
		this.number = number;
	}

	@Transient
	public boolean isActive() {
		if (this.getTeilnehmer() instanceof Uebungsgruppe) {
			return ((Uebungsgruppe) this.getTeilnehmer()).isActive();
		} else {
			return true;
		}
	}

	@Transient
	public BewertungId getBewertungId() {
		return new BewertungId(this.getLeistungsnachweis().getNumber(),
				this.getTeilnehmer().getTeilnehmerId());
	}

	
	@Override
	public int hashCode() {
		return this.getBewertungId().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Bewertung other = (Bewertung) obj;
		if (getBewertungId() == null) {
			if (other.getBewertungId() != null)
				return false;
		} else if (!getBewertungId().equals(other.getBewertungId()))
			return false;
		return true;
	}

}
