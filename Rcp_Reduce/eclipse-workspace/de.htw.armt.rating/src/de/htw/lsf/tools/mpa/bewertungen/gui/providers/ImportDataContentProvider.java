package de.htw.lsf.tools.mpa.bewertungen.gui.providers;

import java.util.*;

import org.eclipse.jface.viewers.*;

import de.htw.lsf.tools.mpa.bewertungen.structures.*;

public class ImportDataContentProvider implements IStructuredContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		ImportData importData = (ImportData) inputElement;
		List<ImportDataEntry> list = importData.getEntries();
		if (list != null) {
			return list.toArray(new ImportDataEntry[list.size()]);
		}
		return new ImportDataEntry[0];
	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
