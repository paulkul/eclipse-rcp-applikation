package de.htw.lsf.tools.mpa.bewertungen.scheme;

import javax.persistence.*;

import de.htw.lsf.tools.mpa.base.scheme.*;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Module;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Term;

@Entity
@Table(name = "LEHRVERANSTALTUNG")
public class Lehrveranstaltung extends ModelObject {
	// TODO rename in lecture
	// TODO move Id to modul, semester ??

	/**
	 * 
	 */
	private static final long serialVersionUID = -3610726206632067134L;
	@Id
	@Column(name="lehrveranstaltungsid")
	private long lehrveranstaltungsID;
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	@JoinColumn(name="modul")
	private Module modul;
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
	@JoinColumn(name="semester")
	private Term semester;
	@ManyToOne( cascade = {CascadeType.MERGE} )
	@JoinColumn(name="leistungsnachweisgruppe")
	private AssessmentGroup leistungsnachweisGruppe;
	@Column(name="manuellhinzugefuegt")
	private boolean manuellHinzugefuegt = false;


	public Lehrveranstaltung() {
		super();
	}

	public Lehrveranstaltung(long lehrveranstaltungsID, Module modul, Term semester, 
			AssessmentGroup leistungsnachweisGruppe) {
		this(lehrveranstaltungsID, modul, semester, leistungsnachweisGruppe, false);
	}
	
	public Lehrveranstaltung(long lehrveranstaltungsID, Module modul, Term semester, 
			AssessmentGroup leistungsnachweisGruppe, 
			boolean manuellHinzugefuegt) {
		super();
		this.lehrveranstaltungsID = lehrveranstaltungsID;
		this.modul = modul;
		this.semester = semester;
		this.leistungsnachweisGruppe = leistungsnachweisGruppe;
		this.manuellHinzugefuegt = manuellHinzugefuegt;
	}
	
	public AssessmentGroup getLeistungsnachweisGruppe() {
		return leistungsnachweisGruppe;
	}
	public void setLeistungsnachweisGruppe(AssessmentGroup 
			leistungsnachweisGruppe) {
		this.leistungsnachweisGruppe = leistungsnachweisGruppe;
	}
	public long getLehrveranstaltungsID() {
		return lehrveranstaltungsID;
	}
	public void setLehrveranstaltungsID(long lehrveranstaltungsID) {
		this.lehrveranstaltungsID = lehrveranstaltungsID;
	}
	public Term getSemester() {
		return semester;
	}
	public void setSemester(Term semester) {
		this.semester = semester;
	}

	public Module getModul() {
		return modul;
	}

	public void setModul(Module modul) {
		this.modul = modul;
	}

	public boolean isManuellHinzugefuegt() {
		return manuellHinzugefuegt;
	}

	public void setManuellHinzugefuegt(boolean manuellHinzugefuegt) {
		this.manuellHinzugefuegt = manuellHinzugefuegt;
	}
	
	public String getBezeichnung() {
		String txt = "";
		Module m = this.getModul();
		if (m != null) {
			txt = m.getName();
		} else {
			txt = "-";
		}
		txt += " (";
		Term s = this.getSemester();
		if (s != null) {
			txt += s.getName();
		} else {
			txt += "-";
		}
		txt += ")";
		txt += " [";
		AssessmentGroup lng = this.getLeistungsnachweisGruppe();
		if (lng != null) {
			txt += lng.getName();
		} else {
			txt += "-";
		}
		txt += "]";
		return txt;
	}

	@Override
	public Long getNumber() {
		return this.getLehrveranstaltungsID();
	}

	@Override
	public void setNumber(Long number) {
		this.setLehrveranstaltungsID(number);
	}
	
	@Override
    public String toString() {
    	return this.getLeistungsnachweisGruppe().getAbbreviation();
	}

}
