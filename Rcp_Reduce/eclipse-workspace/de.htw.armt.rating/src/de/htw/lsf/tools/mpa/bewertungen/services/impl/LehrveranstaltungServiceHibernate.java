/*
 * LehrveranstaltungService.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.HibernateException;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.base.services.impl.AbstractServiceHibernate;
import de.htw.lsf.tools.mpa.base.utils.HibernateUtil;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.LehrveranstaltungDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl.LehrveranstaltungDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.services.LehrveranstaltungService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;

/**
 * @author V.Medvid
 */
public class LehrveranstaltungServiceHibernate extends AbstractServiceHibernate<Lehrveranstaltung>
		implements LehrveranstaltungService {

	private static LehrveranstaltungServiceHibernate instance = null;
	
	public LehrveranstaltungServiceHibernate() {
		super(Lehrveranstaltung.class);
	}

	@Override
	public AbstractDAO<Lehrveranstaltung> getDAO(EntityManager entityManager) {
		LehrveranstaltungDAOHibernate dao = LehrveranstaltungDAOHibernate.getInstance();
		dao.setEntityManager(entityManager);
		return dao;
	}

	public static LehrveranstaltungServiceHibernate getInstance() {
		if (instance == null) {
			instance = new LehrveranstaltungServiceHibernate();
		}
		return instance;
	}

	@Override
	public List<Lehrveranstaltung> getLehrveranstaltungenFuerLeistungsnachweis(
			AssessmentGroup ln) throws HibernateException {
		EntityManager entityManager = null;
		List<Lehrveranstaltung> objects = new ArrayList<Lehrveranstaltung>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((LehrveranstaltungDAO) getDAO(entityManager)).getLehrveranstaltungenFuerLeistungsnachweis(ln);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}

	@Override
	public List<Lehrveranstaltung> getLehrveranstaltungenFuerModul(String modulnummer) {
		EntityManager entityManager = null;
		List<Lehrveranstaltung> objects = new ArrayList<Lehrveranstaltung>();
		try {
			entityManager = HibernateUtil.getCurrentSession();
			entityManager.getTransaction().begin();
			objects = ((LehrveranstaltungDAO) getDAO(entityManager)).getLehrveranstaltungenFuerModul(
					modulnummer);
			entityManager.getTransaction().commit();
		} catch (HibernateException e) {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.getTransaction().rollback();
			}
			throw e;
		} finally {
			HibernateUtil.releaseCurrentSession(entityManager);
		}
		return objects;
	}
}
