package de.htw.lsf.tools.mpa.bewertungen.structures;

import java.util.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.*;

public class ExportData {
	
	private String modulnummer;
	private List<ExportDataEntry> entries;
	private HSSFWorkbook workbook = null;
	
	private Logger logger = Logger.getLogger(ExportData.class);
	
	public ExportData(String modulnummer, List<ExportDataEntry> entries) {
		this.modulnummer = modulnummer;
		this.entries = entries;
	}

	public ExportData(String modulnummer, String[] headers, List<String[]> values) {
		this.modulnummer = modulnummer;
		this.entries = new ArrayList<ExportDataEntry>();
		int row = 0;
		for (String[] strings : values) {
			long pordnr = -1;
			long labnr = -1;
			long stg = -1;
			String sortname = "";
			long matrikelnummer = -1;
			long pversuch = -1;
			long bewertung = -1;
			for (int i = 0; i < headers.length; i++) {
				if ((headers[i] != null) && (headers[i].length() > 0)) {
					if (headers[i].equalsIgnoreCase("pordnr")) {
						try {
							pordnr = Long.parseLong(strings[i]);
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("labnr")) {
						try {
							labnr = Long.parseLong(strings[i]);
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("stg")) {
						try {
							stg = Long.parseLong(strings[i]);
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("sortname")) {
						sortname = strings[i];
					} else if (headers[i].equalsIgnoreCase("mtknr")) {
						try {
							matrikelnummer = Long.parseLong(strings[i]);
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("pversuch")) {
						try {
							pversuch = Long.parseLong(strings[i]);
						} catch (NumberFormatException e) {
							logger.log(Level.WARN, "", e);
						}
					} else if (headers[i].equalsIgnoreCase("bewertung")) {
						if (strings[i] != null && !strings[i].isEmpty()) {
							try {
								bewertung = Long.parseLong(strings[i]);
							} catch (NumberFormatException e) {
								logger.log(Level.WARN, "", e);
							}
						}
					}
				}
			}
			this.entries.add(new ExportDataEntry(row, pordnr, labnr, stg, sortname, 
					matrikelnummer, pversuch, bewertung));
			row++;
		}
	}

	public String getModulnummer() {
		return modulnummer;
	}

	public List<ExportDataEntry> getEntries() {
		return entries;
	}

	public HSSFWorkbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(HSSFWorkbook workbook) {
		this.workbook = workbook;
	}

	public ExportDataEntry getEntryByMatrikelnummer(long mn) {
		ExportDataEntry entry = null;
		for (ExportDataEntry e : entries) {
			if (mn == e.getMatrikelnummer()) {
				entry = e;
				break;
			}
		}
		return entry;
	}
}
