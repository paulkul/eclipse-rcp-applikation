/*
 * LehrveranstaltungDAOImpl.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import de.htw.lsf.tools.mpa.base.scheme.dao.impl.AbstractDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.LehrveranstaltungDAO;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.AssessmentGroup;

/**
 * @author V.Medvid
 */
public class LehrveranstaltungDAOHibernate extends AbstractDAOHibernate<Lehrveranstaltung> implements
		LehrveranstaltungDAO {

	private static LehrveranstaltungDAOHibernate instance;
	
	private LehrveranstaltungDAOHibernate() {
	}

	public static synchronized LehrveranstaltungDAOHibernate getInstance() {
		if (instance == null) {
			instance = new LehrveranstaltungDAOHibernate();
		}
		return instance;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Lehrveranstaltung> getLehrveranstaltungenFuerLeistungsnachweis(
			AssessmentGroup ln) {
		List<Lehrveranstaltung> objects = new ArrayList<Lehrveranstaltung>();
		Query query = getEntityManager().createQuery(
				"from lehrveranstaltung where leistungsnachweisgruppe=:lnId ").setParameter(
				"lnId", ln.getNumber());
		objects = query.getResultList();
		return objects;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Lehrveranstaltung> getLehrveranstaltungenFuerModul(String modulnummer) {
		List<Lehrveranstaltung> objects = new ArrayList<Lehrveranstaltung>();
		Query query = getEntityManager().createQuery(
				"from lehrveranstaltung where modul like :modulNr ").setParameter(
				"modulNr", modulnummer.substring(0, modulnummer.length() - 1) + "%");
		objects = query.getResultList();
		return objects;
	}

}
