package de.htw.lsf.tools.mpa.bewertungen.scheme.dao;

import java.util.List;
import java.util.Set;

import de.htw.lsf.tools.mpa.base.scheme.dao.AbstractDAO;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Bewertung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.scheme.Assessment;

public interface BewertungDAO extends AbstractDAO<Bewertung> {

	public Bewertung getBewertung(AbstrakterTeilnehmer tn, Assessment ln);

	public Bewertung getTeilnehmerBewertung(Teilnehmer tn, Assessment ln);

	public Bewertung getBewertung(Set<Uebungsgruppe> workgroups,
			Assessment ln);

	public List<Bewertung> findByLehrveranstaltungIdAndLeistungsnachweisen(
			Long lehrveranstaltungId, List<Assessment> leistungsnachweise);

}
