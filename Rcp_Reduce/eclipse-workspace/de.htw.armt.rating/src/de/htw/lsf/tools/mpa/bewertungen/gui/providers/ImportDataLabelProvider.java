package de.htw.lsf.tools.mpa.bewertungen.gui.providers;

import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.*;

import de.htw.lsf.tools.mpa.bewertungen.structures.*;

public class ImportDataLabelProvider extends LabelProvider implements ITableLabelProvider {

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		ImportDataEntry entry = (ImportDataEntry) element;
		switch (columnIndex) {
		case 0:
			return entry.getVeranstaltung();
		case 1:
			return entry.getVeranstaltungsnummer();
		case 2:
			return String.valueOf(entry.getMatrikelnummer());
		case 3:
			return entry.getNachname();
		case 4:
			return entry.getVorname();
		default:
			throw new RuntimeException("Should not happen");
		}
	}

}
