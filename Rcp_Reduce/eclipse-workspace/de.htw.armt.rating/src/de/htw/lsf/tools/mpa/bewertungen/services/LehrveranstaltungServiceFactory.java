/*
 * LehrveranstaltungServiceFactory.java
 *
 * Erzeugt von V.Medvid
 */
package de.htw.lsf.tools.mpa.bewertungen.services;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import de.htw.lsf.tools.mpa.bewertungen.services.impl.LehrveranstaltungServiceFactoryHibernate;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.LeistungsnachweisService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.ModulService;
import de.htw.lsf.tools.mpa.lehrveranstaltungen.services.SemesterService;

/**
 * @author V.Medvid
 */
public abstract class LehrveranstaltungServiceFactory {

	public static final Class<? extends LehrveranstaltungServiceFactory> HIBERNATE = LehrveranstaltungServiceFactoryHibernate.class;

	public static LehrveranstaltungServiceFactory getInstance(
			Class<? extends LehrveranstaltungServiceFactory> factory) {
		Logger logger = Logger.getLogger(LehrveranstaltungServiceFactory.class);
		try {
			return (LehrveranstaltungServiceFactory) factory.newInstance();
		} catch (Exception ex) {
			logger.log(Level.ERROR, "ServiceFactory kann nicht erzeugt werden: ", ex);
			return null;
		}
	}

	abstract public LehrveranstaltungService getLehrveranstaltungService();

	abstract public LeistungsnachweisService getLeistungsnachweisService();

	abstract public ModulService getModulService();

	abstract public SemesterService getSemesterService();
}
