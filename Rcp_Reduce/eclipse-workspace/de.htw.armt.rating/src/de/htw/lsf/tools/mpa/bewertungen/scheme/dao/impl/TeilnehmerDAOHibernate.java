package de.htw.lsf.tools.mpa.bewertungen.scheme.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import de.htw.lsf.tools.mpa.base.scheme.dao.impl.AbstractDAOHibernate;
import de.htw.lsf.tools.mpa.bewertungen.scheme.AbstrakterTeilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Lehrveranstaltung;
import de.htw.lsf.tools.mpa.bewertungen.scheme.ParticipantIdDummy;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Teilnehmer;
import de.htw.lsf.tools.mpa.bewertungen.scheme.Uebungsgruppe;
import de.htw.lsf.tools.mpa.bewertungen.scheme.dao.TeilnehmerDAO;
import de.htw.lsf.tools.mpa.student.scheme.Student;

public class TeilnehmerDAOHibernate extends AbstractDAOHibernate<AbstrakterTeilnehmer> implements TeilnehmerDAO {

	private static TeilnehmerDAOHibernate instance = null;

	private TeilnehmerDAOHibernate() {
	}

	public static synchronized TeilnehmerDAOHibernate getInstance() {
		if (instance == null) {
			instance = new TeilnehmerDAOHibernate();
		}
		return instance;
	}

	public AbstrakterTeilnehmer insert(AbstrakterTeilnehmer teilnehmer) {
		if (teilnehmer.getNumber() == null) {
			ParticipantIdDummy id = new ParticipantIdDummy();
			getEntityManager().persist(id);
			teilnehmer.setNumber(id.getId());
			getEntityManager().remove(id);
		}
		return super.insert(teilnehmer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Teilnehmer> getTeilnehmerByMatrNr(Long matrNr) {
		List<Teilnehmer> objects = new ArrayList<Teilnehmer>();
		Query query = getEntityManager().createQuery("from teilnehmer t where t.student.registrationNumber = :matrNr")
				.setParameter("matrNr", matrNr);
		objects = query.getResultList();
		return objects;
	}

	@Override
	public boolean exists(Teilnehmer t) {
		if (t.getNumber() != null) {
			return this.exists(t.getTeilnehmerId(), AbstrakterTeilnehmer.class);
		}
		Student student = t.getStudent();
		Lehrveranstaltung lv = t.getVeranstaltung();
		Query query = getEntityManager()
				.createQuery("from teilnehmer where student = :student and lehrveranstaltung = :lv")
				.setParameter("student", student).setParameter("lv", lv);
		return (query.getResultList().size() > 0);
	}

	@Override
	public boolean exists(Uebungsgruppe ug) {
		if (ug.getNumber() > 0) {
			return this.exists(ug.getTeilnehmerId(), AbstrakterTeilnehmer.class);
		}
		String bez = ug.getBezeichnung();
		Lehrveranstaltung lv = ug.getVeranstaltung();
		Query query = getEntityManager()
				.createQuery("from teilnehmer where bezeichnung=:bez and lehrveranstaltung=:verID")
				.setParameter("bez", bez).setParameter("verID", lv.getLehrveranstaltungsID());
		return (query.getResultList().size() > 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Teilnehmer> getTeilnehmerFuerLehrveranstaltung(Lehrveranstaltung lehrveranstaltung) {
		List<Teilnehmer> objects = new ArrayList<Teilnehmer>();
		Query query = getEntityManager()
				.createQuery("from Teilnehmer t " + "where lehrveranstaltung = :lehrveranstaltung "
				// + "and DISC = 'T' "
						+ "order by t.student.lastname asc")
				.setParameter("lehrveranstaltung", lehrveranstaltung);
		objects = query.getResultList();
		return objects;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Teilnehmer getTeilnehmer(Lehrveranstaltung lehrveranstaltung, long matrikelNummer) {
		Teilnehmer object = null;
		List<Teilnehmer> objects = new ArrayList<Teilnehmer>();
		Query query = getEntityManager().createQuery(
				"from teilnehmer where student.registrationNumber = :studentMN and veranstaltung.lehrveranstaltungsID = :verID")
				.setParameter("studentMN", matrikelNummer)
				.setParameter("verID", lehrveranstaltung.getLehrveranstaltungsID());
		objects = query.getResultList();
		if (objects != null && objects.size() > 0) {
			object = objects.get(0);
		}
		return object;
	}

	@Override
	public List<Teilnehmer> getTeilnehmerFuerUebungsgruppe(Uebungsgruppe gruppe) {
		List<Teilnehmer> result = new ArrayList<Teilnehmer>(gruppe.getTeilnehmer());
		return result;
		// List<Teilnehmer> objects = new ArrayList<Teilnehmer>();
		// Query query = getSession().createQuery("from Teilnehmer t");
		// objects = query.list();
		// List<Teilnehmer> results = new ArrayList<Teilnehmer>();
		// for ( Teilnehmer t : objects){
		// if ( Arrays.asList(t.getUebungsgruppe()).contains(gruppe)){
		// results.add(t);
		// }
		//
		// }
		// return results;
	}
}
