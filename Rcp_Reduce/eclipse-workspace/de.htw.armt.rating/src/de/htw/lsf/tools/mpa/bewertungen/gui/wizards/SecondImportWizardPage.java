package de.htw.lsf.tools.mpa.bewertungen.gui.wizards;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.htw.lsf.tools.mpa.bewertungen.Messages;
import de.htw.lsf.tools.mpa.bewertungen.gui.providers.ImportDataContentProvider;
import de.htw.lsf.tools.mpa.bewertungen.gui.providers.ImportDataLabelProvider;
import de.htw.lsf.tools.mpa.bewertungen.structures.ImportData;

public class SecondImportWizardPage extends WizardPage {

	private FirstImportWizardPage page1;
	private TableViewer viewer;
	private Text semesterText;

	protected SecondImportWizardPage() {
		super(Messages.SecondImportWizardPage_0);
		setTitle(Messages.SecondImportWizardPage_1);
		setDescription(Messages.SecondImportWizardPage_2);
	}

	void setFirstPage (FirstImportWizardPage page1){

		this.page1 = page1;
	}

	@Override
	public void createControl(Composite parent) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
//		Composite container = toolkit.createComposite(parent, SWT.NULL);
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		container.setLayout(layout);

//		Composite semesterContainer = toolkit.createComposite(container, SWT.NULL);
		Composite semesterContainer = new Composite(container, SWT.NULL);
		layout = new GridLayout();
		layout.numColumns = 2;
		semesterContainer.setLayout(layout);
//		toolkit.createLabel(semesterContainer, "Semester: ",  SWT.NONE);
		Label lbl = new Label(semesterContainer, SWT.NONE);
		lbl.setText(Messages.SecondImportWizardPage_3);
		semesterText = toolkit.createText(semesterContainer, "", SWT.SINGLE | SWT.BORDER); //$NON-NLS-1$
//		semesterText = new Text(semesterContainer, SWT.SINGLE | SWT.BORDER);
		semesterText.setEditable(false);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.widthHint = 300;
		semesterText.setLayoutData(gd);

		viewer = new TableViewer(container, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		createColumns(viewer);
		viewer.setContentProvider(new ImportDataContentProvider());
		viewer.setLabelProvider(new ImportDataLabelProvider());
		viewer.setInput(page1.getImportData());

		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;
		viewer.getControl().setLayoutData(gridData);

		setControl(container);
		setPageComplete(true);
	}

	public void refreshData() {
		setErrorMessage(null);
		ImportData data = page1.getImportData();
		semesterText.setText(data.getSemester());
		viewer.setInput(data);
		viewer.refresh();
	}

	private void createColumns(final TableViewer viewer) {
		String[] titles = new String[] { Messages.SecondImportWizardPage_5, Messages.SecondImportWizardPage_6,
				Messages.SecondImportWizardPage_7, Messages.SecondImportWizardPage_8,
				Messages.SecondImportWizardPage_9 };
		int[] bounds = { 300, 80, 70, 80, 80 };
		for (int i = 0; i < titles.length; i++) {
			TableViewerColumn viewerColumn = new TableViewerColumn(viewer, SWT.NONE);
			final TableColumn column = viewerColumn.getColumn();
			column.setText(titles[i]);
			column.setWidth(bounds[i]);
			column.setResizable(true);
			column.setMoveable(true);
		}
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}
}
